﻿; терморегулятор на pic12CE519
; (c) 2005, ib
; 
; История версий
;
; 07.10.2005, ver.1.02
; исправлено: 
; - теперь в качестве температурной компенсации убавляется не 1, а 4 градуса
;
; 03.10.2005, ver.1.01 
; исправлено: 
; - убран баг: по умолчанию теперь +0x02 градуса вместо +0хFF градусов
; - при нажатой кнопке при включении устройства программа продолжает свою работу
;   только после отпускания кнопки
; - в режиме программирования при нажатии кнопки происходит пауза 0.5 сек
;
; 30.09.2005, ver.1.00 rc1
; исправлено:
; - желтый цвет и программирование знака и режима убрано (всегда положительная 
;   температура на нагревание)
; - переход в режим программирования происходит по отпусканию кнопки
;
; xx.05.2005, ver.0.xx
; доработка основной программы

;          Железо:
;                          внутренний тактовый генератор 4 МГц
;                          термодатчик-термостат Dallas1821
;                          два двуполярных светодиода со "средней точкой" (красный-зеленый)
;
;          Особенности:
;                          1024х12 памяти программ
;                          TRIS-регистр - только чтение (не адресуется)
;                          Память разделена на две страницы по 512 байт
;                          Область подпрограмм - первые 256 байт каждой страницы
;                          16 байт EEPROM
;
;
;          EEPROM:
;                               0x00 - адрес в EEPROM для маркера (=0x00, если записано)
;                               0x01,0 - адрес в EEPROM для байта режима работы: =1 - охлаждение, 0 - нагревание
;                               0x01,1 - адрес в EEPROM для байта знака температуры: =1 - плюсовая, 0 - минусовая
;                               0x02 - адрес в EEPROM для байта "Десятки" порога
;                               0x03 - адрес в EEPROM для байта "Единицы" порога


        #define CORRECTION_enable;      закомментарить для отмены коррекции на 1 градус
        #define EE_enable;              закомментарить для отладки на эмуляторе
;                                       (эмулятор не поддерживает pic12CE519)

        #include "p12CE519.inc";        подключение библиотеки процессора
        __CONFIG _CP_ON &_MCLRE_OFF &_WDT_ON &_IntRC_OSC
; защита кода выключена, сброс по сигналу выключен, охранный таймер включен, используется внутренний генератор

        #define BANK0   bcf     STATUS,PA0;     дефайн переключения на нулевую страницу
        #define BANK1   bsf     STATUS,PA0;     дефайн переключения на первую страницу

; ****==== определения портов =====****
        #define VD2             GPIO,0;  СИД "Десятки"
        #define TEMPER          GPIO,1;  DS1821
;       #define VDCOM           GPIO,2;  Общий вывод для VD1 и VD2
        #define BUTTON  GPIO,3;  Батон
        #define DEVICE          GPIO,4;  Нагреватель
        #define VD1             GPIO,5;  СИД "Единицы"
        #define SDA             GPIO,6;  I2C - линия данных             \   для         / физически
        #define SCL             GPIO,7;  I2C - тактируемый сигнал       / EEPROM        \ контакты отсутсвуют

; ****==== определение переменных ====****
        cblock  0x07
        i;                                временная переменная
        j;                                временная переменная
        CountLow;                          счетчик для малых значений
        CountHigh;                        счетчик для больших значений
        tmpTRIS;                        переменная для хранения содержимого регистра
        ;                                  TRIS. Необходимо сначала записать в нее,
        ;                                  а потом командой TRIS 6 - в сам регистр TRIS
        temperature;                    для измерения температуры
        tmp;                            временная переменная
        CurrentDec;                      текущее значение десятков
        CurrentUnits;              текущее значение единиц
        TMR0last;                          последнее значение TMR0
        ButtonCounterLow;                  счетчик кнопки для малых значений
        ButtonCounterHigh;                счетчик кнопки для больших значений
        FLAGS;                    для флагов (см. ниже определения флагов)
        FLAGS2;                  для флагов (см. ниже определения флагов)
        FLAGS3;                  для флагов (см. ниже определения флагов)
        ThresholdDec;              для порогового значения десятков
        ThresholdUnits;          для порогового значения единиц
        DeviceCounter;            счетчик для включения девайса
        endc

; ****==== определение переменных для EEPROM ====****
        cblock  0x1A
        PC_OFFSET                  ; регистр сдвига PC (младшие 4 бита),
                                ; значение, зависимое от операции с EEPROM
                                ; также 7-й бит используется для EE_OK flag
        EEADDR            ; Адрес в EEPROM
        EEDATA            ; Данные EEPROM
        EEBYTE            ; Байт для отправки/получения из EEPROM
                                ; (управляющий, адрес или данные)
        COUNTER          ; Бит счетчика для последовательного обмена
        endc
; ****==== конец определения переменных ====****

; ****==== дефайны ====****
        #define SignFlag                FLAGS,0; знак запрогр. темпер. (+ =1,- =0)
        #define ModeFlag                FLAGS,1; режим запр. темпер. (нагр =1,охл =0)
        #define BlinkUnitsFlag                  FLAGS,2; мигание единиц
        #define BlinkDecFlag            FLAGS,3; мигание десятков
        #define CurrentColor            FLAGS,4; тек. цвет одного диода
        #define CurrentColor2                   FLAGS,5; тек. цвет другого диода
        #define ModeNotSign             FLAGS,6; временн. флаг
        #define ModeSignEnd             FLAGS,7; временн. флаг

        #define CurSignFlag             FLAGS2,0; знак тек. темпер.
        #define tmpFlag                 FLAGS2,1; врем. флаг
        #define DeviceOnFlag            FLAGS2,2; флаг включен. девайса
        #define Check3EndFlag           FLAGS2,3; конец проверки на +/- градуса
        #define NullNearFlag            FLAGS2,4; температура отличается от 0 и -1 меньше, чем на 3
        #define DiffSignFlag            FLAGS2,5; знаки порог. и тек. температур отличаются
        #define tmpFlag2                FLAGS2,6; врем. флаг
        #define FaseFlag                FLAGS2,7; значение фазы в тек. момент

        #define DeviceOutFlag           FLAGS3,0; флаг отсутствия девайса
        #define tmpcolor                FLAGS3,1; врем. флаг
        #define DecNotUnits             FLAGS3,2; врем. флаг, используется в Program
        #define DeviceOnPermitFlag      FLAGS3,3; флаг разрешения включения устройства
        #define BeginNotEndFlag FLAGS3,4; временный флаг, используется при индикации введенных значений
        #define ShortButtonFlag         FLAGS3,5; флаг кратковременного нажатия
        #define SecondFlag              FLAGS3,6; флаг второго прохода, используется при мигании желтых

; для EEPROM
        #define OK                0x01;   флаг удачной записи
        #define NO                0x00;   флаг неудачной записи
        #define EE_OK      0x07;   бит 7 в PC_OFFSET используется как OK flag для EE

; ****==== конец дефайнов

        org 0x00;
        goto Init;        начало программы после области подпрограмм (~0x194)



TmprMsr
; ****==== запись в статус датчика ****====
        call DSReset; сигнал сброса
        movlw 0x0C; запись в i сигнала записи в статус
        call SendDS;

        movlw 0x02; запись в i байта статуса
        call SendDS;

; ****==== инициализация начала конвертации ====****
        call DL250ms;

        call DSReset;

        movlw 0xEE;      запись в i сигнала начала конвертации температуры
        call SendDS;


; ****==== чтение температуры ====****
        call DL250ms;

        call DSReset; сигнал сброса

        movlw 0xAA;      запись в i сигнала чтения температуры
        call SendDS;

        call GetDS;      чтение температуры
        goto EndTmpr;   переход на метку "конец измерения"

;****************************************************************
;****************************************************************
;****************************************************************

;***************************************
; подпрограммы пишем в первые 256 байт
;***************************************

;***************************************
; инициализация датчика
;***************************************
DSReset
        call    DSTmprOut; устанавливаем порт TRIS датчика на выход =0
        call    DL500us;
        call    DSTmprIn;

        call    DL60us;
        btfsc   TEMPER;
        retlw   0xFF;
        call    DL500us;
        retlw   0;

;***************************************
; получение байта от датчика (см. доку по ds1821)
;***************************************
GetDS
        movlw 8;
        movwf j;
GetDSLp
        call    DSTmprOut;
        call    DSTmprIn;

        goto    $+1;

        bsf     STATUS,C;
        btfss   TEMPER;
        bcf     STATUS,C;

        rrf     i,f;

        call    DL60us;

        decfsz  j,f;
        goto    GetDSLp;

;       поправка на градус вниз
        #ifdef  CORRECTION_enable
        decf    i,f;
        decf    i,f;
        decf    i,f;
        decf    i,f;
        #endif
;       конец поправки

        movf    i,w;
        movwf temperature;        запись считанного байта в temperature
        retlw   0;

;***************************************
; посылка байта датчику
;***************************************
SendDS
        movwf i;
        movlw 8;
        movwf j;
SendDSLp
        call    DSTmprOut;
        nop;

        btfsc   i,0;
        call    DSTmprIn;

        call    DL60us;
        rrf     i,f;
        call    DSTmprIn;

        decfsz  j,f;
        goto    SendDSLp;

        retlw   0;

;***************************************
; установка порта TRIS датчика на выход
;***************************************
DSTmprOut
        movlw 0FDh; устанавливаем TRIS датчика на выход
        andwf tmpTRIS,w;
        movwf tmpTRIS;
        TRIS 06;
        goto $+1;
        nop;
        bcf TEMPER;
        retlw 0;

;***************************************
; установка порта TRIS датчика на выход
;***************************************
DSTmprIn
        movlw 02h;              устанавливаем TRIS датчика на вход
        goto gavLabel_1;


;//////////// -=D=- ////////////////////////////
;включения/выключения СИД
VD1On
        movlw 0xDF;             установка порта TRIS СИД "Единицы" на выход - включение
        goto gavLabel_2;
VD2On
        movlw 0xFE;             установка порта TRIS СИД "Десятки" на выход - включение
        goto gavLabel_2;
VD2Off
        movlw 0x01;             установка порта TRIS СИД "Десятки" на вход - выключение
        goto gavLabel_1;
VD1Off
        movlw 0x20;             установка порта TRIS СИД "Единицы" на вход - выключение
gavLabel_1
        iorwf tmpTRIS,w;
        goto gavLabel_3;
gavLabel_2
        andwf tmpTRIS,w;
gavLabel_3
        movwf tmpTRIS;
        TRIS 06;
        retlw 0;
;//////////// -=D=- ////////////////////////////


;***************************************************
; установка СИД "Единицы", цвет зависит от CurSignFlag
;***************************************************
SetVD1Cur
;       загорание зеленым
        btfss CurSignFlag;
        bcf VD1;
;       загорание красным
        btfsc CurSignFlag;
        bsf VD1;
        retlw 0;

;***************************************************
; установка СИД "Десятки", цвет зависит от SignFlag
;***************************************************
SetVD2Cur
; загорание зеленым
        btfss CurSignFlag;
        bcf VD2;
; загорание красным
        btfsc CurSignFlag;
        bsf VD2;
        retlw 0;

;****************************
; задержка на 60 мкс
;****************************
DL60us
        movlw d'18';
        movwf CountLow;
        decfsz CountLow,f;
        goto $-1;
        retlw 0;

;****************************
; задержка на 500 мкс
;****************************
DL500us
        movlw     d'180';
        movwf   CountLow;
        decfsz  CountLow,f;
        goto    $-1;
        retlw 0;

;****************************
; задержка на 0,25 с
;****************************
DL250ms
        incf            DeviceCounter,f;                        инкремент счетчика устройства
        bcf             DeviceOnPermitFlag;                      сброс флага разрешения включения устройства
        movlw           0xDD;                              сравнение с константой
        subwf           DeviceCounter,w;                        если при вычитании константы заем был
        btfsc           STATUS,C;                                  то установка флага разрешения включения устройства
        bsf             DeviceOnPermitFlag;                      ~50 сек.

        movlw           0x08;
        movwf           CountLow;
DL250ms1
        clrf            TMR0;                           очистка таймера
        btfss           BUTTON;
        bsf             ShortButtonFlag;
DL250ms2
        clrwdt;
        movlw   b'11000111';
        OPTION;
        btfss           TMR0,7;
        goto            DL250ms2;
        decfsz          CountLow,f;
        goto            DL250ms1;
        retlw           0;

;****************************
; задержка на 1 с
;****************************
DLS
        movlw           0x04;
        addwf           DeviceCounter,f;
        bcf             DeviceOnPermitFlag;
        movlw           0xDD;
        subwf           DeviceCounter,w;
        btfsc                   STATUS,C;
        bsf             DeviceOnPermitFlag;
        movlw   0x1F;
        movwf   CountLow;
DelayS1
        clrf            TMR0;
        movlw   b'11000111';
        OPTION;
        btfss           BUTTON;
        bsf             ShortButtonFlag;
DelayS2
        clrwdt;
        btfss           TMR0,7;
        goto            DelayS2;
        decfsz  CountLow,f;
        goto            DelayS1;
        retlw           0;

;*****************************
; отображение температуры
;*****************************
ShowTmpr
        clrwdt;
        call VD1Off;
        call VD2Off;
; ****==== проверка на нули десятков и единиц ====****
; ****==== если десятки или единицы =0, то флаги устанавливаются в ноль ====****
        movf CurrentDec,w;                запись во временные переменные
        movwf i;                        текущих десятков и единиц
        movf CurrentUnits,w;
        movwf j;

        bcf STATUS,Z;              выяснение, равны ли "десятки" или "единицы"
        movf i,f;                          нулю. если =0, то установка соответствующих
        bcf BlinkUnitsFlag;              флагов в "1"
        btfss STATUS,Z;
        bsf BlinkUnitsFlag;
        bcf STATUS,Z;
        movf j,f;
        bcf BlinkDecFlag;
        btfss STATUS,Z;
        bsf BlinkDecFlag;
        clrf tmp;
; *****==== выбор режима мигания           ====****
; *****==== tmp =0 для нулевой температуры ====****
; *****==== tmp =1 для мигания "десятков"  ====****
; *****==== tmp =2 для мигания "единиц" ====****
; *****==== tmp =3 для мигания обоих СИД   ====****
        btfsc BlinkUnitsFlag;      проверка флагов равенства "Дес" или "Ед." нулю
        incf tmp,f;
        btfsc BlinkDecFlag;
        goto BnkSlctD;
        goto BnkCycle;
BnkSlctD
        incf tmp,f;
        incf tmp,f;
; ****==== индикаторы горят цветом знака ====****
; ****==== кр. =1 - положит., зел. =0 - отриц. ====****
BnkCycle
        movf tmp,w;              если tmp=0, то переход на длинное мигание -
        xorlw 0x00;              признак нулевой температуры
        btfsc STATUS,Z;
        goto BnkNone;

        movf tmp,w;              если tmp=1, то переход на мигание "десятков"
        xorlw 0x01;
        btfsc STATUS,Z;
        goto BnkDec;

        movf tmp,w;              если tmp=2, то переход на мигание "единиц"
        xorlw 0x02;
        btfsc STATUS,Z;
        goto BnkUnits;

        movf tmp,w;              если tmp=3, то переход на мигание обоих СИД
        xorlw 0x03;
        btfsc STATUS,Z;
        goto BnkBoth;

; ****==== собственно, мигание светодиодов ====****
; ****==== мигают оба ====****
BnkBoth
        call VD1On;              устанавливаем TRIS обоих СИД на выход - включаем
        call VD2On;
        call SetVD1Cur;
        call SetVD2Cur;
        call DL250ms;      задержка

        call VD1Off;            устанавливаем TRIS обоих СИД на вход - отключаем
        call VD2Off;
        call DL250ms;      задержка

        decf i,f;                  декремент "дес." и "ед."
        decf j,f;
        movf i,f;                  проверка "дес." на равентсво "0"
        btfsc STATUS,Z;  если "дес." =0, то переход, иначе
        goto BnkBothT;    проверка "ед." на равентсво "0"
        bcf STATUS,Z;      если "ед." =0, то tmp =1 (мигание "Дес.")
        movf j,f;                  иначе переход на начало цикла
        btfss STATUS,Z;
        goto BnkCycle;
        movlw 0x01;
        movwf tmp;
        goto BnkCycle;
BnkBothT
        bcf STATUS,Z;      проверка "ед." на равенство "0"
        movf j,f;                  если "ед." =0, то переход на конец цикла миганий
        btfsc STATUS,Z;  иначе tmp =2 (мигание "Ед."), переход на начало цикла
        goto BnkEnd;
        movlw 0x02;
        movwf tmp;
        goto BnkCycle;
; ****==== мигают "десятки" ====****
BnkDec
        call VD2On;              устанавливаем TRIS СИД "Десятков" на выход - включаем
        call SetVD2Cur;
        call DL250ms;      задержка

        call VD2Off;            устанавливаем TRIS СИД "Десятков" на вход - выключаем
        call DL250ms;      задержка

        decfsz i,f;              уменьшение "Дес.", если "Дес." =0, то переход в конец цикла
        goto BnkCycle;    иначе переход в начало цикла
        goto BnkEnd;
; ****==== мигают "единицы" ====****
BnkUnits
        call VD1On;              устанавливаем TRIS СИД "Единиц" на выход - включаем
        call SetVD1Cur;
        call DL250ms;      задержка

        call VD1Off;            устанавливаем TRIS СИД "Единиц" на вход - выключаем
        call DL250ms;      задержка

        decfsz j,f;              уменьшение "Ед.", если "Ед." =0, то переход в конец цикла
        goto BnkCycle;    иначе переход в начало цикла
        goto BnkEnd;
; ****==== оба СИД горят постоянно - признак нулевой температуры ====****
BnkNone
        call VD1On;
        call VD2On;              устанавливаем TRIS обоих СИД на выход - включаем
        call SetVD1Cur;
        call SetVD2Cur;
        call DLS;                  задержка
                call VD1Off;                    выключение СИД
                call VD2Off;
BnkEnd
        call DLS;
        retlw 0;


;************************************************
; конвертирование температуры в десятки и единицы
;************************************************
Cnvrt2DU
        clrf    CurrentDec;
        clrf    CurrentUnits;
        movf    temperature,w;
        movwf j;
CnvrtFlg
        bsf     CurSignFlag;
        btfsc   j,7;
        goto    CnvrtFlM;
        goto    Cnvrt2D;
CnvrtFlM
        bcf     CurSignFlag;
        comf    j,f;                    инвертирование температуры (j)
        incf    j,f;
Cnvrt2D
        incf    j,f;
        movlw d'10';                     запись в i числа 10 для счетчика
        movwf i;
Cnvrt2U
        decf    j,f;                    декремент температуры
        movf    j,f;                    проверка j на ноль
        btfsc   STATUS,Z;               если =1, то переход в конец подпроги
        goto    CnvrtEnd;
        incf    CurrentUnits,f;         инкремент Units
        decfsz  i,f;                    декремент i с проверкой на ноль
        goto    Cnvrt2U;                переход на инкремент юнитов
        decf    j,f;                    декремент температуры
        movf    j,f;                    проверка j на ноль
        btfsc   STATUS,Z;               если =1, то переход на преоверку единиц
        goto    CnvrtEnd;
        clrf    CurrentUnits;
        incf    CurrentDec,f;           инкремент Dec
        goto    Cnvrt2D;
CnvrtEnd
        movf    CurrentUnits,w;         если количество единиц =10, то инкремент десятков
        xorlw   d'10';                  и обнуление единиц
        btfss   STATUS,Z;               иначе переход в конец
        goto    CnvrtEnd2;
        clrf    CurrentUnits;
        incf    CurrentDec,f;
CnvrtEnd2
        goto    EndCnvrt;


; *******************************************
; ***  В К Л Ю Ч Е Н И Е   Д Е В А Й С А  ***
; *******************************************
DvcOn
; проверка необходимых флагов
        btfss   DeviceOnPermitFlag;              если флаг разрешения включения устройства =0
        goto    DvcMng5;                           то переход в конец подпрограммы
; ***  включение девайса ***
        clrf    DeviceCounter;            очищение счетчика устройства
        movlw b'11101111';
        andwf   tmpTRIS,w;
        movwf   tmpTRIS;
        TRIS    06;                     порт девайса на выход
        bsf     DeviceOnFlag;
        bcf     DEVICE;                  если =1, то включение девайса
        goto    DvcMng21;


; *********************************************
; *** У П Р А В Л Е Н И Е   Д Е В А Й С О М ***
; *********************************************
DvcMng
        clrf tmp;
        bcf tmpFlag;                    сброс флага (первоначальное сравнение прошло)
        movf ThresholdUnits,w;
        movwf j;                        занесение в j предела темп. единиц
        movf ThresholdDec,w;
        movwf i;                        занесение в i предела темп. десятков
        xorlw 0x00;                      проверка "десятков" на ноль
        skpz;                      если рез-т "искл. или" был не нулевой, то
        goto DvcMng01;            переход на выполнение конвертации
        movf j,w;                          иначе проверка "единиц" на ноль
        xorlw 0x00;
        skpz;                      если рез-т "искл. или" был не нулевой, то
        goto DvcMng02;            переход на выполнение конвертации
        goto DvcMng1;
; перевод из десятков/единиц в одно число, нужно для сравнения с тек. температ.
DvcMng01
        movlw 0x0A;
        movwf CountLow;
        incf tmp,f;
        decfsz CountLow,f;
        goto $-2;
        decfsz i,f;
        goto $-6;
        movf j,w;                          иначе проверка "единиц" на ноль
        xorlw 0x00;
        skpnz;                     если рез-т "искл. или" был не нулевой, то
        goto DvcMng1;
DvcMng02
        incf tmp,f;
        decfsz j,f;
        goto $-2;
; проверка знака сравниваемой температуры
; если отрицат., то 7 бит сбрасывается, иначе 7 бит устанавливается
DvcMng1
        btfss SignFlag;          проверка флага знака запрогр.темпер.
        comf tmp,f;                      если =0, то инвертирование
        btfss SignFlag;          проверка флага знака запрогр.темпер.
        incf tmp,f;                      увеличение на 1 (требуется для правильности)
        btfss SignFlag;          проверка флага знака запрогр.темпер.
        bcf tmp,7;                        если =0, то обнуляем последний бит
        btfsc SignFlag;          проверка флага знака запрогр.темпер.
        bsf tmp,7;                        если =1, то устанавливаем последний бит
        btfss CurSignFlag;                проверка флага знака тек. температуры
        bcf temperature,7;                если =0, то устанавливаем последний бит
        btfsc CurSignFlag;                проверка флага знака тек. температуры
        bsf temperature,7;                если =1, то устанавливаем последний бит
DvcMng12
        btfss Check3EndFlag;
        goto DvcMng3;
; включение девайса при превышении предела
DvcMng2
        movf temperature,w;             занос в акк. значения тек. температуры
        btfsc ModeFlag;         проверка режима
        movf tmp,w;                     если охл., то занос в акк. порог. знач.
        btfss ModeFlag;         если флаг режима =1 (нагр.), то
        subwf tmp,w;                    порог.температура - тек.температура = ?
        btfsc ModeFlag;         если флаг режима =0 (охл.), то
        subwf temperature,w;            тек.температура - порог.температура = ?
        btfss STATUS,C;          если заем был (результат отриц., C=0), то
        goto DvcOn;                      включение девайса
        btfsc STATUS,Z;          проверка бита Z
        goto DvcOn;                      если рез-т нулевой (Z=1), то включение девайса
DvcMng21
        bsf Check3EndFlag;                то уст. флага Check3EndFlag
        btfsc DeviceOnFlag;              если =1, то
        goto DvcMng3;              переход на проверку возрастания
DvceOff
        movlw b'00010000';                ставим порт девайса на вход - выключение
        iorwf tmpTRIS,w;
        movwf tmpTRIS;
        TRIS 06;
        bcf DeviceOnFlag;
        bsf DEVICE;                      если =1, то выкл. девайса
        goto DvcMng5;              переход в конец подпрограммы
; проверка на возрастание на 3 градуса
DvcMng3
        bcf Check3EndFlag;                сброс флага "сравнивать температуру можно"
        btfsc tmpFlag;            проверка временного флага - показывает, прошла ли проверка на +/-3 градуса
        goto DvcMng5;              если =1 (проверка прошла), переход ниже
        bcf DeviceOnFlag;
        bsf tmpFlag;                    уст. временного флага - проверка +/-3 прошла
;;        btfss ModeFlag;
;;        goto DvcMng4;
;;        incf tmp,f;                      увеличение на 2
        incf tmp,f;
        goto DvcMng2;              переход на метку контроля за устройством
; проверка на убывание на 3 градуса
;;DvcMng4
;;        decf tmp,f;                      уменьшение на 2 знач. tmp
;        decf tmp,f;
;;        goto DvcMng2;              переход на метку контроля за устройством
; конец подпрограммы
DvcMng5
        btfsc CurSignFlag;                проверка флага знака тек. температуры
        bcf temperature,7;                если =0, то устанавливаем последний бит
        btfss CurSignFlag;                проверка флага знака тек. температуры
        bsf temperature,7;                если =1, то устанавливаем последний бит

        goto EndDvc;


; *********************************************************************
; ***  запись в EEPROM дефолтных значений                           ***
; *********************************************************************

EEPROMW0
;	ждем, пока отпустят кнопку
	clrwdt;
        btfss   BUTTON;
        goto	EEPROMW0;
        call	DL250ms;
        btfss   BUTTON;
        goto	EEPROMW0;
;	обнуляем флаг короткого нажатия, чтобы не было показа температуры
        bcf	ShortButtonFlag;
;	устанавливаем дефолтные значения        
        clrf ThresholdDec;
        movlw 0x02;
        movwf ThresholdUnits;
        bsf ModeFlag;
        bsf SignFlag;

        bsf      STATUS,PA0;

;          запись маркера
        clrf    EEADDR;
        movlw   0x00;
        movwf   EEDATA;
        call    WRITE_BYTE;
        call    Dl4ms;

;          запись флагов режима и знака
        incf    EEADDR,f;
        clrf    EEDATA;
        bsf      EEDATA,0;
        bsf      EEDATA,1;
        call    WRITE_BYTE;
        call    Dl4ms;

;          запись десятков
        incf    EEADDR,f;
        clrf    EEDATA;
        call    WRITE_BYTE;
        call    Dl4ms;

;          запись единиц
        incf    EEADDR,f;
        movlw   0x02;
        movwf   EEDATA;
        call    WRITE_BYTE;
        call    Dl4ms;

        bcf     STATUS,PA0;

        call DLS;
        goto Start;


;  ______    ___         __    ______     ________
; |__/\__|   ||\\        ||   |__/\__|   |___/\___|
;    ||      || \\       ||      ||          ||
;    ||      ||  \\      ||      ||          ||
;    ||      ||   \\     ||      ||          ||
;    ||    инициализация регистров и флагов  ||
;    ||      ||     \\   ||      ||          ||
;    ||      ||      \\  ||      ||          ||
;    ||      ||       \\ ||      ||          ||
;  __||__    ||        \\||    __||__        ||
; |__\/__|   \/         \|/   |__\/__|       \/

Init
        call    DLS;
        movlw b'00111111';
        movwf tmpTRIS;
        TRIS    06;
        movlw b'11000111';
        OPTION;
        clrf    FLAGS;
        clrf    FLAGS2;
        clrf    FLAGS3;
        movlw   0xAA;
        movwf   DeviceCounter;
        movlw   0x02;
        movwf   ThresholdUnits;
        clrf    ThresholdDec;
        bsf     SignFlag;
        bsf     ModeFlag;

;        call    DL250ms;

ResetChk
        btfsc BUTTON;
        goto EndResetChk;
        goto EEPROMW0;
EndResetChk
        BANK1;
        goto EEPROMR;



;       О С Н О В Н О Й   Б Л О К    П Р О Г Р А М М Ы



Start
        goto TmprMsr;
EndTmpr
        goto Cnvrt2DU;
EndCnvrt
        call ShowTmpr;
        goto DvcMng;
EndDvc
        clrf tmp;
        clrf TMR0;
        movlw b'11000111';
        OPTION;
        clrf TMR0last;
        clrf ButtonCounterLow;
        clrf ButtonCounterHigh;
BtnCycle
        clrwdt;                                 очистка таймера
        movf    TMR0,w;                 заносим в аккумулятор содержимое TMR0
        bcf     STATUS,Z;                       обнуляем бит нулевого результата
        xorwf   TMR0last,w;                     если TMR0last и TMR0 различаются, то
        btfsc   STATUS,Z;                       бит нулевого результата =0 -> цикл выполняется дальше
        goto    BtnCycle;                       иначе переход на начало цикла выбора режима

        incf    CountLow,f;                     инкрементируем младший байт счетчика
        btfss   CountLow,7;                     если 7 бит младшего счетчика =0, то
        goto    BtnCyL1;                        переходим дальше, иначе
        incf    CountHigh,f;                    инкрементируем старший байт счетчика
        clrf    CountLow;                       обнуляем младший байт счетчика
BtnCyL1
        movf    TMR0,w;                 запихиваем содержимое TMR0 в TMR0last
        movwf TMR0last;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;begin
        btfsc   ShortButtonFlag;
        goto    BtnCyShort;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;end
        btfsc   BUTTON;                 если кнопка не нажата (=0), то
        goto    BtnCyL3;                        переход на очищение старшего счетчика,
        incf    ButtonCounterLow,f;             иначе инкрементируем младший байт счетчика кнопки
        btfss   ButtonCounterLow,7;             если 7 бит младшего байта счетчика =0,
        goto    BtnCyPress;                     то ничего не делаем (переход),
        incf    ButtonCounterHigh,f;            иначе инкрементируем старший байт счетчика
        clrf    ButtonCounterLow;               и очищаем младший байт счетчика
BtnCyPress
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;begin
        bcf     ShortButtonFlag;
        movf    ButtonCounterHigh,f;
        btfss   STATUS,Z;
        bsf     ShortButtonFlag;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;end
        btfss   CountHigh,6;            110 0000 (0x60)- 3 секунды, т.е. если 6 бит =0,
        goto    BtnCyShort;             то переход на проверку короткого нажатия,
        btfss   CountHigh,5;            иначе если 5 бит в старшем счетчике =0,
        goto    BtnCyShort;             то переход на проверку короткого нажатия,
        BANK1;
        goto    Program;                если 3 сек. прошло, то переход на программирование
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;begin
BtnCyShort
        btfsc   BUTTON;
        goto    ShowAdj;
        bcf     ShortButtonFlag;
        goto    BtnCycle;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;end
BtnCyL3
        clrf ButtonCounterHigh;  очищение старшего счетчика кнопки
        goto EndChkBt;
EndChkBt
        ;
EndPrgm
        goto Start;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;begin
ShowAdj
;;        bsf     BeginNotEndFlag;
;;ShowAdj1
;;;       movlw           0x02;
;;;       movwf           tmp;
;;;       обнуляем таймер, таймерную переменную и счетчик
;;;ShowAdj2
;;        clrf    TMR0;
;;        movlw b'11000111';
;;        OPTION;
;;        clrf    TMR0last;
;;        clrf    CountHigh;
;;;       включаем СИД
;;        call    VD1On;
;;        call    VD2On;
;;ShowAdjYellowCycle
;;        clrwdt;                                 
;;        movf    TMR0,w;                 заносим в аккумулятор содержимое TMR0
;;        xorwf   TMR0last,w;                     если TMR0last и TMR0 различаются, то
;;        btfsc   STATUS,Z;                       бит нулевого результата =0 -> цикл выполняется дальше
;;        goto    ShowAdjYellowCycle;             иначе переход на начало цикла выбора режима
;;        movf    TMR0,w;                 запихиваем содержимое TMR0 в TMR0last
;;        movwf   TMR0last;
;;
;;        btfss   VD1;                            инвертирование цвета обоих СИД (желтый)
;;        goto    $+4;
;;        bcf     VD1;                            включение зеленых единиц;       -=D=-
;;        bcf     VD2;                            включение зеленых десятков;     -=D=-
;;        goto    $+3;
;;        bsf     VD1;                            включение красных единиц;       -=D=-
;;        bsf     VD2;                            включение красных десятков;     -=D=-
;;
;;        incf    CountLow,f;                     инкрементируем младший байт счетчика
;;        btfss   CountLow,7;                     если 7 бит младшего счетчика =0, то
;;        goto    ShowAdjYellowCycle;             переходим на начало цикла, иначе
;;        incf    CountHigh,f;                    инкрементируем старший байт счетчика
;;        clrf    CountLow;                       обнуляем младший байт счетчика
;;
;;
;;ShowAdjYellowCycle2
;;        btfss   CountHigh,4;
;;        goto    ShowAdjYellowCycle;
;;
;;        call    VD1Off;
;;        call    VD2Off;
;;
;;;       call    DL250ms;
;;;       call    DL250ms;
;;;       decfsz  tmp,f;
;;;       goto    ShowAdj2;
;;
;;        call    DLS;
;;
;;        btfss   BeginNotEndFlag;
;;        goto    ShowAdjEnd;
;;;       горение режима
;;ShowAdjBegin
;;;       задержка 1 с
;;;
;;        call    VD2On;                          включается СИД
;;        btfss   ModeFlag;
;;        goto    $+3;
;;        bsf     VD2;
;;        goto    $+2;
;;        bcf     VD2;
;;        call    DLS;
;;        call    VD2Off;
;;        call    DLS;

;       выключение СИД
        call    VD1Off;
        call    VD2Off;
        call	DLS;

;       записываем ThresholdDec и ThresholdUnits в CurrentDec и CurrentUnits
        movf ThresholdDec,w;
        movwf CurrentDec;
        movf ThresholdUnits,w;
        movwf CurrentUnits;
;       установка текущего цвета СИД в цвет знака
        bsf     CurSignFlag;
;;        btfss   SignFlag;
;;        bcf     CurSignFlag;

;       показ температуры
        call    ShowTmpr;
;;        bcf     BeginNotEndFlag;
;;        goto    ShowAdj1;

ShowAdjEnd
	call	DLS;
        bcf     ShortButtonFlag;        обнуляем флаг короткого нажатия
        goto    Start;                  переход на начало программы
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;end


; ****==== начало кода с адреса 0x200 - для подпрограмм ****====
        org 0x200

;******************************************
;***  подпрограммы для работы с EEPROM  ***
;***  исходник:  FL51XINC.ASM  V1.10    ***
;***  переведен: 04-08-04, ib      ***
;******************************************
; Обмен данными с EEPROM основывается на I2C-протоколе с подтверждением (Ack)
;
; WRITE_BYTE: запись байта
;          Inputs:  EEPROM Address   EEADDR
;               EEPROM Data       EEDATA
;          Outputs: Return 01 in W if OK, else return 00 in W
;
; READ_RANDOM: чтение байта с заданного адреса
;          Inputs:  EEPROM Address      EEADDR
;          Outputs: EEPROM Data    EEDATA
;               Return 01 in W if OK, else return 00 in W
;
; Замечание:
;          подпрограммы для работы с EEPROM установят бит 7 в регистре PC_OFFSET,
;          если подтверждение от EEPROM пришло, иначе этот бит будет очищен. Этот бит
;          может быть проверен вместо проверки значения, возвращаемого в W

; ****====      установка управляющих байтов EEPROM  ====****
WRITE_BYTE
        MOVLW   B'10000000'   ; сдвиг PC для записи байта.  EE_OK: bit7 = '1'
        GOTO    INIT_WRITE_CONTROL

READ_RANDOM
        MOVLW   B'10000011'   ; сдвиг PC для случайного чтения.  EE_OK: bit7 = '1'

INIT_WRITE_CONTROL
        MOVWF   PC_OFFSET        ; загрузка в регистр сдвига PC, значение записано в W
        MOVLW   B'10100000'   ; Control byte with write bit, bit 0 = '0'

START_BIT
        BCF      SDA  ; Start-бит, SDA и SCL уст. в '1'

; ****====  установка выходных данных (упр. байт, адрес или данные) и счетчика  ====****
PREP_TRANSFER_BYTE
        MOVWF   EEBYTE  ; Byte to transfer to EEPROM already in W
        MOVLW   .8              ; Counter to transfer 8 bits
        MOVWF   COUNTER

; ****====  Запись данных (упр., адрес или данные)  ====****
OUTPUT_BYTE
        BCF      SCL  ; уст. SCL = low, пока данные устанавливаются
        RLF      EEBYTE, F       ; сдвиг влево, старший бит заносится в бит переноса
        BCF      SDA  ; установка SDA = low, если есть сдвиг
        SKPNC            ;   a '1', then:
        BSF      SDA  ; уст. SDA = high, иначе оставить
        NOP
        BSF      SCL  ; clock data into EEPROM
        DECFSZ  COUNTER, F      ; повторить, пока весь байт не будет записан
        GOTO    OUTPUT_BYTE
        NOP

; ****====  Проверка подтверждения  ====****
        BCF      SCL  ; SCL=low, 0.5us<действие_ack<3us
        NOP
        BSF      SDA

        GOTO    $+1              ; М.б. нужна для SCL Tlow на маленьком напряжении
        BSF      SCL    ; уст. SCL в 1, EEPROM ack пока еще действует
        BTFSC   SDA     ; проверка SDA на ack (low)
        BCF      PC_OFFSET,EE_OK ; если SDA!=low (no ack), уст. флага ошибки
        BCF      SCL    ; уст. SCL в 0, EEPROM освобождает шину
        BTFSS   PC_OFFSET,EE_OK ; если ошибок нет - продолжать, иначе стоп-бит
        GOTO    STOP_BIT


; ****====  Уст. сдвига счетчика програм, основывается на действующем режиме EEPROM  ====****
        MOVF    PC_OFFSET,W
        ANDLW   B'00001111'
        ADDWF   PCL, F
        GOTO    INIT_ADDRESS      ;PC offset=0, запись завершена посылка адреса
        GOTO    INIT_WRITE_DATA   ;PC offset=1, запись адреса завершена, посылка данных
        GOTO    STOP_BIT          ;PC offset=2, запись завершена, посылка стоп-бита
        GOTO    INIT_ADDRESS      ;PC offset=3, запись управляющего байта завершена, посылка адреса
        GOTO    INIT_READ_CONTROL ;PC offset=4, посылка управляющего байта чтения
        GOTO    READ_BIT_COUNTER  ;PC offset=5, уст. бита счетчика и чтения
        GOTO    STOP_BIT          ;PC offset=6, случ. чтение завершено, посылка стоп-бита


; ****====  Инициализация EEPROM (адрес, данные, упр. байт)  ====****
INIT_ADDRESS
        INCF    PC_OFFSET, F ; инкремент сдвига PC на 2 (запись) или 4 (чтение)
        MOVF    EEADDR,W         ; запись в EEPROM адреса в W, готов передавать в EEPROM
        GOTO    PREP_TRANSFER_BYTE


INIT_WRITE_DATA
        INCF    PC_OFFSET, F ; инкремент сдвига PC для перехода на STOP_BIT
        MOVF    EEDATA,W         ; запись данных EEPROM в W, готов передавать в EEPROM
        GOTO    PREP_TRANSFER_BYTE

INIT_READ_CONTROL
        BSF      SCL ; подъем SCL
        nop
        BSF      SDA ; подъем SDA
        INCF    PC_OFFSET, F ; инкремент сдвига PC для перехода на READ_BIT_COUNTER
        MOVLW   B'10100001'  ; уст. упр. бита чтения, готов к передаче в EEPROM
        GOTO    START_BIT       ; bit 0 = '1' - для операции чтения


; ****====  Чтение данных из EEPROM  ====****
READ_BIT_COUNTER
        BSF      SDA
        NOP
        BSF      SCL ; уст. бита данных в 1, поэтому не переводим шину данных в ноль
        MOVLW   .8         ; уст. счетчика, поэтому 8 bits будут считаны EEDATA
        MOVWF   COUNTER

READ_BYTE
        BSF      SCL ; подъем SCL, SDA действительно. SDA пока принимает ack
        SETC             ; Assume bit to be read = 1
        BTFSS   SDA ; проверка SDA на 1
        CLRC             ; если SDA != 1, тогда очистить бит переноса
        RLF      EEDATA, F      ; сдвиг бита переноса (=SDA) в EEDATA;
        BCF      SCL ; сброс SCL
        BSF      SDA ; уст. SDA
        DECFSZ  COUNTER, F   ; декремент счетчика
        GOTO    READ_BYTE       ; прочитать следующий бит, если не закончено чтения байта

        BSF      SCL
        NOP
        BCF      SCL

; ****====  Генерация STOP-бита и возврат  ====****
STOP_BIT
        BCF      SDA ; SDA=0, on TRIS, to prepare for transition to '1'
        BSF      SCL      ; SCL = 1, подготовка к STOP-биту
        GOTO    $+1       ; необходимо 4 nop для I2C spec Tsu:sto = 4.7us
        GOTO    $+1
        BSF      SDA ; Stop-бит, SDA переводится в '1', пока SCL на высоком уровне

        BTFSS   PC_OFFSET,EE_OK ; проверка на ошибку
        RETLW   NO                ; если есть ошибка, то возврат NO
        RETLW   OK                ; если нет ошибки, то возврат OK

; Замечание: SDA и SCL все еще управляются ведущим, оба настроены на выход.
; ****====  Конец подпрограмм для работы с EEPROM  ====****


; ***********************
; ***  Задержка 4 мс  ***
; ***********************
Dl4ms
        clrwdt;
        movlw     d'33';
        movwf   CountHigh;
        movlw     d'33';
        movwf   CountLow;
        decfsz  CountLow,w;
        goto    $-2;
        decfsz CountHigh,w;
        goto    $-6;
        retlw 0;

; включение зеленых единиц
;VD1GREEN
;               bcf     VD1;
;               bsf     VDCOM;
;               retlw   0;

; включение красных единиц
;VD1RED
;               bsf     VD1;
;               bcf     VDCOM;
;               retlw   0;

; включение зеленых десятков
;VD2GREEN
;               bcf     VD2;
;               bsf     VDCOM;
;               retlw   0;

; включение красных десятков
;VD2RED
;               bsf     VD2;
;               bcf     VDCOM;
;               retlw   0;


; ***********************************************
; ****  запись полученных значений во Flash  ****
; ***********************************************

EEPROMW
        clrwdt;
; ****==== запись флага режима и знака====****
        movlw   0x01;
        movwf   EEADDR;
        clrf    EEDATA;
        bsf      EEDATA,0;
;;        btfss   ModeFlag;
;;        bcf      EEDATA,0;
        bsf      EEDATA,1;
;;        btfss   SignFlag;
;;        bcf     EEDATA,1;
        call    WRITE_BYTE;
        call    Dl4ms;

; ****==== запись "десятков" ====****
        movlw 0x02;
        movwf EEADDR;
        movf ThresholdDec,w;
        movwf EEDATA;
        call WRITE_BYTE;
        call Dl4ms;

; ****==== запись "единиц" ====****
        movlw 0x03;
        movwf EEADDR;
        movf ThresholdUnits,w;
        movwf EEDATA;
        call WRITE_BYTE;
        call Dl4ms;

; ****==== запись маркера ====****
        clrf    EEADDR;
        clrf 	EEDATA;
        call 	WRITE_BYTE;
        call 	Dl4ms;

        BANK0;
        goto 	EndPrgm;


; **********************************
; ***  Ч Т Е Н И Е  И З  EEPROM  ***
; **********************************

EEPROMR
; чтение байта-маркера
        clrf    EEADDR;
; условный дефайн для работы на эмуляторе
        #ifdef  EE_enable
        call    READ_RANDOM;
        btfsc   EEDATA,0;
        #endif
        goto    Default;                если переменная 0х00 в ПЗУ пуста, то переход на дефолтные значения
; чтение из EEPROM
NDefault
        incf    EEADDR,f;                                  иначе чтение значений из EEPROM
        call    READ_RANDOM;
        bsf      ModeFlag;
;;        btfss   EEDATA,0;
;;        bcf      ModeFlag;
        bsf      SignFlag;
;;        btfss   EEDATA,1;
;;        bcf      SignFlag;
        call    Dl4ms;

        incf    EEADDR,f;
        call    READ_RANDOM;
        movf    EEDATA,w;
        movwf   ThresholdDec;
        call    Dl4ms;

        incf    EEADDR,f;
        call    READ_RANDOM;
        movf    EEDATA,w;
        movwf   ThresholdUnits;
        call    Dl4ms;

        BANK0;
        goto Start;
; дефолтные значения
Default
        clrf ThresholdDec;                по умолчанию стоят +2 градуса на нагревание
        movlw 0x02;
        movwf ThresholdUnits;
        bsf SignFlag;
        bsf ModeFlag;
        BANK0;
        goto Start;


; **********************************************
; ***  программирование датчика температуры  ***
; **********************************************
Program
        clrf TMR0;
        movlw b'11000111';
        OPTION;
        clrf TMR0last;
        clrf ButtonCounterHigh;
        clrf CountLow;
        clrf CountHigh;
        bsf CurrentColor2;
        bsf ModeNotSign;

        BANK0;           включение "Единиц" и "десятков"
        call VD1On;
        call VD2Off;
   	bsf VD1;
        bsf VD2;                    включение красных десятков
        BANK1;
PrgWait
	clrwdt;
        btfss   BUTTON;
        goto	PrgWait;
        BANK0;
        call DL250ms;
        BANK1;
        btfss   BUTTON;
        goto	PrgWait;

;;PrgBtnCy
;;        clrwdt;                         очистка таймера
;;        movf TMR0,w;                    заносим в аккумулятор содержимое TMR0
;;        bcf STATUS,Z;                   обнуляем бит нулевого результата
;;        xorwf TMR0last,w;               если TMR0last и TMR0 различаются, то
;;        btfsc STATUS,Z;         бит нулевого результата =0 -> цикл выполняется дальше
;;        goto PrgBtnCy;                  иначе переход на начало цикла выбора режима
;;
;;        btfss VD1;                      инвертирование цвета обоих СИД (желтый)
;;        goto $+4;
;;        bcf     VD1;                    включение зеленых единиц;       -=D=-
;;        bcf     VD2;                    включение зеленых десятков;-=D=-
;;        goto $+3;
;;        bsf     VD1;                    включение красных единиц;       -=D=-
;;        bsf     VD2;                    включение красных десятков;-=D=-
;;
;;        incf CountLow,f;                инкрементируем младший байт счетчика
;;        btfss CountLow,7;               если 7 бит младшего счетчика =0, то
;;        goto PrgBtn1;                   переходим дальше, иначе
;;        incf CountHigh,f;               инкрементируем старший байт счетчика
;;        clrf CountLow;                  обнуляем младший байт счетчика
;;PrgBtn1
;;        movf TMR0,w;                    запихиваем содержимое TMR0 в TMR0last
;;        movwf TMR0last;
;;
;;        btfsc BUTTON;                   если кнопка не нажата (=0), то
;;        goto PrgBtn3;                   переход на очищение старшего счетчика,
;;        incf ButtonCounterLow,f;        иначе инкрементируем старший байт счетчика кнопки
;;        btfss ButtonCounterLow,7;       если 7 бит младшего байта счетчика =0,
;;        goto PrgBtn2;                   то ничего не делаем (переход),
;;        incf ButtonCounterHigh,f;       иначе инкрементируем старший байт счетчика
;;        clrf ButtonCounterLow;          и очищаем младший байт счетчика
;;PrgBtn2
;;        btfss ButtonCounterHigh,5;      если 5 бит в старшем счетчике кнопы =0,
;;        goto PrgBtnCy;                  то переход на начало цикла,
;;PrgBtn3
;;        BANK0;
;;        call VD1Off;
;;        call VD2Off;
;;        call DLS;
;;        call VD1On;
;;        call VD2On;
;;        BANK1;
;;
;;        clrf TMR0;
;;        movlw b'11000111';
;;        OPTION;
;;        clrf TMR0last;
;;        clrf ButtonCounterHigh;
;;
;;
;;
;;;;; ****==== В Ы Б О Р   Р Е Ж И М А  ====****
;;; ****==== 0 - охлаждение (зеленый) ====****
;;; ****==== 1 - нагревание (красный) ====****
;;; ****==== выбор знака:          ====****
;;; ****==== 0 - минус (зеленый)    ====****
;;; ****==== 1 - плюс (красный)      ====****
;;CycleMde
;;        clrwdt;                  очистка таймера
;;        movf TMR0,w;                    заносим в аккумулятор содержимое TMR0
;;        xorwf TMR0last,w;                  если TMR0last и TMR0 различаются, то
;;        btfsc STATUS,Z;          бит нулевого результата =0 -> цикл выполняется дальше
;;        goto CycleMde;            иначе переход на начало цикла выбора режима
;;
;;        incf CountLow,f;                инкрементируем младший байт счетчика
;;        btfss CountLow,7;                  если 7 бит младшего счетчика =0, то
;;        goto CyclMdL1;            переходим дальше, иначе
;;        incf CountHigh,f;                  инкрементируем старший байт счетчика
;;        clrf CountLow;            обнуляем младший байт счетчика
;;CyclMdL1
;;        movf TMR0,w;                    запихиваем содержимое TMR0 в TMR0last
;;        movwf TMR0last;
;;
;;        btfsc BUTTON;              если кнопка не нажата (=0), то
;;        goto CyclMdL3;            переход на очищение старшего счетчика,
;;        incf ButtonCounterLow,f;        иначе инкрементируем старший байт счетчика кнопки
;;        btfss ButtonCounterLow,7;          если 7 бит младшего байта счетчика =0,
;;        goto CyclMdL2;            то ничего не делаем (переход),
;;        incf ButtonCounterHigh,f;          инкрементируем старший байт счетчика
;;        clrf ButtonCounterLow;    и очищаем младший байт счетчика
;;CyclMdL2
;;        ;                                  32*128*256 = 1000000 циклов = 1 сек
;;        ;                                  d'32' = b'100000', сравниваем 5 бит
;;        btfss ButtonCounterHigh,0;        если =0, то
;;        goto CyMdVD1;              переход на инвертирование СИД,
;;        ;                                  иначе время нажатия кнопки больше 1 сек.
;;        btfss ModeNotSign;                если флаг режим/знак =0,
;;        goto CySgSlct;            то переход по метке "выбор знака"
;;        bcf ModeFlag;              сброс флага режима (охлаждение)
;;        btfsc CurrentColor2;            если тек. цвет2 =1, то установка
;;        bsf ModeFlag;              флага режима в 1 (нагревание)
;;        clrf ButtonCounterLow;    сброс счетчиков
;;        clrf ButtonCounterHigh;  сброс счетчиков
;;
;;        BANK0;
;;        call VD1Off;                    отключение "Единиц"
;;        call DLS;                          задержка на 1 секунду
;;        call VD1On;                      включение "Единиц"
;;        BANK1;
;;
;;        goto SignSlct;            переход на выбор знака
;;
;;CySgSlct;          выбор знака (Cycle Sign Select)
;;        bcf SignFlag;              сброс флага знака (минус)
;;        btfsc CurrentColor2;            если тек. цвет =1, то установка
;;        bsf SignFlag;              флага знака в 1 (плюс)
;;        clrf ButtonCounterLow;    сброс счетчиков
;;        clrf ButtonCounterHigh;  сброс счетчиков
;;
;;        BANK0;
;;        call VD2Off;                    выключение "Десятков"
;;        call DLS;                          задержка 1 сек.
;;        call VD2On;                      включение "десятков"
;;        BANK1;
;;
;;        goto SignSlct;            переход на выбор знака
;;
;;CyclMdL3
;;        clrf ButtonCounterHigh;  очищение старшего счетчика кнопки
;;
;;CyMdVD1
;;        btfsc CurrentColor;             процедура инвертирования текущего цвета
;;        goto CyMdVD1F;
;;        bsf CurrentColor;
;;        btfss ModeNotSign;
;;        bsf     VD2;                    включение красных десятков
;;        btfsc ModeNotSign;
;;        bsf     VD1;включение красных единиц
;;        goto CyMdVD2;
;;CyMdVD1F
;;        bcf CurrentColor;
;;        btfss ModeNotSign;
;;        bcf     VD2;                    включение зеленых десятков
;;        btfsc ModeNotSign;
;;        bcf     VD1;                    включение зеленых единиц
;;
;;CyMdVD2
;;        btfss CountHigh,6;              110 0000 (0x60)- 3 секунды, т.е. если 6 бит =0,
;;        goto CycleMde;                  то переход на начало цикла,
;;        btfss CountHigh,5;              иначе если 5 бит в старшем счетчике =0,
;;        goto CycleMde;                  то переход на начало цикла,
;;        clrf CountHigh;                 иначе обнуление счетчиков
;;        clrf CountLow;                  и
;;        btfsc CurrentColor2;            процедура инвертирования тек. цвета 2
;;        goto CyMdVD2F;
;;        bsf CurrentColor2;
;;        btfss ModeNotSign;
;;        goto CySgVD1N;
;;        bsf     VD2;                    включение красных десятков
;;        goto CycleMde;
;;CySgVD1N
;;        bsf     VD1;                    включение красных единиц
;;        goto CycleMde;
;;CyMdVD2F
;;        bcf CurrentColor2;
;;        btfss ModeNotSign;
;;        goto CySgVD1F;
;;        bcf     VD2;                    включение зеленых десятков
;;        goto CycleMde;
;;CySgVD1F
;;        bcf     VD1;                    включение зеленых единиц
;;        goto CycleMde;
;;SignSlct
;;        BANK0;
;;        call VD1Off;                    выключение "Единиц" и "Десятков"
;;        call VD2Off;
;;        call DLS;                       задержка на 1 секунду
;;        call VD1On;                     включение "единиц" и "десятков"
;;        call VD2On;
;;        BANK1;
;;
;;        btfss ModeNotSign;
;;        goto EnterDc0;
;;        bcf ModeNotSign;
;;        goto CycleMde;

; ====****  В В О Д   Д Е С Я Т К О В   И   Е Д И Н И Ц  ****====
EnterDc0
        bsf CurSignFlag;
        btfss SignFlag;
        bcf CurSignFlag;
;;        BANK0;
;;        call VD2Off;
;;        BANK1;

        bsf DecNotUnits;
        clrf ThresholdDec;
        clrf ThresholdUnits;
EnterDec
        clrf ButtonCounterLow;
        clrf ButtonCounterHigh;
        clrf CountHigh;
        clrf TMR0last;
        clrf TMR0;
        movlw b'11000111';
        OPTION;
        btfsc DecNotUnits;
        goto VD1On01;
        BANK0;
        call VD2On;                      включение "Десятков"
        BANK1;
        goto CycleDec;

VD1On01
        BANK0;
        call VD1On;                      включение "Единиц"
        BANK1;

CycleDec
        clrwdt;
        movf TMR0,w;                    заносим в аккумулятор содержимое TMR0
        bcf STATUS,Z;              обнуляем бит нулевого результата
        xorwf TMR0last,w;                  если TMR0last и TMR0 различаются, то
        btfsc STATUS,Z;          бит нулевого результата =0 -> цикл выполняется дальше
        goto CycleDec;            иначе переход на начало цикла выбора режима

        incf CountLow,f;                инкрементируем младший байт счетчика
        btfss CountLow,7;                  если 7 бит младшего счетчика =0, то
        goto CyclDcL1;            переходим дальше, иначе
        incf CountHigh,f;                  инкрементируем старший байт счетчика
        clrf CountLow;            обнуляем младший байт счетчика

CyclDcL1
        movf TMR0,w;                    запихиваем содержимое TMR0 в TMR0last
        movwf TMR0last;

        btfsc BUTTON;              если кнопка не нажата (=0), то
        goto CyclDcL3;            переход на очищение старшего счетчика,
        incf ButtonCounterLow,f;        иначе инкрементируем старший байт счетчика кнопки
        btfss ButtonCounterLow,7;          если 7 бит младшего байта счетчика =0,
        goto CyclDcL2;            то ничего не делаем (переход),
        incf ButtonCounterHigh,f;          инкрементируем старший байт счетчика
        clrf ButtonCounterLow;    и очищаем младший байт счетчика
CyclDcL2
        ;                                  _1_*128*256 = 32000 циклов = 0,03 сек
        ;                                  ************/////////////////////////d'32' = b'100000', сравниваем 5 бит
        btfss ButtonCounterHigh,0;        если =0, то
        goto CyDcVD1;              переход на инвертирование СИД,
        ;                                  иначе время нажатия кнопки больше 1 сек.
        clrf ButtonCounterHigh;  сброс счетчиков кнопки
        clrf CountHigh;          сброс счетчиков
        btfss DecNotUnits;
        goto CyclUnL1;
        incf ThresholdDec,f;            увеличиваем значение порога десятков
        movlw 0x05;
        btfss SignFlag;
        movlw 0x03;                     проверка десятков на превышение допустимого порога
        subwf ThresholdDec,w;           в 5 десятков для плюс. и 3 десятка для минус. температуры
        btfsc STATUS,C;                 если флаг C =1, то заема не было => десятки
        clrf ThresholdDec;              превышают предел, очищение ThresholdDec

        BANK0;
;;        call VD1Off;                    отключение "Единиц"
        call VD2On;                     включение "Десятков"
        BANK1;

;;        btfss SignFlag;
;;        bcf     VD2;                    включение зеленых десятков
;;        btfsc SignFlag;
;;        bsf     VD2;                    включение красных десятков

        BANK0;
        call DL250ms;
        call DL250ms;
        call VD2Off;                    выключение "десятков"
        call DL250ms;
        call DL250ms;
        BANK1;

;;      btfss   SignFlag;               цвет "Единиц" зависит от знака
;;      bcf     VD1;                    включение зеленых единиц
;;      btfsc   SignFlag;
;;      bsf     VD1;                    включение красных единиц

        goto CycleDec;
CyclUnL1
        incf ThresholdUnits,f;          увеличиваем значение порога единиц
        movlw 0x0A;
        subwf ThresholdUnits,w;         в 5 десятков для плюс. и 3 десятка для минус. температуры
        btfsc STATUS,C;                 если флаг C =1, то заема не было => единицы
        clrf ThresholdUnits;            превышают предел, очищение ThresholdUnits

        BANK0;
        call VD1On;                     включение "Единиц"
;;        call VD2Off;                    отключение "Десятков"
        BANK1;

;;        btfss   SignFlag;
;;        bcf     VD1;                    включение зеленых единиц
;;        btfsc   SignFlag;
;;        bsf     VD1;                    включение красных единиц

        BANK0;
        call DL250ms;
        call DL250ms;
        call VD1Off;                    выключение "Единиц"
        call DL250ms;
        call DL250ms;
	BANK1;

;;        btfss   SignFlag;
;;        bcf     VD2;                    включение зеленых десятков
;;        btfsc   SignFlag;
;;        bsf     VD2;                    включение красных десятков
        BANK0;
;;        call VD2On;
        BANK1;

        goto CycleDec;                  переход на начало цикла

CyclDcL3
        clrf ButtonCounterHigh;         очищение старшего счетчика кнопки
        btfss CountHigh,7;              A0 = 1010 0000 - примерно 5 сек.
        goto CyDcVD1;
        btfss CountHigh,5;
        goto CyDcVD1;
        movf ThresholdDec,w;
        movwf CurrentDec;
        movf ThresholdUnits,w;
        movwf CurrentUnits;

        BANK0;
        call VD1Off;
        call VD2Off;
        call DLS;

        bsf CurSignFlag;
        btfss SignFlag;
        bcf CurSignFlag;

;;        btfss DecNotUnits;              если программирование закончено,
;;        call ShowTmpr;                  то переход на показ температуры
        BANK1;

        goto CycleUnt;

CyDcVD1
        btfss CurrentColor;             процедура инвертирования текущего цвета
        goto CyDcVD1N;
        bcf CurrentColor;
;;
;;        btfss DecNotUnits;
;;        bcf     VD2;                    включение зеленых десятков
;;        btfsc DecNotUnits;
;;        bcf     VD1;                    включение зеленых единиц

        goto CycleDec;

CyDcVD1N
        bsf CurrentColor;
;;
;;        btfss DecNotUnits;
;;        bsf     VD2;                    включение красных десятков
;;        btfsc DecNotUnits;
;;        bsf     VD1;                    включение красных единиц

        goto CycleDec;

CycleUnt
        btfss DecNotUnits;
        goto PrgCyEnd;
        bcf DecNotUnits;
        goto EnterDec;
PrgCyEnd
        bsf DecNotUnits;
        clrf CountLow;
        clrf CountHigh;
        clrf TMR0;
        movlw b'11000111';
        OPTION;
        clrf TMR0last;
PrgCyEnd0
        BANK0;
;;;;!!!!! VD2On на VD2Off
        call VD1Off;
        call VD2Off;
        BANK1;
;;
;;        btfss CurrentColor;
;;        bcf     VD1; включение зеленых единиц
;;        btfsc CurrentColor;
;;        bsf     VD1;включение красных единиц
;;        btfss CurrentColor;
;;        bcf     VD2;включение зеленых десятков
;;        btfsc CurrentColor;
;;        bsf     VD2;включение красных десятков
;;
;;        btfsc CurrentColor;
;;        goto $+3;
;;        bsf CurrentColor;
;;        goto $+2;
;;        bcf CurrentColor;
;;
;;        clrwdt;                  очистка таймера
;;        movf TMR0,w;                    заносим в аккумулятор содержимое TMR0
;;        xorwf TMR0last,w;                  если TMR0last и TMR0 различаются, то
;;        btfsc STATUS,Z;          бит нулевого результата =0 -> цикл выполняется дальше
;;        goto $-4;                          иначе переход на начало цикла выбора режима
;;
;;        incf TMR0last,f;                инкремент TMR0last
;;        incf CountLow,f;                инкремент младшего счетчика
;;        btfsc CountLow,7;                  если 7 бит младшего счетчика =1, то
;;        goto $+2;                          очистка младшего счетчика и инкремент
;;        goto $+3;                          старшего счетчика, иначе ничего
;;        incf CountHigh,f;
;;        clrf CountLow;
;;
;;        btfsc CountHigh,6;                если 6 и 5 биты старшего счетчика =0
;;        goto $+4;                          (примерно 3 сек), то переход ниже,
;;        btfsc CountHigh,5;
;;        goto $+2;
;;        goto PrgCyEnd0;          иначе переход в конец (выше)
;;
;;        BANK0;
;;        call VD1Off;                    "х1" и "х10" выключены, задержка, переход
;;        call VD2Off;                    к процедуре записи в EEPROM
;;        call DLS;
;;        BANK1;
;;!!
	bsf SignFlag;
	bsf ModeFlag;
	bcf	ShortButtonFlag;
;;!!
        goto EEPROMW;

        END
