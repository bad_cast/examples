﻿;	Индикатор напряженности поля
;	(c) 2005, ib

	processor	16f676
	#include 	"p16f676.inc"
	__CONFIG (_CP_OFF & _CPD_OFF & _BODEN & _MCLRE_OFF & _PWRTE_ON & _WDT_OFF & _INTRC_OSC_NOCLKOUT)

	#define		setbank0	bcf	STATUS,RP0;
	#define		setbank1	bsf	STATUS,RP0;

;	D E F I N E
	#define		IND4		PORTC,0;	
	#define		IND3		PORTA,2;	
	#define		IND2		PORTA,1;	
	#define		IND1		PORTA,0;
	
; 	V A R
	cblock		0x20;
	dl_counter;
	endc;

;	C O N S T
	#define		THRESH_LOW		d'160';
	#define		THRESH_MID		d'170';
	#define		THRESH_HIGH		d'180';
	#define		THRESH_VERY_HIGH	d'190';
	

;	P R O G R A M M E
	org		0x00;
	goto 		INIT;
	org		0x04;


INIT
	movlw		b'00000111';			конфигурационный регистр компаратора
	movwf		CMCON;				переводим в цифровой режим

; установка АЦП
	setbank0;
	movlw		b'00011100';	модуль АЦП включен, выбор первого канала,
	movwf		ADCON0;		левое выравнивание, напряжение питания - опорное
	setbank1;
;	*****	установка регистра ADCON1 - выбор такта АЦП	*****
	bsf		ADCON1,ADCS0;
	bcf		ADCON1,ADCS1;
	bsf		ADCON1,ADCS2;

	
	movlw		b'10001011';				предделитель перед WDT = 1x8; прерывание по заднему фронту
	movwf		OPTION_REG;		
	movlw		b'00000000';				установка TRIS порта A на выход
	movwf		TRISA;				
	movlw		b'00001000';				установка TRIS порта C
	movwf		TRISC;
	movlw		b'10000000';				установка AN7(PORTC3) на АЦП
	movwf		ANSEL;
	setbank0;
	bsf		ADCON0,ADON;

;	----	главная функция	----
START
	movlw		.20;
	movwf		dl_counter;
	call		DelayUs;
	call		ADC;
	goto		START;

;-------------------------------------- работа с АЦП
ADC
	btfsc		ADCON0,GO_DONE;
	return;

ADC_CHECK1
	movlw		THRESH_LOW;
	subwf		ADRESH,w;		w = ADRESH - THRESH_LOW
	btfsc		STATUS,C;
	goto		ADC_CHECK2;
ADC_VERY_LOW
	bcf		IND1;	
	bcf		IND2;	
	bcf		IND3;	
	bcf		IND4;	
	goto		ADC_END;

ADC_CHECK2
	movlw		THRESH_MID;
	subwf		ADRESH,w;
	btfsc		STATUS,C;
	goto		ADC_CHECK3;
ADC_LOW
	bsf		IND1;	
	bcf		IND2;	
	bcf		IND3;	
	bcf		IND4;	
	goto		ADC_END;

ADC_CHECK3
	movlw		THRESH_HIGH;
	subwf		ADRESH,w;
	btfsc		STATUS,C;
	goto		ADC_CHECK4;
ADC_MID
	bsf		IND1;	
	bsf		IND2;	
	bcf		IND3;	
	bcf		IND4;	
	goto		ADC_END;

ADC_CHECK4
	movlw		THRESH_VERY_HIGH;
	subwf		ADRESH,w;
	btfsc		STATUS,C;
	goto		ADC_VERY_HIGH;
ADC_HIGH
	bsf		IND1;	
	bsf		IND2;	
	bsf		IND3;	
	bcf		IND4;	
	goto		ADC_END;

ADC_VERY_HIGH
	bsf		IND4;
	bsf		IND3;
	bsf		IND2;
	bsf		IND1;
ADC_END
	bsf		ADCON0,GO_DONE;			начало конвертирования
	return;

;-------------------------------------- задержка
DelayUs
	goto		$+1;
	decfsz		dl_counter,f;
	goto		$-2;
	return;

		
	END
