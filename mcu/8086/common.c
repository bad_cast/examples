#include "common.h"

u8 Bin2AsciiHi(u8 ch)
{
  ch = (ch & 0xF0) >> 4;
  if(ch < 10)
    ch += '0';
  else
    ch += 'A' - 10;
  return ch;
}

u8 Bin2AsciiLo(u8 ch)
{
  ch = ch & 0x0F;
  if(ch < 10)
    ch += '0';
  else
    ch += 'A' - 10;
  return ch;
}

void SwapData16(u16* data, u16 len)
{
  int i = 0;
  for(i=0; i<len; i++)
    data[i] = (data[i] << 8) + (data[i] >> 8);
}


