﻿#include "settings.h"

struct tSaveData save_data;

void InitData(void)
{
  aio.data.mode = MODE_STOP;
  aio.data.state = STATE_OK;
  aio.data.wash_temper_cur = 0;
  aio.data.imp_prep_after = 0;
  aio.data.cleans_cnt = 0;
  aio.data.cleans_delay = 0;
  aio.data.input_timeout = 0;
  aio.data.breakdown_type = 0;
  aio.data.discharge_cnt = 0;
  aio.data.rins_temper_cur = 0;
  aio.data.blow_temper_cur = 0;
  aio.data.wash_ph_cnt = 0;
  aio.data.water_refresh_cnt = 0;
  aio.data.water_open_imp = 0;
  aio.data.prep_step = 0;
  
  dio.data.wash_ph_ctrl_run = 0;
  dio.data.cleans_run = 0;
  dio.data.jx_run = 0;
  dio.data.blow_trm_run = 0;
  dio.data.jx_is_on = 0;
  dio.data.wash_trm_is_on = 0;
  dio.data.mute_sound = 0;
  dio.data.is_breakdown = 0;
}


//--------------------
//  загрузка установок
//--------------------
void LoadSettings(void)
{

  u8 *pdio = (u8*)&dio;
  u8 *paio = (u8*)&aio;
  int i;

  __asm cli

  for(i=DATABLOCK11; i<DATABLOCK12; i+=8)
  {
    EE_MultiRead(D_BLOCK, i, 8, &pdio[i]);
    EE_MultiRead(A_BLOCK, i, 8, &paio[i]);
  }
  for(i=DATABLOCK21; i<DATABLOCK22; i+=8)
  {
    EE_MultiRead(D_BLOCK, i-100, 8, &pdio[i]);
    EE_MultiRead(A_BLOCK, i-100, 8, &paio[i]);
  }

  if(!memcmp(aio.stats.version_label, VERSION_LABEL, sizeof(aio.stats.version_label)))
    if(aio.stats.version_major <= VERSION_MAJOR)
    {
      if(aio.stats.version_minor != VERSION_MINOR || aio.stats.version_major != VERSION_MAJOR)
        SaveSettings();
      return;
    }

  __asm sti

  ResetSettings();
  return;
}

//--------------------
//  сохранение установок
//--------------------
void SaveSettings(void)
{
  int i;
  u8 *pdio = (u8*)&dio;
  u8 *paio = (u8*)&aio;

  __asm cli

  EE_WriteEnable();
//  DbgOut("SaveSettings\n");

  for(i=DATABLOCK11; i<DATABLOCK12; i+=8)
  {
    EE_MultiWrite(D_BLOCK, i, 8, &pdio[i]);
    EE_MultiWrite(A_BLOCK, i, 8, &paio[i]);
  }
  for(i=DATABLOCK21; i<DATABLOCK22; i+=8)
  {
    EE_MultiWrite(D_BLOCK, i-100, 8, &pdio[i]);
    EE_MultiWrite(A_BLOCK, i-100, 8, &paio[i]);
  }

  EE_WriteProtect();

  __asm sti
}

//--------------------
//  сброс установок
//--------------------
void ResetSettings(void)
{
  memset(&dio.conf, 0, sizeof(dio.conf));
  dio.conf.ext_in_enable = 0;
  dio.conf.ext_out_enable = 0;
  dio.conf.wash_enable = 1;
  dio.conf.wash_temper_enable = 1;
  dio.conf.rins_enable = 1;
  dio.conf.rins_temper_enable = 0;
  dio.conf.blow_enable = 1;
  dio.conf.blow_temper_enable = 0;

  memset(&aio.conf, 0, sizeof(aio.conf));
  aio.conf.belt_roller_diameter = 64;
  aio.conf.belt_nominal_rpm = 1320;
  aio.conf.belt_gear_ratio = 27;
  aio.conf.wash_ph_method = 2;
  aio.conf.rins_ph_method = 0;

  memset(&dio.sett, 0, sizeof(dio.conf));
  
  memset(&aio.sett, 0, sizeof(aio.sett));
  aio.sett.belt_speed = 80;
  aio.sett.water_refresh_period = 60;
  aio.sett.water_refresh_volume = 5;

  aio.sett.wash_ph_set_sens = 7.0;
  aio.sett.cleans_dose_sens = 10;
  aio.sett.wash_ph_period_sens = 10;
  aio.sett.cleans_dose_time = 10;
  aio.sett.wash_ph_period_time = 10;

  aio.sett.wash_temper_set = 300;
  aio.sett.wash_water_add_volume = 10;
  aio.sett.rins_temper_set = 25;
  aio.sett.blow_temper_set = 30;
  
  memset(&dio.calib, 0, sizeof(dio.calib));
  dio.calib.cleans_on = 1;
  dio.calib.cleans_finished = 1;
 
  memset(&aio.calib, 0, sizeof(aio.calib));
  aio.calib.cleans_time_set = 60;
  aio.calib.cleans_volume_calc = 20;
  aio.calib.cleans_quotient_cur = 10;
  aio.calib.cleans_quotient_calc = 0;
  aio.calib.water_quotient_cur = 40*100; // 40 импульсов на литр
  aio.calib.water_quotient_calc = 0;

  memset(&dio.stats, 0, sizeof(dio.stats));
  
  memset(&aio.stats, 0, sizeof(aio.stats));
  memcpy(aio.stats.version_label, VERSION_LABEL, 10);
  aio.stats.version_major = VERSION_MAJOR;
  aio.stats.version_minor = VERSION_MINOR;

  SaveSettings();
}

//  сохранение отдельного блока
void SaveBlock(u8 block_id, u16 addr, u16 len)
{
  int i;
  u16 offset;
  u8* pdata;
  u16 end_addr = addr + len;
  if(/*(addr >= DATABLOCK11) && */(addr < DATABLOCK12))
    offset = 0;
  else if((addr >= DATABLOCK21) && (addr < DATABLOCK22))
    offset = 100;
  else
    return;
  DbgOut("SaveBlock: id=%d, addr=%d, len=%d, offset=%d\n", block_id, addr, len, offset);
  if(block_id == D_BLOCK)
    pdata = (u8*)&dio;
  else
    pdata = (u8*)&aio;
  
  __asm cli
  EE_WriteEnable();
  for(i=addr; i<end_addr; i+=8)
  {
    if(end_addr-i >= 8)
 {
      EE_MultiWrite(block_id, i+offset, 8, &pdata[i]);  // блок, адрес, кол-во, данные
 DbgOut("id=%d, i=%d, offset=%d\n", block_id, i, offset);

}

    else
{      EE_MultiWrite(block_id, i+offset, end_addr-i, &pdata[i]);
 DbgOut("id=%d, i=%d, offset=%d, end_addr=%d\n", block_id, i, offset, end_addr);
}
  }
  EE_WriteProtect();

//вытереть
  {
  u8 tmp_buf[16];
  EE_MultiRead(0, 0, 16, &tmp_buf[0]);
  for(i=0;i<16;i++)
    DbgOut("%d ", tmp_buf[i]);
  }

  __asm sti
}

//  сохранение статистики в память
void SaveStats(void)
{
  int i;
  //  сохраняем общий расход
  u8* p = (u8*)&aio.stats.water_expense_litres_total;
  
//  __asm cli

  for(i=0; i<4; i++)
    WriteRTC(32+i, p[i]);
  p = (u8*)&aio.stats.cleans_expense;
  for(i=0; i<2; i++)
    WriteRTC(36+i, p[i]);

//  __asm sti
}

//  чтение статистики из памяти
//  32..35 - water_expense_litres_prev = water_expense_litres_total
//  36..37 - cleans_expense
void LoadStats(void)
{
  int i;
  //  расход воды
  u8* p = (u8*)&aio.stats.water_expense_litres_prev;
  aio.stats.water_expense_litres_cur = 0; 
  aio.stats.water_expense_litres_total = 0; 
  aio.stats.water_expense_imp = 0;
  for(i=0; i<4; i++)
    ReadRTC(32+i, &p[i]);

  // расход моющего средства
  p = (u8*)&aio.stats.cleans_expense;
  for(i=0; i<2; i++)
    ReadRTC(36+i, &p[i]);
}

//  сброс статистики
void ResetStats(void)
{
  int i;

  aio.stats.water_expense_imp = 0;
  aio.stats.water_expense_litres_total = 0;
  aio.stats.water_expense_litres_cur = 0;
  aio.stats.water_expense_litres_prev = 0;
  aio.stats.cleans_expense = 0;

//  __asm cli

  for(i=0; i<6; i++)
    WriteRTC(32+i, 0);

//  __asm sti
  
}

//  сохранение блока данных отложенное
void SaveDataDelayed(u8 block_id, u16 addr, u16 len)
{
  //  проверяем, входят ли данные в область сохраняемых
  if((/*(addr >= DATABLOCK11) && */(addr < DATABLOCK12)) ||   // ругаетца варнингом
     ((addr >= DATABLOCK21) && (addr < DATABLOCK22)))
  {
//    DbgOut("SaveDataDelayed\n");
    save_data.block_id = block_id;
    save_data.addr = addr;
    save_data.len = len;
    save_data.save_flag = 1;
  }
}

//  сохранение блока данных немедленное
void SaveDataInstant(void)
{
  if(save_data.save_flag)
  {
//    DbgOut("SaveDataInstant\n");
    SaveBlock(save_data.block_id, save_data.addr, save_data.len);
    save_data.save_flag = 0;
  }
}

//  функция проверки, есть ли данные для сохранения
u8 IsDataToSave(void)
{
  return save_data.save_flag;
}
