﻿#include "timers.h"

//#define DBG_TIMERS

u32 cnt_system = 0;

//  матрица переходов между режимами
//  для разрешения и запрещения 
//  0 - INIT
//  1 - PREP
//  2 - PAUSE
//  3 - WORK
//  4 - DRAIN
//  5 - STOP
//  6 - MANUAL
//  7 - SERVICE                 0 1 2 3 4 5 6 7
const u8 modes_trans[8][8] = { {1,0,0,0,0,1,0,0},  //  0
                               {0,1,0,0,0,1,0,0},  //  1
                               {0,0,1,1,0,1,0,0},  //  2
                               {0,0,1,1,0,1,0,0},  //  3
                               {0,0,0,0,1,1,0,0},  //  4
                               {0,1,0,0,1,1,1,1},  //  5
                               {0,0,0,0,0,1,1,0},  //  6
                               {0,0,0,0,0,1,0,1}}; //  7

/*
 * ФУНКЦИИ ДЛЯ КОНТРОЛЯ РЕЖИМОВ
 */
//	таймер для приготовления воды
void TimerModePreparation(void)
{
  if(aio.data.mode != MODE_PREPARE)
  {
    aio.data.prep_step = 0;
    return;
  }
   
 
  //  включаем воду
  if(aio.data.prep_step == 0)
  {
//    DbgOut("Step 0\n");
    //  выключаем все выходы
    InitDO();
    dio.data.wash_trm_run = 0;
    dio.data.jx_run = 0;
    WaterFeedOn();
    aio.data.prep_step++;
  }
  //  ждем пока не сработают уровни ополаскивателя
  if(aio.data.prep_step == 1)
  {
    if(dio.conf.rins_enable)
      if(!GetRinsMin() || !GetRinsMax())
        return;
//    DbgOut("Step 1\n");
    aio.data.prep_step++;
  }
  //  ждем, пока не сработют уровни мойки  
  if(aio.data.prep_step == 2)
  {
    if(dio.conf.wash_enable)
    {
      if(!GetWashMin() || !GetWashMax())
        return;
      aio.data.imp_prep_after = aio.sett.wash_water_add_volume * aio.calib.water_quotient_cur / 100;
      if(dio.conf.wash_temper_enable)
        dio.data.wash_trm_run = 1;
      WashPumpOn();
    }
//    DbgOut("Step 2\n");
    aio.data.prep_step++;
  }
  //  ждем пока дольется
  if(aio.data.prep_step == 3)
  {
    if(aio.data.imp_prep_after)
      return;
    if(aio.conf.wash_ph_method)
      dio.data.wash_ph_ctrl_run = 1;
    WaterFeedOff();
//    DbgOut("Step 3\n");
    aio.data.prep_step++;
  }
  //  ждем пока не нагреется вода
  if(aio.data.prep_step == 4)
  {
    if(dio.conf.wash_temper_enable)
      if((aio.data.wash_temper_cur / 10 ) < aio.sett.wash_temper_set-2)
        return;
        
//    DbgOut("Step 4\n");
    aio.data.prep_step++;
  }
  //  ждем pH и переходим в паузу
  if(aio.data.prep_step == 5)
  {
    if(aio.conf.wash_ph_method == 2)
    {
      if(aio.sett.wash_ph_set_sens > 7.0)
      {
        if(aio.data.wash_ph_cur < aio.sett.wash_ph_set_sens - 0.5)
          return;
      }
      else
      {
        if(aio.data.wash_ph_cur > aio.sett.wash_ph_set_sens + 0.5 )
          return;
      }
    }
    //  цикл подготовки закончился, водичку выключаем, пампу выключаем, переходим в паузу
    WashPumpOff();
    dio.data.wash_ph_ctrl_run = 0;
//    aio.data.mode = MODE_PAUSE;
    aio.data.tmp_watch = 1;
    ChangeMode(MODE_PAUSE);
//    DbgOut("Step 5\n");
    aio.data.prep_step++;
  }
}

//  --------------------
//  слив воды
//  --------------------
void TimerModeDrain(void)
{
  static u8 step = 0;
  if(aio.data.mode != MODE_DRAIN)
  {
    step = 0;
    return;
  }

  if(!dio.conf.autodrain_enable)
  {
    aio.data.tmp_watch = 2;
    ChangeMode(MODE_STOP);
    return;
  }

  //  выключаем все выходы
  if(step == 0)
  {
    InitDO();
    dio.data.wash_trm_run = 0;
    dio.data.jx_run = 0;

    WashDrainOn();
    RinsDrainOn();
    
    step++;
  }
  if(step == 1)
  {
    if(GetWashMin() || GetRinsMin())
      return;
    aio.data.discharge_cnt = 30;
    step++;
  }
  if(step == 2)
  {
    if(aio.data.discharge_cnt)
      return;
    aio.data.tmp_watch = 3;
    ChangeMode(MODE_STOP);
    step++;
  }
}

//  --------------------
//  работа
//  --------------------
void TimerModeWork()
{
  static u8 step = 0;
  if(aio.data.mode != MODE_WORK)
  {
    step = 0;
    return;
  }

  if(step == 0)
  {
    if(dio.conf.wash_enable)
    {
      WashPumpOn();
      if(dio.conf.wash_temper_enable)
        dio.data.wash_trm_run = 1;
      if(aio.conf.wash_ph_method)
        dio.data.wash_ph_ctrl_run = 1;
    }
    dio.data.jx_run = 1;
    dio.conf.rins_enable ? RinsPumpOn() : RinsPumpOff();
    dio.conf.blow_enable ? BlowOn() : BlowOff();
    step++;
  }
}

//  стоп
void TimerModeStop()
{
  static u8 step = 0;
  if(aio.data.mode != MODE_STOP)
  {
    step = 0;
    return;
  }


  if(step == 0)
  { //  выключаем все
    InitDO();
    dio.data.wash_trm_run = 0;
    dio.data.jx_run = 0;
    dio.data.wash_ph_ctrl_run = 0;
    step++;
  }
}

//  режим ПАУЗА
void TimerModePause(void)
{
  static u8 step = 0;
  if(aio.data.mode != MODE_PAUSE)
  {
    step = 0;
    return;
  }

//  if(!GetWashMax())
//    WaterFeedOn();
//  else 
//    WaterFeedOff();
  
  if(step == 0)
  {
    if(dio.conf.wash_enable)
    {
      if(dio.conf.wash_temper_enable)
        dio.data.wash_trm_run = 1;
    }
    dio.data.wash_ph_ctrl_run = 0;
    dio.data.jx_run = 0;
    WashPumpOff();
    RinsPumpOff();
    BlowOff();
    step++;
  }
}

//  ручное управление
void TimerModeManual()
{
//  
}

//  сервис
void TimerModeService()
{

}


//  таймер обработки ошибок/аварий
void TimerManageStates(void)
{
//  u16 tmp_cnt = 0;
  if(!dio.data.d_i.extra_stop)  //  кнопка инверсная
    aio.data.breakdown_type |= BD_EXTRA_STOP;
  else
    aio.data.breakdown_type &= ~BD_EXTRA_STOP;

  if(dio.data.d_i.fold1)
    aio.data.breakdown_type |= BD_FOLD1;
  else
    aio.data.breakdown_type &= ~BD_FOLD1;

  if(dio.data.d_i.fold2)
    aio.data.breakdown_type |= BD_FOLD2;
  else
    aio.data.breakdown_type &= ~BD_FOLD2;

  if(dio.data.d_i.wash_pump_alarm)
    aio.data.breakdown_type |= BD_WASH_ALARM;
  else
    aio.data.breakdown_type &= ~BD_WASH_ALARM;

  if(dio.data.d_i.rins_pump_alarm)
    aio.data.breakdown_type |= BD_RINS_ALARM;
  else
    aio.data.breakdown_type &= ~BD_RINS_ALARM;

  if(dio.data.d_i.blow_alarm)
    aio.data.breakdown_type |= BD_BLOW_ALARM;
  else
    aio.data.breakdown_type &= ~BD_BLOW_ALARM;


  if(!dio.conf.wash_temper_enable)
    aio.data.breakdown_type &= ~BD_TRM_TIMEOUT;
  if(aio.conf.wash_ph_method != 2)
    aio.data.breakdown_type &= ~BD_PH_TIMEOUT;
  if(aio.data.breakdown_type)
  {
    if(aio.data.breakdown_type - BD_JX_TIMEOUT == 0)
      return;
    if(aio.data.mode != MODE_SERVICE && aio.data.mode != MODE_STOP)
      ChangeMode(MODE_STOP);
    dio.data.is_breakdown = 1;
    aio.data.state = STATE_BREAKDOWN;
  }
  else
  {
    aio.data.state = STATE_OK;
    dio.data.is_breakdown = 0;
  }             
}


//  таймер, занимающийся обработкой уровня PH
void TimerWashPH(void)
{
  if(!dio.data.wash_ph_ctrl_run || !aio.conf.wash_ph_method)
    return;
  
  if(!aio.data.wash_ph_cnt)
  {  // по датчику
    if(aio.conf.wash_ph_method == 2)
    {
      if(((aio.sett.wash_ph_set_sens <= 7.0) && (aio.data.wash_ph_cur > aio.sett.wash_ph_set_sens + 0.5)) ||
         ((aio.sett.wash_ph_set_sens > 7.0 && aio.data.wash_ph_cur < aio.sett.wash_ph_set_sens - 0.5)))
      {
        dio.data.cleans_run = TRUE;
        CleansPumpOn();
        aio.data.cleans_cnt = aio.sett.cleans_dose_sens * 100 / aio.calib.cleans_quotient_cur;
      }
      else
        dio.data.cleans_run = FALSE;
      aio.data.wash_ph_cnt = aio.sett.wash_ph_period_sens;
    }
    if(aio.conf.wash_ph_method == 1)
    { // циклический
      aio.data.cleans_cnt = aio.sett.cleans_dose_time * 100 / aio.calib.cleans_quotient_cur;
      aio.data.wash_ph_cnt = aio.sett.wash_ph_period_time;
      CleansPumpOn();
      dio.data.cleans_run = TRUE;
    }
  }

    //  работа дозатора моющего средства
  if(dio.data.cleans_run && !aio.data.cleans_cnt)
  {
    CleansPumpOff();
    dio.data.cleans_run = 0;
  }
}

//  таймер по
void TimerWaterRefresh(void)
{
  static u8 step = 0;

  //  работаем только в режиме РАБОТА
  if(aio.data.mode != MODE_WORK)
  {
    step = 0;
    return;
  }

  //  если вода не достает до верхнего уровня мойки - доливаем как в подготовке
  if((!GetWashMax() || !GetRinsMax()) && (step < 3))
    step = 3;

  //  инициируем счетчик
  if(step == 0)
  {
    aio.data.water_refresh_cnt = aio.sett.water_refresh_period * 60;
    step++;
  }
  //  ждем, пока счетчик не обнулится
  if(step == 1)
  {
    if(aio.data.water_refresh_cnt)
     return;
    //  обнулился. открываем кран и инициируем счетчик подсчета импульсов
    aio.data.water_open_imp = aio.sett.water_refresh_volume * aio.calib.water_quotient_cur / 100;
    WaterFeedOn();
    step++;
  }

  if(step == 2)
  {
    if(aio.data.water_open_imp)
      return;
    WaterFeedOff();
    step = 0;
  }
 
  //  доливаем как в подготовке
  if(step == 3)
  {
    aio.data.imp_prep_after = aio.sett.wash_water_add_volume * aio.calib.water_quotient_cur / 100;
    WaterFeedOn();
	step++;
  }
  if(step == 4)
  {
    if(aio.data.imp_prep_after)
      return;
    WaterFeedOff();
    step = 0;
  }
}

//  обработка нажатий кнопок ПУСК и СТОП
//  TODO: укоротить по аналогии с ManageModes
//  ну его нахуй. работает - не трогай
void TimerManageButtons(void)
{
  //  пока авария - ничего не делаем
  if(dio.data.is_breakdown)
  {
    ResetStartKeyStatus();
    ResetStopKeyStatus();
    return;
  }

  ManageStartKeyStatus();
  ManageStopKeyStatus();


  switch(aio.data.mode)
  {
    case MODE_INIT:
      ChangeMode(MODE_STOP);
      break;

    case MODE_PREPARE:
      if(GetStopKeyStatus() != KEY_STATUS_NOT)
        ChangeMode(MODE_STOP);
      break;

    case MODE_PAUSE:
      if(GetStartKeyStatus() != KEY_STATUS_NOT)
        ChangeMode(MODE_WORK);                  
      if(GetStopKeyStatus() != KEY_STATUS_NOT)
        ChangeMode(MODE_STOP);                 
      break;
      
    case MODE_WORK:
      if(GetStopKeyStatus() == KEY_STATUS_SHORT)
		ChangeMode(MODE_PAUSE);
      if(GetStopKeyStatus() == KEY_STATUS_LONG)
		ChangeMode(MODE_STOP);
      break;

    case MODE_DRAIN:
      if(GetStopKeyStatus() == KEY_STATUS_SHORT)
        ChangeMode(MODE_STOP);   
      break;

    case MODE_STOP:
      if(GetStartKeyStatus() == KEY_STATUS_LONG)
        ChangeMode(MODE_PREPARE);
      if(GetStopKeyStatus() == KEY_STATUS_LONG)
		ChangeMode(MODE_DRAIN);
      break;

    case MODE_MANUAL:
      if(GetStopKeyStatus() != KEY_STATUS_NOT)
        ChangeMode(MODE_STOP);
      break;
  }
  ResetStartKeyStatus();
  ResetStopKeyStatus();
}

//  управление режимами
void TimerManageMode(void)
{
  int i;
  u8 *p_permit = (u8*)&dio.data.mode_prep_permit;
  u8 *p_pressed = (u8*)&dio.data.mode_prep_pressed;
  u8 mode_pressed = 0;

  //  вычисляем, в какой режим пришла команда перехода
  for(i=1; i<MODES_NUMBER; i++)
    if(p_pressed[i-1] && (aio.data.mode != i))
    { 
      mode_pressed = i;
      break;
    }
 
  //  авария
  if(dio.data.is_breakdown && mode_pressed != MODE_SERVICE-1)
  {
    memset(&p_pressed[0], 0, MODES_NUMBER-1);
    p_pressed[MODE_STOP-1] = 1;
  }

  //  проверяем, нажата ли вообще какая-нибудь кнопа
  //  если не нажата - нажимаем
  if(i == MODES_NUMBER)
  {
    mode_pressed = aio.data.mode;
    p_pressed[mode_pressed-1] = 1;
  }

  //  теперь анализируем, можно ли перейти в этот режим
  if(!modes_trans[aio.data.mode][mode_pressed])
    return;

  //  переводим режим
  aio.data.mode = mode_pressed;

  //  ставим разрешения на кнопки
  for(i=1; i<MODES_NUMBER; i++)
    p_permit[i-1] = modes_trans[aio.data.mode][i];

  //  очищаем все нажатые кнопки, кроме последней нажатой
  memset(&p_pressed[0], 0, MODES_NUMBER-1);
  p_pressed[mode_pressed-1] = 1;
}

//	таймер счетчика воды
void TimerReadWaterExpense(void)
{
  static u8 cur_state, prev_state;
  cur_state = GetWaterExpense();
  if(cur_state != prev_state)
  {
    if(!cur_state)
    {
      if(aio.stats.water_expense_imp < 0xFFFFFFFFL)
        aio.stats.water_expense_imp++;
      if(aio.calib.water_imp_cur)
        aio.calib.water_imp_cur--;
      if(aio.data.water_open_imp)
        aio.data.water_open_imp--;
      if(aio.data.imp_prep_after)
        aio.data.imp_prep_after--;
    }
    prev_state = cur_state;
  }
}


//  таймер вычисляемых величин
void TimerRecalculate(void)
{
  //  пересчет импульсов счетчика воды в литры
  if(aio.calib.water_quotient_cur)
    aio.stats.water_expense_litres_cur = aio.stats.water_expense_imp * 100 / aio.calib.water_quotient_cur;
  else
    aio.stats.water_expense_litres_cur = 0;
  aio.stats.water_expense_litres_total = aio.stats.water_expense_litres_cur + aio.stats.water_expense_litres_prev;

  //  пересчет скорости движения конвейера в частоту для MX2
  if(aio.conf.belt_speed_cur && aio.sett.belt_speed)
  {
    u32 tmp = (u32) aio.sett.belt_speed * 500 / (u32)aio.conf.belt_speed_cur;
    aio.conf.belt_freq_calc = (u16)tmp;
    if(aio.conf.belt_freq_calc > 500)
      aio.conf.belt_freq_calc = 500;
  }

  //  
}

//	таймер для чтения входов
void TimerUpdateInputs(void)
{
  int i;
  u8  di[16];
  u16 d = GetDI();
  for(i=0; i<16; i++)
    di[i]= (d >> i) & 1;
  memcpy(&dio.data.d_i, &di[0], sizeof(dio.data.d_i));
//  DbgOut("DIN=%X, cleans_min=%X, di[10]=%X\n", d, dio.data.d_i.cleans_min, di[10]);

}

// таймер для чтения данных от устройств
void TimerManageModbusMaster(void)
{
  static u16 step = 0;
  static u8 trm_run = 0;
  static u16 trm_temper = 0;
  static u8 jx_run = 0;
  static u16 jx_freq = 0;

  TimerMBMasterRx();

  switch(step)
  {
    case 0:
    if(dio.conf.wash_temper_enable)
    {
      if(!MBMasterIsTxReady())
        return;
      MBMasterSendGetTemperMeas();
//      DbgOut("1. TRM TEMPER MEAS\n");
//      DbgOut("System: %d\n", cnt_system);
    }
    break;
     
    case 10:
    if(dio.conf.wash_temper_enable)
    {
      if(trm_temper != aio.sett.wash_temper_set)
      {
        if(!MBMasterIsTxReady())
          return;
        MBMasterSendSetTemper(aio.sett.wash_temper_set);
        trm_temper = aio.sett.wash_temper_set;
//        DbgOut("2. TRM TEMPER SET\n");
//        DbgOut("System: %d\n", cnt_system);
      }
    }
    break;
  
    case 20:
    if(dio.conf.wash_temper_enable)
    {
      if(trm_run != dio.data.wash_trm_run)
      {
        if(!MBMasterIsTxReady())
          return;
        MBMasterSendRunTRM(dio.data.wash_trm_run == 0 ? MB_CMD_TRM_OFF : MB_CMD_TRM_ON);
        trm_run = dio.data.wash_trm_run;
//        DbgOut("3. TRM RUN\n");
//        DbgOut("System: %d\n", cnt_system);
      }
    }
    break;

    case 30:
    if(jx_freq != aio.conf.belt_freq_calc)
    {
      if(!MBMasterIsTxReady())
        return;
      MBMasterSendSetFreq(aio.conf.belt_freq_calc);
      jx_freq = aio.conf.belt_freq_calc;
    }
//    DbgOut("4. MX2 SET FREQ\n");
//    DbgOut("System: %d\n", cnt_system);
    break;
    
    case 40:
    if(jx_run != dio.data.jx_run)
    {
      if(!MBMasterIsTxReady())
        return;
      MBMasterSendRunJX(dio.data.jx_run == 0 ? MB_CMD_JX_OFF : MB_CMD_JX_ON);
      jx_run = dio.data.jx_run;
    }
//    DbgOut("5. MX2 RUN\n");
//    DbgOut("System: %d\n", cnt_system);
    break;

    case 50:
/*    if(jx_run != dio.data.jx_run)
    {
      if(!MBMasterIsTxReady())
        return;
      MBMasterSendGetJX(dio.data.jx_run == 0 ? MB_CMD_JX_OFF : MB_CMD_JX_ON);
      jx_run = dio.data.jx_run;
    }
//    DbgOut("5. MX2 RUN\n");
//    DbgOut("System: %d\n", cnt_system);*/
    break;
    
    case 60:
    if(aio.conf.wash_ph_method == 2)
    {
      if(!MBMasterIsTxReady())
        return;
      MBMasterSendGetPH();
    }
    break;

    default:
    break;
  }
//  DbgOut("::::  %d  ::::\n", step);
  step++;
  if(step > 70)
    step = 0;
}

//  таймер для обновления или стирания статистики
void TimerCheckResetStats(void)
{
  if(dio.stats.reset_stats)
  {
    dio.stats.reset_stats = 0;
    ResetStats();
    return;
  }
}


//  таймер для калибровки ПАВ, 1 с
void TimerCalibCleans(void)
{
  static u8 step = 0;
  
  if(aio.data.mode != MODE_SERVICE || !dio.calib.cleans_calib_on)
  {
    step = 0;
    return;
  }
  //  инициализация калибровки
  if(step == 0)
  {
    dio.calib.cleans_on = 0;
    dio.calib.cleans_quotient_save = 0;
    dio.calib.cleans_finished = 0;
    step++;
//    DbgOut("Step0\n");
  }
  //  начинаем дозировать
  if(step == 1)
  {
    if(!dio.calib.cleans_on || !aio.calib.cleans_time_set)
      return;
   
    dio.calib.cleans_finished = 0;
    aio.calib.cleans_quotient_calc = 0;

    CleansPumpOn();
    aio.calib.cleans_time_cur = aio.calib.cleans_time_set;
//    DbgOut("Step1\n");
    step++;
  }
  //  выключаем дозирование
  if(step == 2)
  {
    if(!dio.calib.cleans_on)
      step = 0;
    if(aio.calib.cleans_time_cur)
      return;
    dio.calib.cleans_on = 0;
    CleansPumpOff();
   
    step++;
//    DbgOut("Step2\n");
  }
  //  считаем коэффициент или, если нажата кнопка, опять начинаем дозирование
  if(step == 3)
  {
    u32 tmp;
    if(dio.calib.cleans_on)
    {
      step = 1;
//      DbgOut("Rev\n");
      TimerCalibCleans();
      return;
    }
 
   if(!aio.calib.cleans_volume_calc)
      return;
  
    tmp = (u32)aio.calib.cleans_volume_calc * 100 / (u32)aio.calib.cleans_time_set;
    aio.calib.cleans_quotient_calc = (u16)tmp;
    dio.calib.cleans_finished = 1;
    if(dio.calib.cleans_quotient_save)
    {
      aio.calib.cleans_quotient_cur = aio.calib.cleans_quotient_calc;
      dio.calib.cleans_quotient_save = 0;
      SaveDataDelayed(A_BLOCK, 75*2, 4*2);
//      DbgOut("CleansSave\n");
    }
  }
  
}


//  калибровка водички
void TimerCalibWaterFeed(void)
{
  static u8 step = 0;
  if(aio.data.mode != MODE_SERVICE || !dio.calib.water_calib_on)
  {
    step = 0;
    return;
  }

  //  обнуляем переменные

  //  0. инициализация калибровки
  if(step == 0)
  {
    dio.calib.water_on = 0;
    dio.calib.water_finished = 0;
    aio.calib.water_quotient_calc = 0;
    step++;
//    DbgOut("Step0\n");
  }
  //  1. включаем воду
  if(step == 1)
  {
    if(!dio.calib.water_on || !aio.calib.water_imp_set)
      return;
    dio.calib.water_finished = 0;
    aio.calib.water_quotient_calc = 0;
    aio.calib.water_imp_cur = aio.calib.water_imp_set;
    WaterFeedOn();
    step++;
//    DbgOut("Step1\n");
  }
  
  //  2. выключаем воду
  if(step == 2)
  {
    if(!dio.calib.water_on)
      step = 0;
    if(aio.calib.water_imp_cur)
      return;
    dio.calib.water_on = 0;
    WaterFeedOff();
    step++;
//    DbgOut("Step2\n");
  }
 
  //  3. конец калибровки
  if(step == 3)
  {
    if(dio.calib.water_on)
    {
      step = 0;
      TimerCalibWaterFeed();
      return;
    }
    if(!aio.calib.water_imp_set)
      return;
    aio.calib.water_quotient_calc = (u32)(aio.calib.water_volume_calc) * 100 / (u32)(aio.calib.water_imp_set);
    dio.calib.water_finished = 1;
    if(dio.calib.water_quotient_save)
    {
      aio.calib.water_quotient_cur = aio.calib.water_quotient_calc;
      dio.calib.water_quotient_save = 0;
      SaveDataDelayed(A_BLOCK, 80*2, 6*2);
//      DbgOut("WaterSaved\n");
    }
  }
}

//  таймер калибровки конвейера
void TimerCalibBelt(void)
{
  if(aio.data.mode != MODE_SERVICE || !dio.calib.belt_calib_on)
    return;
  
  if(aio.conf.belt_roller_diameter && aio.conf.belt_nominal_rpm && aio.conf.belt_gear_ratio)
  {
    u32 speed = (u32)aio.conf.belt_nominal_rpm * PI100 * (u32)aio.conf.belt_roller_diameter / ((u32)aio.conf.belt_gear_ratio * 60 * 100);
    aio.conf.belt_speed_calc = (u16) speed;
    if(dio.calib.belt_speed_save)
    {
      aio.conf.belt_speed_cur = aio.conf.belt_speed_calc;
      dio.calib.belt_speed_save = 0;
      SaveDataDelayed(A_BLOCK, 0, 8*2);
    }
  }
}

//  поменять режим
void ChangeMode(u8 mode)
{
  u8 *p_permit = (u8*)&dio.data.mode_prep_permit;
  u8 *p_pressed = (u8*)&dio.data.mode_prep_pressed;
  int i;

  aio.data.mode = mode;

  //  ставим разрешения на кнопки
  for(i=1; i<MODES_NUMBER; i++)
    p_permit[i-1] = modes_trans[mode][i];

  //  очищаем все нажатые кнопки, кроме последней нажатой
  memset(&p_pressed[0], 0, MODES_NUMBER-1);
  p_pressed[mode-1] = 1;
//  DbgOut("mode %d\n", mode);

}

//  обрабатываем внешние сигналы
void TimerManageExternal(void)
{
  static u8 ext_in_prev = 0, ext_in_cur = 0;

//  управляем внешним выходом
  if(dio.conf.ext_in_enable)
  {
    if(dio.data.jx_is_on && aio.data.mode == MODE_WORK)
      ExternalOn();
    else
      ExternalOff();
  }

//  управляемся внешним входом
  ext_in_cur = GetExternal();
  if(ext_in_cur != ext_in_prev && ext_in_cur && aio.data.mode == MODE_PAUSE)
  {
    ext_in_prev = ext_in_cur;
    if(dio.conf.ext_out_enable)
    {aio.data.tmp_watch = 15;
      ChangeMode(MODE_WORK);
    }
  }
  if(ext_in_cur != ext_in_prev && !ext_in_cur)
  {
    ext_in_prev = ext_in_cur;
    if(dio.conf.ext_out_enable)
      if(aio.data.mode == MODE_WORK)
        ChangeMode(MODE_PAUSE);
  }
}

//  САМЫЙ ГЛАВНЫЙ ТАЙМЕР
void TimerGeneral_10ms(void)
{
  static u16 cnt_50ms = 0;
  static u16 cnt_500ms = 0;
  static u16 cnt_1s = 0;
  static u16 cnt_10s = 0;
  u8 buf[30];
  u8 st1, st2;

  st1 = GetTimeTicks();

  //  счетчик воды
  TimerReadWaterExpense();
  TimerWaterRefresh();
  TimerManageStates();
  
  //  все, что касается modbus
  TimerManageModbusMaster();
  TimerMBSlave();
  //  проверяем выходы
//  ManageOutputs();
  //  апдейтим выходы
  OutputUpdate(FALSE);
 
  //  DCON (входы и выходы)
  TimerUpdateDCON();

  //  считаем таймауты
  TimerManageTimeouts();

  //  рекалькуляция параметров
  TimerRecalculate();

  //  управление режимами
  TimerManageButtons();
  TimerManageMode();
//  cnt_system++;

  TimerManageExternal();
  
  //  сброс статистики
  TimerCheckResetStats();

  //  калибровка ПАВ
  TimerCalibCleans();
  //  калибровка воды
  TimerCalibWaterFeed();
  //  калибровка конвейера
  TimerCalibBelt();

  //  50 мс таймер
  if(cnt_50ms++ >= 5)
  {
    TimerUpdateInputs();
//    TimerUpdateButtons();

    cnt_50ms = 0; 
  }

  if(cnt_500ms++ >= 50)
  {
    TimerLight();
    TimerSiren();

    cnt_500ms = 0;
  }

  //  1 с таймер
  if(cnt_1s++ >= 100)
  {
    int i;
    //  для поддержания уровня pH
    TimerWashPH();

    //  управление процессами в режимах
    TimerModePreparation();
    TimerModeDrain();
    TimerModeStop();
    TimerModePause();
    TimerModeWork();
    TimerModePause();
    TimerModeManual();
    TimerModeService();

    cnt_system++;
    cnt_1s = 0;

    //  таймер калибровки дозирования ПАВ
    if(aio.calib.cleans_time_cur)  aio.calib.cleans_time_cur--;
    //  таймер слива
    if(aio.data.discharge_cnt)     aio.data.discharge_cnt--;
    if(aio.data.cleans_cnt)        aio.data.cleans_cnt--;
    if(aio.data.wash_ph_cnt)       aio.data.wash_ph_cnt--;
    if(aio.data.water_refresh_cnt) aio.data.water_refresh_cnt--;

  }                                 

  if(cnt_10s++ >= 1000)
  {
    //  сохраняем статистику в энергонезависимую память
    SaveStats();
    
    cnt_10s = 0;
  }
  
  st2 = GetTimeTicks();
  if(st2-st1 >= 10)
    DbgOut("Таймер не успевает, бро. Ticks:%d..%d\n", st1, st2);
}
