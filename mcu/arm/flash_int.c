﻿#include "stm32f10x.h"
#include "flash_int.h"
#include "../common/crc16.h"
#include <string.h>
#include <stddef.h>

/*
 * DEFINES
 */

#define FI_HEADER_SIZE      8        //  размер заголовка (crc16 + counter + size = 8)
#define FI_PAGE_SIZE        256     //  размер страницы
#define FI_NUMBER_PAGES     8192    //  количество страниц (8192*256 = 2Mb)
#define FI_MAX_DATA_BLOCKS  512     //  макс. количество блоков данных в массиве tFlashData
#define FI_MAX_BUFFER_SIZE  256     //  размер буфера (пока ограничен страницей)
#define FI_MAX_DATA_SIZE    (256-FI_HEADER_SIZE)    //  макс. размер буфера для сохранения
                                                    //  пока ограничение - одна страница минус
                                                    //  заголовок

#define FI_CRC_POS  0
#define FI_CNT_POS  2
#define FI_SIZE_POS 6
#define FI_DATA_POS 8

/*
 * FUNCTIONS' DECLARATIONS
 */

void FI_FormData(u16 idx);
s16  FI_LoadData(u16 idx);
void FI_SaveData(u16 idx);

/*
 * VARIABLES
 */

tFlashData flash_data[FI_MAX_DATA_BLOCKS];
u16 flash_data_num = 0;
u8 flash_buf[FI_MAX_BUFFER_SIZE];


/*
 * FUNCTIONS' DEFINITIONS
 */

void FI_Init(void)
{
    sFLASH_Init();
}

void FI_DeInit(void)
{
    sFLASH_DeInit();
}

s16 FI_AssignData(void* ptr, u16 size, u16 page, u8 num_pages)
{
    int i;
    u16 x11, x12, x21, x22;
    //  проверки
    if(size > FI_MAX_DATA_SIZE)
        return -1;

    if(ptr == NULL)
        return -2;

    if(!num_pages)
        return -3;

    if(page+num_pages > FI_NUMBER_PAGES)
        return -4;

    x21 = page;
    x22 = page + num_pages;

    for(i=0; i<flash_data_num; i++)
    {
        x11 = flash_data[i].page;
        x12 = flash_data[i].page + flash_data[i].num_pages;
        if((x12 < x21) || (x22 < x11))
            continue;
        else
            return -5;
    }

    //  добавляем
    flash_data[flash_data_num].data_ptr = ptr;
    flash_data[flash_data_num].size = size;
    flash_data[flash_data_num].page = page;
    flash_data[flash_data_num].num_pages = num_pages;
    flash_data[flash_data_num].counter = 0;
    flash_data[flash_data_num].write_flag = 0;
    flash_data[flash_data_num].cur_page = page;

    flash_data_num++;
    return (flash_data_num-1);
}


//  формирование буфера для записи
void FI_FormData(u16 idx)
{
    u16 crc;

    //  копируем данные
    __disable_irq();
    memcpy(&flash_buf[FI_DATA_POS], flash_data[idx].data_ptr, flash_data[idx].size);
    __enable_irq();

    //  инкрементируем и записываем счетчик
    flash_data[idx].counter++;
    memcpy(&flash_buf[FI_CNT_POS], &flash_data[idx].counter, 4);

    //  записываем размер
    memcpy(&flash_buf[FI_SIZE_POS], &flash_data[idx].size, 2);

    //  считаем CRC16 и копируем в буфер (начиная с позиции FI_CNT_POS=2)
    crc = GetCRC16(&flash_buf[FI_CNT_POS], flash_data[idx].size + FI_HEADER_SIZE - 2);
    memcpy(&flash_buf[FI_CRC_POS], &crc, 2);
}

//  пометка данных на сохранение по индексу
s16 FI_FlagDataToSaveByIndex(u16 idx)
{
    if(idx >= flash_data_num)
        return -1;
    if(flash_data[idx].data_ptr == NULL)
        return -2;

    flash_data[idx].write_flag = 1;
    return 0;
}

//  пометка данных на сохранение по указателю
s16 FI_FlagDataToSaveByPtr(void* ptr)
{
    u16 idx;
    for(idx=0; idx<flash_data_num; idx++)
        if(flash_data[idx].data_ptr == ptr)
            break;
    if(idx == flash_data_num)
        return -1;

    flash_data[idx].write_flag = 1;
    return 0;
}


//  сохранение помеченных данных
void FI_SaveFlaggedData(void)
{
    u16 idx;
    for(idx=0; idx<flash_data_num; idx++)
        if(flash_data[idx].write_flag)
        {
            //  сбрасываем флаг
            flash_data[idx].write_flag = 0;
            //  формируем данные в буфере
            FI_FormData(idx);
            //  сохраняем данные
            FI_SaveData(idx);

        }
}

//  сохранение данных (в т.ч. циклично-страничное)
void FI_SaveData(u16 idx)
{
    flash_data[idx].cur_page++;
    if(flash_data[idx].cur_page >= flash_data[idx].page + flash_data[idx].num_pages)
        flash_data[idx].cur_page = flash_data[idx].page;
    sFLASH_ErasePage((u32)(flash_data[idx].cur_page) * FI_PAGE_SIZE);
    sFLASH_WritePage((u8*)&flash_buf[0], ((u32)(flash_data[idx].cur_page) * FI_PAGE_SIZE), FI_PAGE_SIZE);
}

//  загрузка данных по индексу
s16 FI_LoadDataByIndex(u16 idx)
{
    if(idx >= flash_data_num)
        return 0;
    if(flash_data[idx].data_ptr == NULL)
        return 0;

    return FI_LoadData(idx);
}

//  загрузка данных по указателю
s16 FI_LoadDataByPtr(void* ptr)
{
    u16 idx;
    for(idx=0; idx<flash_data_num; idx++)
        if(flash_data[idx].data_ptr == ptr)
            break;
    if(idx == flash_data_num)
        return 0;

    return FI_LoadData(idx);
}

s16 FI_LoadAllData(void)
{
    int idx;
    s16 error;
    for(idx=0; idx<flash_data_num; idx++)
        if((error = FI_LoadDataByIndex(idx)) != 0)
            return error;

    return 0;
}

//  загрузка данных (в т.ч. циклично-страничное)
s16 FI_LoadData(u16 idx)
{
    u32 i;

    for(i=flash_data[idx].page; i<flash_data[idx].page+flash_data[idx].num_pages; i++)
    {
        u16 crc, size;
        u32 counter;

        //  читаем первую страницу и вычленяем нужные данные
//        sFLASH_ReadBuffer(&flash_buf[0], (i * FI_PAGE_SIZE), flash_data[idx].size + FI_HEADER_SIZE);
        sFLASH_ReadBuffer(&flash_buf[0], (i * FI_PAGE_SIZE), FI_PAGE_SIZE);
        memcpy(&crc, &flash_buf[FI_CRC_POS], 2);
        memcpy(&size, &flash_buf[FI_SIZE_POS], 2);
        memcpy(&counter, &flash_buf[FI_CNT_POS], 4);

        //  если crc не совпадает, то все
        if(GetCRC16(&flash_buf[FI_CNT_POS], flash_data[idx].size + FI_HEADER_SIZE - 2) != crc)
            return -1;


        //  если первая страница - то записываем данные и скипуем к следующей странице (если она есть)
        if(i == flash_data[idx].page)
        {
            flash_data[idx].cur_page = i;
            flash_data[idx].counter = counter;

            __disable_irq();
            memcpy(flash_data[idx].data_ptr, &flash_buf[FI_DATA_POS], flash_data[idx].size);
            __enable_irq();

            continue;
        }

        //  если счетчик предыдущей страницы больше - то все
        if(flash_data[idx].counter >= counter)
            break;

        flash_data[idx].cur_page = i;
        flash_data[idx].counter = counter;

        __disable_irq();
        memcpy(flash_data[idx].data_ptr, &flash_buf[FI_DATA_POS], flash_data[idx].size);
        __enable_irq();
    }

    return 0;
}


s16 FI_InitDataByIndex(u16 idx)
{
    u32  i;
    if(idx >= flash_data_num)
        return -1;
    if(flash_data[idx].data_ptr == NULL)
        return -2;

    FI_FormData(idx);
    for(i=flash_data[idx].page; i<flash_data[idx].num_pages+flash_data[idx].page; i++)
//        sFLASH_WriteBuffer((u8*)&flash_buf[0], (i * FI_PAGE_SIZE), flash_data[idx].size + FI_HEADER_SIZE);
    {
        sFLASH_ErasePage(i * FI_PAGE_SIZE);
        sFLASH_WritePage((u8*)&flash_buf[0], (i * FI_PAGE_SIZE), FI_PAGE_SIZE);
    }

    return 0;
}

s16 FI_InitDataByPtr(void* ptr)
{
    u16 idx;
    for(idx=0; idx<flash_data_num; idx++)
        if(flash_data[idx].data_ptr == ptr)
            break;
    if(idx == flash_data_num)
        return -1;

    return FI_LoadData(idx);
}

s16 FI_InitAllData(void)
{
    int idx;
    s16 error;
    for(idx=0; idx<flash_data_num; idx++)
        if((error = FI_InitDataByIndex(idx)) != 0)
            return error;
    return 0;
}
