﻿#ifndef _FLASH_INT_H_
#define _FLASH_INT_H_

#include "../hl/stm32_eval_spi_flash.h"

typedef struct
{
    void*   data_ptr;
    u16     size;
    u16     page;
    u8      num_pages;
    u32     counter;
    u16     crc;
    u8      write_flag;
    u16     cur_page;
} tFlashData;

/*typedef struct tFlashSaveData
{
    u32 counter;
    u16 size;
    u16 crc;
    void* data;
};*/

void FI_Init(void);

s16 FI_AssignData(void* data_ptr, u16 size, u16 page, u8 num_pages);

s16  FI_FlagDataToSaveByIndex(u16 idx);
s16  FI_FlagDataToSaveByPtr(void* ptr);
void FI_SaveFlaggedData(void);

s16 FI_LoadDataByIndex(u16 idx);
s16 FI_LoadDataByPtr(void* ptr);
s16 FI_LoadAllData(void);

s16  FI_InitDataByIndex(u16 idx);
s16  FI_InitDataByPtr(void* ptr);
s16  FI_InitAllData(void);

#endif
