﻿#ifndef _MODBUS_COMMON_H_
#define _MODBUS_COMMON_H_

#define MAX_BIT_EVENTS      100
#define MAX_REG_EVENTS      100

#define MAX_MB_BUFFER	270

#define MBS_TIMEOUT     1000
#define MBM_TIMEOUT     1000

typedef enum
{
  MB_STS_RX,
  MB_STS_CHECK_PACKET,
  MB_STS_RX_DONE,
  MB_STS_TX,
  MB_STS_TX_DONE,
} enMBStatus;



typedef struct
{
  u8  addr;
  u8  error;
  u8  inbuf[MAX_MB_BUFFER];
  u16 inbuf_cnt;
  u8  outbuf[MAX_MB_BUFFER];
  u16 outbuf_cnt;
  u16 outbuf_size;
  u16 timeout;
  u8  timeout_start;
  u16 status;
  int event;
} tModbusBuffer;

typedef enum
{
  MB_DT_COIL = 0,
  MB_DT_REGISTER
} enMBDataType;

typedef enum
{
  MB_FN_RD_DO_COILS = 1,
  MB_FN_RD_DI_CONTACTS = 2,
  MB_FN_RD_AO_HOLD_REGS = 3,
  MB_FN_RD_AI_REGS = 4,
  MB_FN_WR_DO_COIL_SINGLE = 5,
  MB_FN_WR_AO_HOLD_REG_SINGLE = 6,
  MB_FN_WR_DO_COILS_MULTI = 15,
  MB_FN_WR_AO_HOLD_REGS_MULTI = 16
} enMBFunction;


typedef enum
{
  MB_MODE_MASTER = 0,
  MB_MODE_SLAVE
} enMBMode;

typedef enum
{
  //  примечание: при обнаружении ошибки паритета (parity, CRC) подчиненный не отвечает главному
  MB_ERR_FUNC_UNSUPPORTED  =  1,     //  код функции не поддерживается
  MB_ERR_ADDR_INACCESSABLE =  2,     //  адрес данных не доступен данному подчиненному
  MB_ERR_WRONG_DATA        =  3,     //  величина в поле данных недопустима
  MB_ERR_DUE_EXECUTION     =  4,     //  произошла ошибка во время выполнения операции
  MB_ERR_MUCH_TIME_NEEDED  =  5,     //  требуется много времени для выполнения операции
  MB_ERR_SLAVE_BUSY        =  6,     //  подчиненный занят обработкой команды
  MB_ERR_PROGRAM_FUNCTION  =  7,     //  подчиненный не может выполнить программную функцию, принятую в запросе (код функции 13 или 14)
  MB_ERR_DUE_READ_EXT_MEM  =  8      //  подчиненный обнаружил ошибку паритета, читая расширенную память

} enMBError;

typedef u8 (*tdModbusBitFunction)(u8* data, u16 offset, u16 size); //  buf, offs, size
typedef u8 (*tdModbusRegFunction)(u16* data, u16 offset, u16 size); //  buf, offs, size

typedef struct
{
  u8  *data;       //  указатель на данные, с которым будет работать модбас
  u16 offset;      //  виртуальный адрес этого массива в модбасе
  u16 number;      //  количество битов в массиве
//  tdModbusBitFunction rd_function;  //  пользовательская функция, вызываемая непосредственно до чтения
  tdModbusBitFunction wr_function;  //  пользовательская функция, вызываемая непосредственно до записи
} tdModbusBitEvent;

typedef struct
{
  u16 *data;       //  указатель на данные, с которым будет работать модбас
  u16 offset;      //  виртуальный адрес этого массива в модбасе
  u16 number;      //  количество слов в массиве
//  tdModbusRegFunction rd_function;  //  пользовательская функция, вызываемая непосредственно до чтения
  tdModbusRegFunction wr_function;  //  пользовательская функция, вызываемая непосредственно до записи
} tdModbusRegEvent;


#endif
