﻿#include "stm32f10x.h"
#include "modbus_slave.h"
#include "crc16.h"
#include "string.h"
#include "stddef.h"

tdModbusBitEvent bit_events[MAX_BIT_EVENTS];
tdModbusRegEvent reg_events[MAX_REG_EVENTS];
u16 n_bit_events;
u16 n_reg_events;

u8 tmp_buf[256];

tModbusBuffer mbs;
USART_TypeDef* mbs_usart_typedef;
uint8_t mbs_usart_id;

//  инит железа
void MBS_InitHard(uint8_t usart_id)
{
    mbs_usart_id = usart_id;

    mbs_usart_typedef = PCB_USART_Init(usart_id, 115200, USART_WordLength_8b,
            USART_StopBits_1, USART_Parity_No, 1);

    PCB_UsartAssignIsr(usart_id, 0, 0, MBS_USART_IRQHandler);

    /* Enable USART1 */
    USART_Cmd(mbs_usart_typedef, ENABLE);
    USART_ITConfig(mbs_usart_typedef, USART_IT_RXNE, ENABLE);
    USART_ITConfig(mbs_usart_typedef, USART_IT_TXE, DISABLE);
    USART_ITConfig(mbs_usart_typedef, USART_IT_TC, DISABLE);
}

//  инит перемешек
void MBS_InitVars(u8 address)
{
    mbs.addr = address;
    mbs.error = 0;
    mbs.inbuf_cnt = 0;
    mbs.outbuf_cnt = 0;
    mbs.outbuf_size = 0;
    n_bit_events = 0;
    n_reg_events = 0;

//    mbs_coils = coils;
//    mbs_registers = registers;

    MBS_InitRecv();
}

void MBS_Init(u8 usart_id, u8 address)
{
    MBS_InitVars(address);
    MBS_InitHard(usart_id);
}

void MBS_InitRecv(void)
{
    mbs.inbuf_cnt = 0;
    mbs.error = 0;
//  mbs_status = MBS_STS_RECV;
}

void MBS_InitTransmit(void)
{
    mbs.outbuf_cnt = 0;
    mbs.outbuf_size = 0;
}

void MBS_USART_IRQHandler(void)
{
    u8 rx_byte;

    //	прерывание по приему
    if ((mbs_usart_typedef->SR & USART_FLAG_RXNE) != (uint16_t) RESET)
    {
        rx_byte = USART_ReceiveData(mbs_usart_typedef);
        if (MBS_Recv(rx_byte))
        {
            //	переключаем на передачу
            USART_ITConfig(USART3, USART_IT_TXE, ENABLE); //  включаем прерывание по окончании передачи
            USART_ITConfig(USART3, USART_IT_RXNE, DISABLE); //  выключаем прерывание по приему
            MODBUS_SLAVE_485_SET_TX();
//	  GPIO_WriteBit(GPIOB, GPIO_Pin_1, Bit_RESET);          //  включаем на прием

            //  запускаем отправку
//	  USART_SendData(USART3, mbs.outbuf[mbs.outbuf_cnt++]);
            return;
        }
        mbs.status = MB_STS_RX;
        return;
    }

    //	прерывание по отправке
    if ((mbs_usart_typedef->SR & USART_FLAG_TXE) != (uint16_t) RESET)
    {
        if (mbs.outbuf_cnt >= mbs.outbuf_size || !mbs.outbuf_size)
        { //  переводим в режим приема
            MBS_InitRecv();
            USART_ITConfig(USART3, USART_IT_TXE, DISABLE); //  выключаем прерывание по окончании передачи
            USART_ITConfig(USART3, USART_IT_RXNE, ENABLE); //  включаем прерывание по приему
            MODBUS_SLAVE_485_SET_RX();
            mbs.status = MB_STS_TX_DONE;
            return;
        }
        USART_SendData(USART3, mbs.outbuf[mbs.outbuf_cnt++]);
        mbs.status = MB_STS_TX;
        return;
    }

    /*  uint16_t rx_frame;

     if ((kbus_usart_typedef->SR & USART_FLAG_RXNE) != (uint16_t)RESET)
     {
     rx_frame = USART_ReceiveData(kbus_usart_typedef);
     KBus_recieve_eh(rx_frame); //interrupt  0x2b  //using m_banc1
     return;
     }
     if((kbusVar.tx_last_frame_flag)&&((kbus_usart_typedef->SR & USART_FLAG_TC) != (uint16_t)RESET))
     {//AP 885-1371B   HR 1234 F2    150 64 95
     kbusVar.tx_last_frame_flag = 0;
     USART_ITConfig(kbus_usart_typedef,USART_IT_TC, DISABLE);
     USART_ReceiveData(kbus_usart_typedef); // kill parasit rx;
     KBUS_485_SET_RX(); //rs_dir = RS_SET_RX;
     kbusVar.cur_frame = KBUS_SFRAMEID_ADDR;
     KBus_TimeOut_Set05mS();//       // 1 mc //таймойт на начало ответа
     USART_ITConfig(kbus_usart_typedef,USART_IT_RXNE, ENABLE);
     }
     else if ((kbus_usart_typedef->SR & USART_FLAG_TXE) != (uint16_t)RESET)
     {
     KBus_send_eh();
     }*/
}

//  возвращает 0, если прием закончился
u8 MBS_Recv(u8 rx_byte)
{
//  static int mbs_timeout = 0;

//  if(mbs_status != MBS_STS_RECV)
//    return mbs_status;

    // TIMEOUT REDO
    /*
     mbs_timeout++;
     if(mbs_timeout > 50)
     {
     mbs_timeout = 0;
     mbs.inbuf_cnt = 0;
     }*/

    mbs.timeout = 0;
    MBS_ResetTimeout();

    mbs.inbuf[mbs.inbuf_cnt++] = rx_byte;

    if (mbs.inbuf_cnt > 2)
    {
        switch (mbs.inbuf[1])
        {
            case 0x01:
            case 0x02:
            case 0x03:
            case 0x04:
            case 0x05:
            case 0x06:
                if (mbs.inbuf_cnt >= 8)
                    return 1;
                break;

            case 0x0F:
            case 0x10:
                if (mbs.inbuf_cnt >= 7)
                    if (mbs.inbuf_cnt >= mbs.inbuf[6] + 9)
                        return 1;
                break;
        }
    }
    return 0;
}

/*void MBSlaveInit(u8 addr)
 {
 NVIC_InitTypeDef    NVIC_InitStructure;
 USART_InitTypeDef   USART_InitStructure;
 GPIO_InitTypeDef    GPIO_InitStructure;

 //  clock
 RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
 RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);

 //  Rx
 GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_11;
 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
 GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
 GPIO_Init(GPIOB, &GPIO_InitStructure);

 //  Tx
 GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_10;
 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
 GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
 GPIO_Init(GPIOB, &GPIO_InitStructure);


 //  параметры усарта и инит
 USART_InitStructure.USART_BaudRate            = 115200;
 USART_InitStructure.USART_WordLength          = USART_WordLength_8b;
 USART_InitStructure.USART_StopBits            = USART_StopBits_1;
 USART_InitStructure.USART_Parity              = USART_Parity_No ;
 USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
 USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;
 USART_Init(USART3, &USART_InitStructure);
 USART_Cmd(USART3, ENABLE);


 //  перемешки
 mbs.addr = addr;
 mbs.error = 0;
 mbs.inbuf_cnt = 0;
 mbs.outbuf_cnt = 0;
 mbs.outbuf_size = 0;
 MBSlaveInitRecv();

 //  натягиваем обработчик прерываний на наши функции
 InterruptAssignFunction(IT_USART3_RX, MBSlaveOnRecv);
 InterruptAssignFunction(IT_USART3_TX, MBSlaveOnSend);

 //  NVIC
 NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
 NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
 NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
 NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
 NVIC_Init(&NVIC_InitStructure);

 //  разрешаем прерывание по приему
 USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
 }*/

//  функция для вызова в 1 мс таймере
//  REDO
/*void MBS_Manage(void)
 {
 switch(mbs_status)
 {
 case MBS_STS_INIT:
 MBSlaveRecvInit();
 break;

 case MBS_STS_RECV:
 MBS_Recv();
 break;

 case MBS_STS_CHECK_PACKET:
 MBS_CheckPacket();
 break;


 case MBS_STS_FORM_ANSWER:
 MBS_FormAnswer();
 break;

 case MBS_STS_SEND:
 MBS_Send();
 break;

 default:
 break;
 }
 }*/

//  проверка пакета на правильность
u8 MBS_CheckPacket(void)
{
    u16 offset;

    mbs.error = 0;
    mbs.event = -1;

    offset = ((uint16_t)(mbs.inbuf[2]) << 8) + mbs.inbuf[3];

    //  если контрольная сумма не сошлась или адрес устройства не совпадает -
    //  игнорируем и переводим на прием
    if (GetCRC16(mbs.inbuf, mbs.inbuf_cnt) || (mbs.inbuf[0] != mbs.addr))
    {
        MBS_InitRecv();
        return 0;
    }

    //  проверка доступного оффсета для койла
    if (mbs.inbuf[1] == 0x01 || mbs.inbuf[1] == 0x02 || mbs.inbuf[1] == 0x05 || mbs.inbuf[1] == 0x0F)
    {
        if ((mbs.event = MBS_FindBitEvent(offset)) == -1)
            mbs.error = MB_ERR_ADDR_INACCESSABLE;
    }

    //  проверка доступного оффсета для регистра
    if (mbs.inbuf[1] == 0x03 || mbs.inbuf[1] == 0x04 || mbs.inbuf[1] == 0x06 || mbs.inbuf[1] == 0x10)
    {
        if ((mbs.event = MBS_FindRegEvent(offset)) == -1)
            mbs.error = MB_ERR_ADDR_INACCESSABLE;
    }

    //  проверка функции пакета
    if (!((mbs.inbuf[1] >= 0x01 && mbs.inbuf[1] <= 0x06)
            || (mbs.inbuf[1] >= 0x0F && mbs.inbuf[1] <= 0x10)))
        mbs.error = MB_ERR_FUNC_UNSUPPORTED;

    return 1;
}

//  формирование ответа
void MBS_FormAnswer(void)
{
    s16 i, j;
    u16 crc;
    u16 crc_pos;
    u16 offset;
    //  адрес и функция
    mbs.outbuf[0] = mbs.inbuf[0];
    mbs.outbuf[1] = mbs.inbuf[1];
    offset = ((u16)(mbs.inbuf[2]) << 8) + mbs.inbuf[3];

    //  формируем исключение
    if (mbs.error)
    {
        mbs.outbuf[1] += 0x80;
        mbs.outbuf[2] = mbs.error;
        crc = GetCRC16(mbs.outbuf, 3);
        mbs.outbuf[3] = (crc >> 8) & 0xFF;
        mbs.outbuf[4] = crc & 0xFF;
        mbs.outbuf_size = 5;
        return;
    }

    //  формируем пакет с битами (чтение)
    if (mbs.inbuf[1] == 0x01 || mbs.inbuf[1] == 0x02)
    {
        u16 bits_cnt = 0;
        uint16_t bits_num = ((uint16_t)(mbs.inbuf[4]) << 8) + mbs.inbuf[5];
        if (bits_num % 8)
            mbs.outbuf[2] = bits_num / 8 + 1;
        else
            mbs.outbuf[2] = bits_num / 8;
        memset(&mbs.outbuf[3], 0, mbs.outbuf[2]);
        for (i = 0; i < mbs.outbuf[2]; i++)
            for (j = 0; j < 8; j++, bits_cnt++)
            {
                index = offset + i * 8 + j;
                if (bits_cnt >= bits_num)
                    break;
                if (MBS_GetBit(offset))
                    mbs.outbuf[i + 3] |= 1 << j;
            }
        //  вычисляем смещение для crc
        crc_pos = 3 + mbs.outbuf[2];
    }

    //  формируем пакет со словами (чтение)
    if (mbs.inbuf[1] == 0x03 || mbs.inbuf[1] == 0x04)
    {
        u16 words_num = ((u16)(mbs.inbuf[4]) << 8) + mbs.inbuf[5];
        mbs.outbuf[2] = words_num * 2;
        memcpy(&mbs.outbuf[3], (void*)MBS_GetRegPtr(offset), mbs.outbuf[2]);

        SwapData16((u16*) &mbs.outbuf[3], words_num);

        //  вычисляем смещение для crc
        crc_pos = 3 + mbs.outbuf[2];
    }

    //  формируем подтверждение записи (1 бит)
    if (mbs.inbuf[1] == 0x05)
    {
        //  тупо копируем, ответ совпадает с запросом
        memcpy(mbs.outbuf, mbs.inbuf, 8);
        crc_pos = 6;
    }

    //  формируем подтверждение записи (1 слово)
    if (mbs.inbuf[1] == 0x06)
    {
        uint16_t value = ((uint16_t)(mbs.inbuf[4]) << 8) + mbs.inbuf[5];
        //  тупо копируем, ответ совпадает с запросом
        memcpy(mbs.outbuf, mbs.inbuf, 8);
        crc_pos = 6;
    }

    //  формируем подтверждение записи (много бит)
    if (mbs.inbuf[1] == 0x0F)
    {
  //      u16 num_bytes, cnt_coils = 0, num_coils;
//        num_coils = ((uint16_t)(mbs.inbuf[4]) << 8) + mbs.inbuf[5];
//        num_bytes = mbs.inbuf[6];
        memcpy(&mbs.outbuf[2], &mbs.inbuf[2], 4);
        crc_pos = 6;
    }

    //  формируем подтверждение записи (много слов)
    if (mbs.inbuf[1] == 0x10)
    {
//        u16 num_words = ((u16)(mbs.inbuf[4]) << 8) + mbs.inbuf[5];
//        u16 num_bytes = mbs.inbuf[6];
//        SwapData16((u16*) &mbs.inbuf[7], num_words);

        //  ответная посылка
        memcpy(&mbs.outbuf[2], &mbs.inbuf[2], 4);
        crc_pos = 6;
    }

    //  считаем CRC для всего этого непотребства
    crc = GetCRC16(mbs.outbuf, crc_pos);
    mbs.outbuf[crc_pos] = (crc >> 8) & 0xFF;
    mbs.outbuf[crc_pos + 1] = crc & 0xFF;

    //  можно отправлять
    mbs.outbuf_size = crc_pos + 2;
    return;
}

//  установка значения бита
void MBS_SetBit(u16 offset, u8 value)
{
    if (value)
        bit_events[mbs.event].data[(offset - bit_events[mbs.event].offset) / 8] |= (1 << (offset % 8));
    else
        bit_events[mbs.event].data[(offset - bit_events[mbs.event].offset) / 8] &= ~(1 << (offset % 8));
}

//  установка значения регистра
void MBS_SetReg(uint16_t value, uint16_t offset)
{
    reg_events[mbs.event].data[offset - reg_events[mbs.event].offset] = value;
}

//  копирование данных в биты
void MBS_SetBitMultiple(u16* data, u16 offset, u16 number)
{
    //  TODO - проверка на выход за границу (number)
    memcpy(&bit_events[mbs.event].data[offset - bit_events[mbs.event].offset], data, number);
}

//  копирование данных в регистры
void MBS_SetRegMultiple(u8* data, u16 offset, u16 number)
{
    //  TODO - проверка на выход за границу (number)
    memcpy(&reg_events[mbs.event].data[offset - reg_events[mbs.event].offset], data, number);
}

//  получение бита
u8 MBS_GetBit(u16 offset)
{
    if(bit_events[mbs.event].data[offset - bit_events[mbs.event].offset])
        return 1;
    return 0;
}

// получение регистра
u16 MBS_GetReg(u16 offset)
{
    return reg_events[mbs.event].data[offset - reg_events[mbs.event].offset];
}


//  получение указателя на регистровые данные по данному оффсету
u8* MBS_GetBitPtr(u16 offset)
{
    return &bit_events[mbs.event].data[offset - bit_events[mbs.event].offset];
}

//  получение указателя на регистровые данные по данному оффсету
u16* MBS_GetRegPtr(u16 offset)
{
    return &reg_events[mbs.event].data[offset - reg_events[mbs.event].offset];
}


//  процессинг данных (только запись, чтение в MBS_FormAnswer)
void MBS_ProcessData(void)
{
    //
    u16 function = mbs.inbuf[1];
    u16 offset = ((u16)(mbs.inbuf[2]) << 8) + mbs.inbuf[3];

    u16 n_event = -1;

    //  операции с койлами ()
    if (    function == 0x01 ||
            function == 0x02 ||
            function == 0x05 ||
            function == 0x0F)
    {
        if((n_event = MBS_FindBitEvent(offset)) == -1);
            return;
        //  запись одного койла
        if (function == 0x05)
        {
            if(bit_events[n_event].wr_function != NULL)
                bit_events[n_event].wr_function(&mbs.inbuf[4], offset, 1);
            else if(bit_events[n_event].data != NULL)
                MBS_SetBit(offset, mbs.inbuf[4]);
        }
        //  запись нескольких койлов
        if (function == 0x0F)
        {
            //  КОД ТРЕБУЕТ ПРОВЕРКИ
            int i,j;
            u16 num_bytes = mbs.inbuf[6];
            u16 num_coils = ((u16)(mbs.inbuf[4]) << 8) + mbs.inbuf[5];
            for(i=0; i<num_bytes; i++)
            {
                for(j=0; j<8; j++)
                    if(num_coils < i*8+j)
                        tmp_buf[i*8+j] = (mbs.inbuf[i] >> j) & 1;
                    else
                        break;
            }
            if(bit_events[n_event].wr_function != NULL)
                bit_events[n_event].wr_function(tmp_buf, offset, num_coils);
            else if(bit_events[n_event].data != NULL)
                MBS_SetBitMultiple(tmp_buf, offset, num_coils);
        }

    }
    //  операции с регистрами
    if (    function == 0x03 ||
            function == 0x04 ||
            function == 0x06 ||
            function == 0x10)
    {
        if((n_event = MBS_FindRegEvent(offset)) == -1);
            return;

        //  запись одного рега
        if(function == 0x06)
        {
            u16 data = ((u16)(mbs.inbuf[4]) << 8) + mbs.inbuf[5];
            if(reg_events[n_event].wr_function != NULL)
                reg_events[n_event].wr_function(&data, offset, 1);
            else if(reg_events[n_event].data != reg_events)
                MBS_SetReg(offset, data);
        }
        //  запись нескольких регов
        if(function == 0x10)
        {
            u16 num_bytes = mbs.inbuf[6];
            memcpy(tmp_buf, mbs.inbuf[7], num_bytes);
            SwapData16(tmp_buf, num_bytes);
            if(reg_events[n_event].wr_function != NULL)
                reg_events[n_event].wr_function((u16*)&tmp_buf, offset, num_bytes);
            else if(reg_events[n_event].data != NULL)
                MBS_SetRegMultiple(tmp_buf, offset, num_bytes);
        }
    }
}
//	таймер (10 мсек)
void MBS_OnTimer(void)
{
    if (mbs.status == MB_STS_RX_DONE)
    {
        //  проверяем пакет. если невалидный - иницируем прием
        if (!MBS_CheckPacket())
        {
            MBS_InitRecv();
            return;
        }

        //  пробегаем по массиву ивентов
        if (!mbs.error)
            MBS_ProcessData();

        //  выполняем все необходимые действия
        //  запрос на чтение

        MBS_InitTransmit();
        MBS_FormAnswer();
        mbs.status = MB_STS_TX;
        //  отправка первого байта
        USART_SendData(mbs_usart_typedef, mbs.outbuf[mbs.outbuf_cnt++]);
    }
}

//  связывание данных для передачи по модбасу с виртуальным адресом
//  в адресном пространстве modbus
u8 MBS_AddBitEvent(u8* data, u16 offset, u16 number, tdModbusBitFunction wr_func)
{
    if (n_bit_events >= MAX_BIT_EVENTS)
        return 0;

    bit_events[n_bit_events].data = data;
    bit_events[n_bit_events].offset = offset;
    bit_events[n_bit_events].number = number;
    bit_events[n_bit_events].wr_function = wr_func;

//    tmp_event.data = data;
//    tmp_event.offset = offset;
//    tmp_event.number = number;
//    tmp_event.rd_function = rd_func;
//    tmp_event.wr_function = wr_func;

    n_bit_events++;
    return 1;
}

//  связывание данных для передачи по модбасу с виртуальным адресом
//  в адресном пространстве modbus
u8 MBS_AddRegEvent(u16* data, u16 offset, u16 number, tdModbusRegFunction wr_func)
{
    if (n_reg_events >= MAX_REG_EVENTS)
        return 0;

    reg_events[n_reg_events].data = data;
    reg_events[n_reg_events].offset = offset;
    reg_events[n_reg_events].number = number;
    reg_events[n_reg_events].wr_function = wr_func;

    //  сразу вставляем в нужное место
//  for(i=0; i<n_reg_events; i++)
//    if(offset < reg_events[i].offset)
//    {
//      idx = i;
//      break;
//    }
//
//  if(idx < n_reg_events)
//    memmove(&reg_events[idx+1], &reg_events[idx], (n_reg_events-idx)*sizeof(tMBRegEvent));
//  memcpy(&reg_events[idx], tmp_event, sizeof(tMBRegEvent));

    n_reg_events++;
    return 1;
}

//  находим битовый ивент
int MBS_FindBitEvent(u16 offset)
{
    u16 i;
    for (i = 0; i < n_bit_events; i++)
        if ((offset >= bit_events[i].offset)
                && (offset < bit_events[i].offset + bit_events[i].number))
            return i;
    return -1;
}

//  находим реговый ивент
int MBS_FindRegEvent(u16 offset)
{
    u16 i;
    for (i = 0; i < n_reg_events; i++)
        if ((offset >= reg_events[i].offset)
                && (offset < reg_events[i].offset + reg_events[i].number))
            return i;
    return -1;
}

void MBS_StartTimeout(void)
{

}

void MBS_StopTimeout(void)
{

}

void MBS_ResetTimeout(void)
{

}

void MBS_SwapData16(u16 *data, u16 len)
{
    int i = 0;
    for(i=0; i<len; i++)
        data[i] = (data[i] << 8) + (data[i] >> 8);
}
