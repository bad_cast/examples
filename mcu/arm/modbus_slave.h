﻿#ifndef _MODBUS_SLAVE_H_
#define _MODBUS_SLAVE_H_

#include "modbus_common.h"
#include "pcb/xr23_hard.h"

#define MBS_ALLOW_OUT_OF_BOUNDS  1

#define MODBUS_SLAVE_485_SET_RX() PCB_RS485_Dir_Set(mbs_usart_id,0);
#define MODBUS_SLAVE_485_SET_TX() PCB_RS485_Dir_Set(mbs_usart_id,1);


void MBS_Init(u8 usart_id, u8 address);
void MBS_InitVars(u8 address);
void MBS_InitHard(uint8_t usart_id);

void MBS_InitRecv(void);
void MBS_InitTransmit(void);

void MBS_USART_IRQHandler(void);

u8   MBS_Recv(u8 rx_byte);
u8   MBS_CheckPacket(void);
void MBS_FormAnswer(void);
void MBS_ProcessData();

u8   MBS_AddBitEvent(u8* data, u16 offset, u16 number, tdModbusBitFunction wr_func);
u8   MBS_AddRegEvent(u16* data, u16 offset, u16 number, tdModbusRegFunction wr_func);

int  MBS_FindBitEvent(u16 offset);
int  MBS_FindRegEvent(u16 offset);

void MBS_SetBit(u16 offset, u8 value);
void MBS_SetReg(uint16_t value, uint16_t offset);
void MBS_SetBitMultiple(u16* data, u16 offset, u16 number);
void MBS_SetRegMultiple(u8* data, u16 offset, u16 number);
u8   MBS_GetBit(u16 offset);
u16  MBS_GetReg(u16 offset);
u8*  MBS_GetBitPtr(u16 offset);
u16* MBS_GetRegPtr(u16 offset);

void MBS_OnTimer(void);

void MBS_StartTimeout(void);
void MBS_StopTimeout(void);
void MBS_ResetTimeout(void);

void MBS_SwapData16(u16 *data, u16 len);

/*
void MBSlaveInit(u8 addr);
u8 MBSlaveAssignRegisterEvent(  u16* data,
                            u16 offset,
                            u16 number,
                            tdFunctionMBEvent rd_func,
                            tdFunctionMBEvent wr_func);

u8 MBSlaveAssignBitEvent(  u8* data,
                            u16 offset,
                            u16 number,
                            tdFunctionMBEvent rd_func,
                            tdFunctionMBEvent wr_func);

//private:
void MBS_PushData(void);
void MBS_OnRecv(void);
void MBSlaveOnSend(void);

void MBSlaveInitRecv(void);
void MBSlaveCheckPacket(void);
void MBSlaveFormAnswer(void);

short MBSlaveFindBitEvent(u16 offset);
short MBSlaveFindRegEvent(u16 offset);

void MBSlaveSetBit(u16 offset, u8 value);
void MBSlaveSetRegister(u16 offset, u16 value);

u8 MBSlaveGetBit(u16 offset);
u16 MBSlaveGetRegister(u16 offset);
u8* MBSlaveGetRegisterPointer(u16 offset);*/

#endif  //  _MODBUS_SLAVE_H_
