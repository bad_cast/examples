﻿/*  crc_avr.h
 *  Модуль подсчета CRC16 прошивки для модуля. для всей прошивки должна == 0
 *  Эталонное значение хранится в последних двух байтах ПЗУ
 *  Вычисляется утилитой hexcrc16
 *  
 *  Исходные данные, которые можно изменить, задаются в нижестоящих определениях
 *  
 *  Last update: 2010-08-11
 */
 
#define KILO              1024   // килобайт
#define MEMORY_SIZE       8*KILO  // объем памяти
#define START_ADDRESS     0x0000  // начальный адрес для вычисления CRC
#define END_ADDRESS       (MEMORY_SIZE + START_ADDRESS) // конечный адрес
#define CRC_DEFAULT       0       // исходное значение CRC

unsigned short crc16;             //  вычисляемое значение CRC

void crc16_init(unsigned short crc_val)
{
	crc16 = crc_val;
}

unsigned int crc16_getnext(unsigned char ser_data)
{
// Update the CRC for transmitted and received data using
// the CCITT 16bit algorithm (X^16 + X^12 + X^5 + 1).
  _WDR();
  crc16  = (unsigned char)(crc16 >> 8) | (crc16 << 8);
  crc16 ^= ser_data;
  crc16 ^= (unsigned char)(crc16 & 0xff) >> 4;
  crc16 ^= (crc16 << 8) << 4;
  crc16 ^= ((crc16 & 0xff) << 4) << 1;
	return(crc16);
}

//  проверка crc
unsigned char check_crc()
{
#ifdef CODE_VISION_DEV
  unsigned char flash *p = START_ADDRESS;
#else
  unsigned char __flash *p = START_ADDRESS;
#endif
	crc16_init(CRC_DEFAULT);
#ifdef CODE_VISION_DEV
  for(p=START_ADDRESS; p<END_ADDRESS; p++)
#else
  for(p=START_ADDRESS; p<(unsigned char __flash *)(END_ADDRESS); p++)
#endif
    crc16_getnext(*p);
  if(crc16)
    return 1;

  return 0;
}
