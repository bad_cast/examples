﻿// HexCrc.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>
#include <conio.h>
#include <string>
#include "HexParse.h"

#define VERSION "1.1"

#define MM_NUMBER	9
const char* mm[] = {"1k", "2k", "4k", "8k", "16k", "32k", "64k", "128k", "256k" };
const int msize[] = {1*KILO, 2*KILO, 4*KILO, 8*KILO, 16*KILO, 32*KILO, 64*KILO, 128*KILO, 256*KILO};

std::string filesrc, filedest;
CHexParse parse;
u32 iStartAddr = 0xFFFFFFFF;
bool bIgnore = true;
u8 iByteFill = 0xFF;
u16 iCrcVal = 0xFFFF;

void PrintPreved()
{
	printf("Calculates CRC16/CCITT of HEX and writes it to last 2 bytes\r\n");
	printf("\r\nUsage: hexcrc16 <src> <dest> <mem size> <start addr> <fill> [-i] [-p]\r\n\r\n");
	printf("<src> - source HEX-file\r\n");
	printf("<dest> - destination HEX-file\r\n");
	printf("<mem size> - 1k, 2k, 4k, 8k, 16k, 32k, 64k, 128k, 256k\r\n");
	printf("<start addr> - may be \"auto\" or number:\r\n");
		printf("  auto - start address calculated automatically\r\n");
		printf("  number - manually set start address, non-negative decimal\r\n");
	printf("<fill> - value to fill unused memory space, decimal [0..255]\r\n");
//	printf("<crc_val> - initial value for CRC16, decimal [0..65535]\r\n");
	printf("[-i] - ignore data crossing\r\n");
	printf("[-p] - waiting for pressing key on exit\r\n");
	printf("\r\nExample: hexcrc16 1.hex 1_s.hex 128k auto 0\r\n");
	printf("Example: hexcrc16 1.hex 1.hex 64k 262144 255 -i -p\r\n");
}

int main(int argc, char* argv[])
{
	int i = 0;
	//	приветствие
	printf("HexCRC16. Intel HEX signing tool using CRC16. Ver. %s\r\n", VERSION);

	//	проверки
	if (argc < 6)
	{
		PrintPreved();
		return 0;
	}
	//	1 <file source>
	filesrc = argv[1];
	
	//	2 <file dest>
	filedest = argv[2];

	//	3 <memory size>
	for(i=0; i<MM_NUMBER; i++)
	{
		if(!strcmp(argv[3], mm[i]))
			break;
	}
	if(i == MM_NUMBER)
	{
		PrintPreved();
		return 0;
	}
	
	//	4 <start addr>
	if(!strcmp(argv[4], "auto"))
		iStartAddr = 0xFFFFFFFF;
	else
		iStartAddr = atoi(argv[4]);

	//	5 <fill byte>
	iByteFill = atoi(argv[5]);

	//	6 <crc init>
//	iCrcVal = atoi(argv[6]);
	iCrcVal = 0xFFFF;

	//	6,7 <ignore data cross>
	if((argc > 6 && !strcmp(argv[6], "-i")) || (argc > 7 && !strcmp(argv[7], "-i")))
		bIgnore = true;
	

	DWORD nTicks = ::GetTickCount();
	parse.Init(msize[i], iStartAddr, bIgnore, iByteFill, iCrcVal);
	parse.ProcessHex(filesrc, filedest);
	parse.Release();
	nTicks = ::GetTickCount() - nTicks;
	switch(parse.GetLastError())
	{
	case ERR_OK:
		printf("Signed hex created successfully to '%s'\r\n", filedest.c_str());
		printf("New data size: %d bytes\r\n", parse.GetDataSize()+2);
		break;
	case ERR_FILE_NOT_FOUND:
		printf("Error: source file not found\r\n");
		break;
	case ERR_FILE_COULD_NOT_CREATE:
		printf("Error: could not write to dest file\r\n");
		break;
	case ERR_HEX_STRING:
		printf("Error: error parsing hex-string\r\n");
		break;
	case ERR_HEX_STRING_CS:
		printf("Error: hex-string checksum error\r\n");
		break;
	case ERR_CRC_BLOCK_BUSY:
		printf("Error: data-block to write crc is busy\r\n");
		break;
	case ERR_OUT_OF_BOUNDS:
		printf("Error: data is out of bounds for this memory size\r\n");
		break;
	case ERR_UNKNOWN_TYPE:
		printf("Error: unknown block type\r\n");
		break;
	case ERR_HEX_LENGTH:
		printf("Error: data block is too long\r\n");
		break;
	case ERR_ADDR_DATA_EXISTS:
		printf("Error: data crossing\r\n");
		break;
	case ERR_HEX_SEGMENT:
		printf("Error: segment for data isn't set\r\n");
		break;
	default:
		printf("Error: error %d\r\n", parse.GetLastError());
		break;
	}
	if(!parse.GetLastError())
	{
		printf("CRC is: 0x%04X\r\n", parse.GetCRC16());
		printf("Execution time: %.3f seconds\r\n", (double)nTicks/1000);
	}

	//	6,7 <pause on exit>
	if((argc > 6 && !strcmp(argv[6], "-p")) || (argc > 7 && !strcmp(argv[7], "-p")))
	{
		printf("\r\nPress any key to exit...\r\n");
		_getch();
	}
	return parse.GetLastError();
}
