﻿#include "stdafx.h"
#include "HexParse.h"
#include <windows.h>

CHexParse::CHexParse() : m_iCRC(0)
, m_data(NULL)
, m_iSegAddrCur(0)
, m_iMemSize(65536)
, m_bEOF(false)
, m_bIgnoreCross(false)
, m_iSegAddrMin(ADDR_MAX)
, m_iSegAddrMax(ADDR_MIN)
, m_iDataAddrMin(ADDR_MAX)
, m_iDataAddrMax(ADDR_MIN)
, m_iDataAddrCur(0)
{
	
}

CHexParse::~CHexParse()
{
	Release();
}

//	опреление по hex-строке конца hex-файала
bool CHexParse::DetectHexEnd(std::string szHexString)
{
	if(szHexString.find(":00000001FF") == -1)
		return false;
	return true;
}

//	калькуляция контрольной суммы для hex-строки
u8 CHexParse::CalcHexCS(tHexPacket* hex)
{
	hex->cs = 0;
	hex->cs += hex->len;
	hex->cs += (u8) (hex->offset >> 8);
	hex->cs += (u8)(hex->offset & 0xFF);
	hex->cs += hex->type;
	for(int i=0; i<hex->len; i++)
		hex->cs += hex->data[i];
	hex->cs = -hex->cs;
	return hex->cs;
}

//	нахождение в hex-файле минимального и максимального адреса сегмента
int CHexParse::FindMinMaxAddr(std::string filesrc)
{
	char buf[512]={0};
	FILE *fp_src = fopen(filesrc.c_str(), "rt");
	if(!fp_src)
	{
		m_error = ERR_FILE_NOT_FOUND; 
		return m_error;
	}
	u32 iTmpSegAddr = 0;
	tHexPacket hex;
	while(!feof(fp_src))
	{
		//	считываем строку
		memset(&hex, 0, sizeof(hex));
		fgets(buf, sizeof(buf), fp_src);
		if(!strlen(buf))
			break;
		//	пребразуем в нормальный вид
		ParseHexString(buf, hex);
		if(GetLastError() < 0)
		{
			fclose(fp_src);
			return GetLastError();
		}

		//	обрабатываем hex-пакет
		switch(hex.type)
		{
		case RT_EXT_SEG_ADDR:
			if(hex.len != 2)
			{
				m_error = ERR_HEX_STRING;
				fclose(fp_src);
				return GetLastError();
			}
			iTmpSegAddr = ((hex.data[0] << 8) + hex.data[1]) << 4;
			if((iTmpSegAddr < m_iDataAddrStart)&& (m_iDataAddrStart != ADDR_MAX))
				break;
			if((m_iSegAddrMin > iTmpSegAddr) || (m_iSegAddrMin == ADDR_MAX))
				m_iSegAddrMin = iTmpSegAddr;
			if(m_iSegAddrMax < iTmpSegAddr)
				m_iSegAddrMax = iTmpSegAddr;
			break;

		case RT_DATA:
			u32 iTmpAddr;
			int i;
			if(iTmpSegAddr == ADDR_MAX)
				iTmpSegAddr = 0;
			iTmpAddr = iTmpSegAddr + hex.offset;
			if((iTmpAddr < m_iDataAddrStart) && (m_iDataAddrStart != ADDR_MAX))
				break;
			if((m_iDataAddrMin > iTmpAddr) || (m_iDataAddrMin == ADDR_MAX))
				m_iDataAddrMin = iTmpAddr;
			if(m_iDataAddrMax < iTmpAddr + hex.len)
				m_iDataAddrMax = iTmpAddr + hex.len;
			//	проверка пересечения данных по этому адресу
			for(i=iTmpAddr; i<iTmpAddr+hex.len; i++)
			{
				if(m_addr_data.find(i) != m_addr_data.end() && !m_bIgnoreCross)
				{
					m_error = ERR_ADDR_DATA_EXISTS;
					break;
				}
				else
        {
					m_addr_data[i] = 1;
        }
			}
			break;

		case RT_EOF:
			break;
		default:
			m_error = ERR_UNKNOWN_TYPE;
			fclose(fp_src);
			return GetLastError();
		}
	}
	fclose(fp_src);

	//	обрабатываем полученные данные
	//	1. проверка наличия сегментов
	if(m_iSegAddrMin == ADDR_MAX)
		m_iSegAddrMin = m_iSegAddrMax = 0;
	//	2. проверка свободного блока CRC
	if(m_iDataAddrMax - m_iDataAddrMin > m_iMemSize-2)
		m_error = ERR_CRC_BLOCK_BUSY;
	if(m_iDataAddrMax - m_iDataAddrStart > m_iMemSize && m_iDataAddrStart != ADDR_MAX)
		m_error = ERR_CRC_BLOCK_BUSY;
	//	3. проверка за выход за границы памяти
	if(m_iDataAddrMax - m_iDataAddrMin > m_iMemSize)
		m_error = ERR_OUT_OF_BOUNDS;
	if(m_iDataAddrMax - m_iDataAddrStart > m_iMemSize && m_iDataAddrStart != ADDR_MAX)
		m_error = ERR_OUT_OF_BOUNDS;

	if(m_iDataAddrMin > m_iSegAddrMin)
		m_iDataAddrMin = m_iSegAddrMin;

	return GetLastError();

}

//	декодирование hex-строки
int CHexParse::DecodeHexString(char* buf, tHexPacket &hex)
{
	hex.len = Text2Hex(buf[0], buf[1]);
	if(hex.len>16)
	{
		m_error = ERR_HEX_LENGTH;
		return m_error;
	}
	hex.offset = (Text2Hex(buf[2], buf[3]) << 8) + Text2Hex(buf[4], buf[5]);
	hex.type = Text2Hex(buf[6], buf[7]);
	int i=0;
	for(i=0; i<hex.len; i++)
		hex.data[i] = Text2Hex(buf[i*2+8], buf[i*2+9]);
	hex.cs = Text2Hex(buf[i*2+8], buf[i*2+9]);
	
	//	проверяем контрольную сумму на всякий пожарный
	u8 cs = 0;
	u8 tmp_buf[sizeof(tHexPacket)] = {0};
	memset(&tmp_buf, 0, sizeof(tmp_buf));
	memcpy(&tmp_buf, &hex, sizeof(tHexPacket));
	for(i=0; i<sizeof(tHexPacket); i++)
		cs +=  tmp_buf[i];
	if (cs)
		m_error = ERR_HEX_STRING_CS;

	return m_error;
}

//	обработка пакета
int CHexParse::ProcessPacket(tHexPacket &hex)
{
	//	обработка команды
	//	сначала проверим данные:
	if(hex.type == RT_DATA)
	{
		m_iDataAddrCur = hex.offset + m_iSegAddrCur;
	}
	if(m_error < 0)
		return m_error;

	//	теперь обработка
	switch(hex.type)
	{
	//	обычные данные
	case RT_DATA:
		if((m_iDataAddrCur < m_iDataAddrMin) && (m_iDataAddrStart != ADDR_MAX))
			break;
    //  проверим на выход за границы заданной границы
    if((m_iDataAddrCur - m_iDataAddrMin + hex.len) > m_iMemSize)
    {
      m_error = ERR_OUT_OF_BOUNDS;
      break;
    }
      
		memcpy(&m_data[m_iDataAddrCur - m_iDataAddrMin], hex.data, hex.len);
		break;
	//	смена сегмента
	case RT_EXT_SEG_ADDR:
		m_iSegAddrCur = ((hex.data[0] << 8) + hex.data[1]) << 4;
		break;
	//	конец файла
	case RT_EOF:
		m_bEOF = true;
		break;
	//	остальные ситуации не обрабатываем
	default:
		m_error = ERR_UNKNOWN_TYPE;
		break;
	}

	return m_error;
}

//	перевод двух символов в текстовом представлении в символ в бинарном представлении
u8 CHexParse::Text2Hex(char ch1, char ch2)
{
	u8 out = 0;
	//  первая тетрада
	if (ch1 >= '0' && ch1 <= '9')
		out = (ch1 - '0') << 4;
	else if (ch1 >= 'A' && ch1 <= 'F')  
		out = (ch1 - 'A' + 10) << 4;
	else if (ch1 >= 'a' && ch1 <= 'f')
		out = (ch1 - 'a' + 10) << 4;
      
	//  вторая тетрада
	if (ch2 >= '0' && ch2 <= '9')
		out += ch2 - '0';
	else if (ch2 >= 'A' && ch2 <='F')
		out += ch2 - 'A' + 10;
	else if (ch2 >= 'a' && ch2 <= 'f')
		out += ch2 - 'a' + 10;

	return out;
}

//	перевод строки с хексом из текстового представления в бинарное
int CHexParse::Text2Hex(char* szIn, u8* out)
{
	if(strlen(szIn) % 2)
		return ERR_HEX_STRING;
	for(int i=0; i<strlen(szIn)/2; i++)
		out[i] = Text2Hex(szIn[i*2], szIn[i*2+1]);

	return strlen(szIn)/2;
}

//	парсинг HEX-строки
int CHexParse::ParseHexString(IN char* szHexText, tHexPacket &hex)
{
	int err = 0;
	int iLenIn = strlen(szHexText);
	
	//	проверяем наличие двоеточия в начале
	if(szHexText[0] != ':')
	{
		m_error = ERR_HEX_STRING;
		return ERR_HEX_STRING;
	}

	//	вырезаем служебные символы (LF/CF)
	while(1)
	{
		if(szHexText[iLenIn-1] == 10 || szHexText[iLenIn-1] == 13)
		{
			szHexText[iLenIn-1] = 0;
			iLenIn--;
			continue;
		}
		break;
	}

	//	декодим
	DecodeHexString(&szHexText[1], hex);
	if(this->GetLastError() < 0)
		return this->GetLastError();

	return ERR_OK;
}

//	инициализация подсчета CRC16
void CHexParse::InitCRC16(u16 crc_val)
{
	m_iCRC = crc_val;
}

//	подсчет CRC16 для следующего символа
u16 CHexParse::CalcCRC16(u8 symbol)
{
// Update the CRC for transmitted and received data using
// the CCITT 16bit algorithm (X^16 + X^12 + X^5 + 1).
    m_iCRC  = (unsigned char)(m_iCRC >> 8) | (m_iCRC << 8);
    m_iCRC ^= symbol;
    m_iCRC ^= (unsigned char)(m_iCRC & 0xff) >> 4;
    m_iCRC ^= (m_iCRC << 8) << 4;
    m_iCRC ^= ((m_iCRC & 0xff) << 4) << 1;

	return(m_iCRC);
}

//	подсчет CRC16 для строки
void CHexParse::CalcCRC16(u8* buf, int buf_len)
{
	for(int i=0; i<buf_len; i++)
		CalcCRC16(buf[i]);
}

//	подсчет CRC16 для массива
u16 CHexParse::CalcCRC16()
{
	for(int i=0; i<m_iMemSize-2; i++)
		CalcCRC16(m_data[i]);
	return m_iCRC;
}

//	получение CRC16
u16 CHexParse::GetCRC16()
{	return m_iCRC; }

//	процессинг Hex-файла
int CHexParse::ProcessHex(std::string filesrc, std::string filedest)
{
	char buf[1024] = {0};	//	считываемая строка
	
	FindMinMaxAddr(filesrc);
	if(m_error < 0)
		return m_error;
	
	bool bRename = false;
	if(filesrc == filedest)
	{
		bRename = true;
		filedest.insert(0, "#");
	}

	FILE *fp_src = fopen(filesrc.c_str(), "rt");
	FILE *fp_dest = fopen(filedest.c_str(), "wt");
	if(!fp_src)
	{
		m_error = ERR_FILE_NOT_FOUND; 
		return m_error;
	}
	if(!fp_dest)
	{
		m_error = ERR_FILE_COULD_NOT_CREATE;
		return m_error;
	}

	tHexPacket hex;
	while(!feof(fp_src))
	{
		//	считываем строку
		memset(&hex, 0, sizeof(hex));
		fgets(buf, sizeof(buf), fp_src);
		if(!strlen(buf))
			break;
		//	пребразуем в нормальный вид
		ParseHexString(buf, hex);
		ProcessPacket(hex);

		if(GetLastError() < 0)
		{
			fclose(fp_src);
			fclose(fp_dest);
			return GetLastError();
		}

		//	пишем в файл
		if(!m_bEOF)
			fprintf(fp_dest, "%s\n", buf);
		else
		{	//	определили конец hex-файла
			//	1. считаем CRC
			CalcCRC16();
			tHexPacket hex;
			//	тут, возможно, понадобится отдельная строка для случая с памятью >64k
//			if(m_iMemSize > 64*KILO)
			{	//	формируем отдельную строку для записи адреса сегмента
				hex.type = RT_EXT_SEG_ADDR;
				hex.len = 2;
				hex.offset = 0;
				u32 tmp;
				if(m_iMemSize < 64*KILO)
					tmp = 0;
				else
					tmp = m_iMemSize - 64*KILO + m_iSegAddrMin;
				hex.data[0] = (tmp >> 4) >> 8;
				hex.data[1] = (tmp >> 4) & 0xFF;
				CalcHexCS(&hex);
				//	пишем ее в файл
				fprintf(fp_dest, ":%02X%04X%02X%02X%02X%02X\n", 
					hex.len,
					hex.offset,
					hex.type,
					hex.data[0],
					hex.data[1],
					hex.cs);
			}
			//	формируем строку с CRC
			hex.type = RT_DATA;
			hex.len = sizeof(m_iCRC);
			hex.offset = ((m_iMemSize-sizeof(m_iCRC)) % (64*KILO)) & 0xFFFF;
			hex.data[0] = (u8)(m_iCRC >> 8);
			hex.data[1] = (u8)(m_iCRC & 0xFF);
			CalcHexCS(&hex);
			//	пишем ее в файл
			fprintf(fp_dest, ":%02X%04X%02X%02X%02X%02X\n", 
				hex.len,
				hex.offset,
				hex.type,
				hex.data[0],
				hex.data[1],
				hex.cs);
			//	пишем конец hex в файл
			fprintf(fp_dest, "%s\n", buf);

			//	Для отладки
			m_data[m_iMemSize-2] = (u8)(m_iCRC >> 8);;
			m_data[m_iMemSize-1] = (u8)(m_iCRC & 0xFF);;
			break;
		}
	}

	int n = m_addr_data.size();
	fclose(fp_src);
	fclose(fp_dest);

	if(bRename)
		MoveFileEx(filedest.c_str(), filesrc.c_str(), MOVEFILE_REPLACE_EXISTING);

#ifdef _DEBUG
	fp_dest = fopen("log.txt", "wt");
	for(u32 i=0; i<m_iMemSize; i+=16)
	{
		fprintf(fp_dest, "%04X %04X:", (i>>16)&0xFFFF, i&0xFFFF);
		for(u32 j=0; j<16; j++)
			fprintf(fp_dest, " %02X", m_data[i+j]);
		fprintf(fp_dest, "\n");
	}
#endif

	return ERR_OK;
}

//	инициализация переменных
void CHexParse::Init(u32 msize, u32 iStartAddr, bool bIgnoreCross, u8 iByteFill, u16 iCrcVal)
{
	m_iMemSize = msize;
	m_data = new u8[m_iMemSize];
	memset(m_data, iByteFill, m_iMemSize);
	m_iSegAddrCur = 0;
	m_iSegAddrMin = ADDR_MAX;
	m_iSegAddrMax = ADDR_MIN;
	m_iDataAddrCur = 0;
	m_iDataAddrMin = ADDR_MAX;
	m_iDataAddrMax = ADDR_MIN;
	m_bEOF = false;
	m_addr_data.clear();
	m_bIgnoreCross = bIgnoreCross;
	m_iDataAddrStart = iStartAddr;
	InitCRC16(iCrcVal);
}

//	уничтожение массива
void CHexParse::Release()
{
	if(m_data != NULL)
	{
		delete [] m_data;
		m_data = NULL;
	}
}

//	получение последней ошибки
int CHexParse::GetLastError()
{
	return m_error;
}

//	получение размера прошивки
int CHexParse::GetDataSize()
{
	return m_addr_data.size();
}