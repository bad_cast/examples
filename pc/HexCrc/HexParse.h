﻿#pragma once

#include <string>
#include <map>

#define IN
#define OUT


typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;

typedef signed char s8;
typedef signed short s16;
typedef signed int s32;


#define ERR_OK						0
#define ERR_HEX_STRING				-1		//	ошибка в строке
#define ERR_HEX_STRING_CS			-2		//	не совпадает контрольная сумма
#define ERR_FILE_NOT_FOUND			-3		//	не найден файл прошивки
#define ERR_FILE_COULD_NOT_CREATE	-4		//	не могу создать файл с подписанной прошивкой
#define ERR_CRC_BLOCK_BUSY			-5		//	блок памяти для CRC занят
#define ERR_OUT_OF_BOUNDS			-6		//	блок с данными за границами памяти
#define ERR_UNKNOWN_TYPE			-7		//	неизвестная для нас команда
#define ERR_HEX_LENGTH				-8		//	слишком большая длина
#define ERR_ADDR_DATA_EXISTS		-9		//	данные по одному из адресов дублированы
#define ERR_HEX_SEGMENT				-10		//	не определен сегмент для данных

#define KILO		1024

#define HEX_DATA_MAX_LEN	0xFF

#define ADDR_MAX	0xFFFFFFFF
#define ADDR_MIN	0

#pragma pack(push,1)

enum
{
	RT_DATA = 0x00,
	RT_EOF = 0x01,
	RT_EXT_SEG_ADDR = 0x02,
	RT_START_SEG_ADDR = 0x03,
	RT_EXT_LIN_ADDR = 0x04,
	RT_START_LIN_ADDR = 0x05,
};

struct tHexPacket
{
	u8 len;
	u16 offset;
	u8 type;
	u8 data[HEX_DATA_MAX_LEN];
	u8 cs;
	tHexPacket()
	{
		memset(data, 0, sizeof(data));
	}
};

#pragma pack(pop)


class CHexParse
{
public:
	CHexParse();
	~CHexParse();
	int ProcessHex(std::string filedest, std::string filesrc);
	u16 GetCRC16();
	void Init(u32 msize, u32 iStartAddr, bool bIgnoreCross, u8 iByteFill, u16 iCrcVal);
	void Release();
	int GetLastError();
	int GetDataSize();

protected:
//	methods

	//	работа с хексом
	u8 Text2Hex(char char1, char char2);
	int Text2Hex(char* in, u8* out);
	int ParseHexString(IN char* szHexIn, tHexPacket &hex);
	int DecodeHexString(char* buf, tHexPacket &hex);
	bool DetectHexEnd(std::string szHexString);
	int FindMinMaxAddr(std::string filesrc);
	int ProcessPacket(tHexPacket &hex);

	//	подсчет CRC32
	void InitCRC16(u16 crc_val = 0);		//	начальное значение CRC
	u16 CalcCRC16();						//	подсчет CRC для всего массива данных
	u16 CalcCRC16(u8 symbol);				//	подсчет CRC для символа
	void CalcCRC16(u8* buf, int buf_len);	//	подсчет CRC для строки
	
	//	прочее
	u8 CalcHexCS(tHexPacket* hex);

	//	vars
	u16 m_iCRC;
//	tHexString m_iLastHex;
	
	u8* m_data;			//	память под прошивку
	int m_iMemSize;		//	размер памяти, необходимой для хранения прошивки
	u32 m_iSegAddrCur;	//	адрес текущего сегмента
	u32 m_iSegAddrMin;	//	минимальный адрес начала сегмента
	u32 m_iSegAddrMax;	//	максимальный адрес начала сегмента
	u32 m_iDataAddrMin;		//	минимальный адрес в hex-файле
	u32 m_iDataAddrMax;		//	максимальный адрес в hex-файле
	u32 m_iDataAddrStart;	//	начальный адрес, с которого считается CRC (задается)
	u32 m_iDataAddrCur;		//	текущий адрес данных
	int m_error;		//	последняя ошибка
	bool m_bEOF;		//	определение конца файла
	bool m_bIgnoreCross;//	флаг проверки пересечения данных в памяти

	std::map<u32, int> m_addr_data;	//	мапа для определения повторяющихся кусков кода
};