﻿// HexSize.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <string.h>
#include <conio.h>

typedef unsigned char u8;
typedef unsigned short u16;


struct tHexPacket
{
	u8 len;
	u16 offset;
	u8 type;
	u8 data[256];
	u8 cs;
	tHexPacket()
	{
		memset(data, 0, sizeof(data));
	}
};

tHexPacket hex;
int nSize = 0;

u8 Text2Hex(char ch1, char ch2)
{
	u8 out = 0;
	//  первая тетрада
	if (ch1 >= '0' && ch1 <= '9')
		out = (ch1 - '0') << 4;
	else if (ch1 >= 'A' && ch1 <= 'F')  
		out = (ch1 - 'A' + 10) << 4;
	else if (ch1 >= 'a' && ch1 <= 'f')
		out = (ch1 - 'a' + 10) << 4;
      
	//  вторая тетрада
	if (ch2 >= '0' && ch2 <= '9')
		out += ch2 - '0';
	else if (ch2 >= 'A' && ch2 <='F')
		out += ch2 - 'A' + 10;
	else if (ch2 >= 'a' && ch2 <= 'f')
		out += ch2 - 'a' + 10;

	return out;
}


//	декодирование hex-строки
int DecodeHexString(char* buf, tHexPacket &hex)
{
	int err = 0;
	int iLenIn = strlen(buf);
	
	//	проверяем наличие двоеточия в начале
	if(buf[0] != ':')
		return -3;

	//	вырезаем служебные символы (LF/CF)
	while(1)
	{
		if(buf[iLenIn-1] == 10 || buf[iLenIn-1] == 13)
		{
			buf[iLenIn-1] = 0;
			iLenIn--;
			continue;
		}
		break;
	}
	memset(&hex, 0, sizeof(tHexPacket));
	hex.len = Text2Hex(buf[1], buf[2]);
	hex.offset = (Text2Hex(buf[3], buf[4]) << 8) + Text2Hex(buf[5], buf[6]);
	hex.type = Text2Hex(buf[7], buf[8]);
	int i=0;
	for(i=0; i<hex.len; i++)
		hex.data[i] = Text2Hex(buf[i*2+9], buf[i*2+10]);
	hex.cs = Text2Hex(buf[i*2+9], buf[i*2+10]);
	
	//	проверяем контрольную сумму на всякий пожарный
	u8 cs = 0;
	u8 tmp_buf[sizeof(tHexPacket)] = {0};
	memset(&tmp_buf, 0, sizeof(tmp_buf));
	memcpy(&tmp_buf, &hex, sizeof(tHexPacket));
	for(i=0; i<sizeof(tHexPacket); i++)
		cs +=  tmp_buf[i];
	if (cs)
		return -1;

	return 0;
}

int main(int argc, char* argv[])
{
	char buf[512];
	if(argc < 2)
	{
		printf("HexSize v.0.1. Usage: HexSize <filename.hex>\n");
		getch();
		return 0;
	}

	FILE *fp = fopen(argv[1], "rt");
	if(!fp)
	{
		printf("File not found.\n");
		getch();
		return 0;
	}
	while(!feof(fp))
	{
		fgets(buf, 255, fp);
		if (!buf)
			break;
		if (DecodeHexString(buf, hex))
		{
			printf("Error in Hex.\n");
			getch();
			fclose(fp);
			return -1;
		}
		if(hex.type == 0)
			nSize += hex.len;
	}
	fclose(fp);
	if(nSize < 1000)
		printf("Bin-size of \"%s\" is %d bytes", argv[1], nSize);
	else 
		printf("Bin-size of \"%s\" is %d %03d bytes", argv[1], nSize / 1000, nSize % 1000);
	getch();
	return 0;
}
