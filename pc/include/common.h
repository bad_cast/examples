﻿#ifndef _COMMON_H_
#define _COMMON_H_

//  часто используемые макросы
#define ABS(x)            (x < 0) ? -x : x)
#define MIN(x,y)          (x < y ? x : y)
#define MAX(x,y)          (x > y ? x : y)

//  основные типы
typedef unsigned char     u8;
typedef unsigned short    u16;
typedef unsigned int      u32;

typedef signed char       s8;
typedef signed short      s16;
typedef signed int        s32;


//  создание бинарных переменных, примеры:
//  char  i1 = BIN8 (101010);
//  short i2 = BIN16(10110110,11101110);
//  long  i3 = BIN24(10110111,00010111,01000110);
//  long  i4 = BIN32(11101101,01101000,01010010,10111100);  
#define BIN8(x) BIN___(0##x)
#define BIN___(x)                                        \
	(                                                \
	((x / 01ul) % 010)*(2>>1) +                      \
	((x / 010ul) % 010)*(2<<0) +                     \
	((x / 0100ul) % 010)*(2<<1) +                    \
	((x / 01000ul) % 010)*(2<<2) +                   \
	((x / 010000ul) % 010)*(2<<3) +                  \
	((x / 0100000ul) % 010)*(2<<4) +                 \
	((x / 01000000ul) % 010)*(2<<5) +                \
	((x / 010000000ul) % 010)*(2<<6)                 \
	)

#define BIN16(x1,x2) \
    ((BIN(x1)<<8)+BIN(x2))

#define BIN24(x1,x2,x3) \
    ((BIN(x1)<<16)+(BIN(x2)<<8)+BIN(x3))

#define BIN32(x1,x2,x3,x4) \
    ((BIN(x1)<<24)+(BIN(x2)<<16)+(BIN(x3)<<8)+BIN(x4))

#endif
