#include "StdAfx.h"
#include "Options.h"

#pragma warning(disable : 4996)

COptions::COptions(string szFile)
{
	SetFile(szFile);
}


COptions::~COptions(void)
{
}


void COptions::AddOption(string szParam, string szValue, string szParent)
{
	string key = szParent + szParam;
	tOption opt;
	opt.szParent = szParent;
	opt.szParam = szParam;
	opt.szValue = szValue;
	m_options[key] = opt;
}

void COptions::AddOption(string szParam, int iValue, string szParent)
{
	char str[50];
	sprintf_s(str, 49, "%d", iValue);
	string key = szParent + szParam;
	tOption opt;
	opt.szParent = szParent;
	opt.szParam = szParam;
	opt.szValue = str;
	m_options[key] = opt;
}

void COptions::AddOption(string szParam, double dValue, string szParent)
{
	char str[50];
	sprintf_s(str, 49, "%.9f", dValue);
	string key = szParent + szParam;
	tOption opt;
	opt.szParent = szParent;
	opt.szParam = szParam;
	opt.szValue = str;
	m_options[key] = opt;
}

BOOL COptions::DeleteOption(string szParam, string szParent)
{
	string key = szParent + szParam;
	map<string, tOption>::iterator iter = m_options.find(key);
	if(iter != m_options.end())
	{
		if(iter->second.szParent == szParent || szParent == "")
		{
			m_options.erase(iter);
			return TRUE;
		}
	}
	return FALSE;
}

void COptions::EraseAll()
{
	for(map<string, tOption>::iterator iter = m_options.begin(); iter != m_options.end(); /*lalala*/)
		m_options.erase(iter++);
}

int COptions::GetOptions(map<string, tOption> &options)
{
	options = m_options;
	return m_options.size();
}

int COptions::SaveAsBinary()
{
	string path = m_szFile + ".opt";
	FILE *fp = fopen(path.c_str(), "wb");
	if(fp == NULL)
		return -1;

	for(map<string, tOption>::iterator iter = m_options.begin(); iter != m_options.end(); iter++)
	{
		char buffer[T_OPT_SIZE];
		ZeroMemory(buffer, T_OPT_SIZE);
		PackOption(buffer, iter->second);
		fwrite(buffer, T_OPT_SIZE, 1, fp);
	}

	fclose(fp);
	return 0;
}

int COptions::SaveAsText()
{
	string path = m_szFile + ".ini";
	FILE *fp = fopen(path.c_str(), "wt");
	if(fp == NULL)
		return -1;

	map<string, tOption> tmp_options = m_options;
	while(!tmp_options.empty())
	{
		map<string, tOption>::iterator iter = tmp_options.begin();
		tOption opt = iter->second;
		fprintf(fp, "\n[%s]\n%s = %s\n", 
			opt.szParent.c_str(),
			opt.szParam.c_str(),
			opt.szValue.c_str());
		tmp_options.erase(iter);
		for(map<string, tOption>::iterator iter2 = tmp_options.begin(); iter2 != tmp_options.end(); )
		{
			if(opt.szParent == iter2->second.szParent)
			{
				fprintf(fp, "%s = %s\n", 
					iter2->second.szParam.c_str(),
					iter2->second.szValue.c_str());
				tmp_options.erase(iter2++);
			}
			else
				++iter2;
		}
	}

	fclose(fp);
	return 0;
}


int COptions::LoadBinaryFile()
{
	EraseAll();
	string path = m_szFile + ".opt";
	FILE *fp = fopen(path.c_str(), "rb");
	if(fp == NULL)
		return -1;
	
	char buf[T_OPT_SIZE];
  int n = fread(buf, T_OPT_SIZE, 1, fp);
	if(fread(buf, T_OPT_SIZE, 1, fp) == T_OPT_SIZE)
	{
		tOption opt = UnpackOption(buf, sizeof(buf));
		if(opt.szParam != "")
			m_options[opt.szParam] = opt;
	}
	fclose(fp);
	return m_options.size();
}

int COptions::LoadTextFile()
{
	string path = m_szFile + ".ini";
	FILE* fp = fopen(path.c_str(), "rt");
	if(fp == NULL)
		return -1;

	string str;
	string szParent = "DEFAULT";
	char buf[256];
	while(!feof(fp))
	{
		fgets(buf, sizeof(buf), fp);
		str = buf;
		int nPosBegin = str.find('[');
		int nPosEnd = str.find(']');
		if(nPosBegin != -1 && nPosEnd != -1 && nPosEnd > nPosBegin)
		{
			str.replace(nPosEnd, str.length()-nPosEnd, "");
			str.replace(0, nPosBegin+1, "");
			szParent = str;
		}
		else
		{
			string szParam, szValue;
			int n = str.find('=');
			if(n == -1)
				continue;
			szParam = str;
			szParam.replace(n, szParam.length()-n, "");
			szValue = str;
			szValue.replace(0, n+1, "");
			//	������ ������ � szParam �����
			while(1)
			{
				n = szParam.find(' ');
				if(n == -1)
					break;
				szParam.replace(n, 1, "");
			}
			//	������ ������ � szValue � ������
			while(1)
			{
				n = szValue.find(' ');
				if(n == -1 || n > 0)
					break;
				szValue.replace(n, 1, "");
			}
			//	������� 13-� ������
			if((n = szValue.find('\n')) != -1)
				szValue.replace(n, 1, "");
			//	���������� �������� � ����
			tOption opt;
			string key = szParent + szParam;
			opt.szParam = szParam;
			opt.szParent = szParent;
			opt.szValue = szValue;
			m_options[key] = opt;
		}
	}
	fclose(fp);
	return m_options.size();
}

void COptions::PackOption(char *dest, tOption opt)
{
	strcpy_s(dest, 128, opt.szParent.c_str());
	strcpy_s(&dest[128], 128, opt.szParam.c_str());
	strcpy_s(&dest[256], 128, opt.szValue.c_str());
}

tOption COptions::UnpackOption(char *buf, int nSize) 
{
	tOption opt;
	if(nSize < T_OPT_SIZE)
		return opt;
	char tmp[128];
	strcpy_s(tmp, 128, buf);
	opt.szParent = tmp;
	strcpy_s(tmp, 128, &buf[128]);
	opt.szParam = tmp;
	strcpy_s(tmp, 128, &buf[256]);
	opt.szValue = tmp;
	return opt;
}

string COptions::GetStringValue(string szParam, string szParent)
{
	string key = szParent + szParam;
	map<string, tOption>::iterator iter = m_options.find(key);
	if(iter != m_options.end())
		return iter->second.szValue;
	return "";
}

int COptions::GetIntValue(string szParam, string szParent)
{
	string key = szParent + szParam;
	map<string, tOption>::iterator iter = m_options.find(key);
	if(iter != m_options.end())
		return atoi(iter->second.szValue.c_str());
	return 0;
}

double COptions::GetDoubleValue(string szParam, string szParent)
{
	string key = szParent + szParam;
	map<string, tOption>::iterator iter = m_options.find(key);
	if(iter != m_options.end())
		return atof(iter->second.szValue.c_str());
	return 0.0;
}

void COptions::SetFile(string szFile)
{
	m_szFile = szFile;
}