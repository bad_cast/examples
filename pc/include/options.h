/*
 * ����� ��� ������ � �����������
 * �������� �� ��������� ������� ���� �� ��������
 */

#pragma once
#include <map>
#include <string>

#define T_OPT_SIZE	512

using namespace std;

struct tOption
{
	string szParent;
	string szParam;
	string szValue;
	tOption()
	{
		szParent = "DEFAULT";
		szParam = "";
		szValue = "";
	}
};

class COptions
{
public:
	COptions(string szFile = "default");
	~COptions(void);

	void AddOption(string szParam, string szValue, string szParent = "DEFAULT");
	void AddOption(string szParam, int iValue, string szParent = "DEFAULT");
	void AddOption(string szParam, double dValue, string szParent = "DEFAULT");
	int DeleteOption(string szParam, string szParent = "DEFAULT");
	void EraseAll();

	string GetStringValue(string szParam, string szParent = "DEFAULT");
	int GetIntValue(string szParam, string szParent = "DEFAULT");
	double GetDoubleValue(string szParam, string szParent = "DEFAULT");

	int GetOptions(map<string, tOption> &options);
	int LoadBinaryFile();
	int LoadTextFile();
	void SetFile(string szFile);
	int SaveAsBinary();
	int SaveAsText();

protected:
	void PackOption(char *dest, tOption opt);
	tOption UnpackOption(char *buf, int nSize);

	string m_szFile;
	map<string, tOption> m_options;	//	���� - szParam
};
