﻿#include "StdAfx.h"
#include "parsing.h"
#include "stdlib.h"
#include "string.h"
#define StopSymbol '#'


Record::Record(void)
{
	strcpy(Value,"");
	strcpy(Commentary,"");
	return;
}

CParsing::CParsing(void)
{
	Nunits=0;
}

CParsing::~CParsing(void)
{
}

bool CParsing::OpenFile(char *name)
{
	char mas[200];
	int i,k;
	file=fopen(name,"r");
	if (file==NULL)
		return false;

	strcpy(units[0].Name,"UNNAME");
	while( fgets(mas,200,file)!=NULL && Nunits<N)
	{
		if(mas[0]=='[')
		{	
			//Чтение заголовка блока
			i=1;
			while(mas[i]!=']' && mas[i]!='\n' )
			{
				units[Nunits].Name[i-1]=mas[i];
				i++;
			}
			units[Nunits].Name[i-1]='\0';
			units[Nunits].Nrecords=0;
			Nunits++;
		}
		else
		{
			if(mas[0]==' ' || mas[0]==StopSymbol || Nunits==0 || mas[0]=='\n' || mas[0]=='\t')
				continue;

			//Разбор записи

			//Разбор названия параметра
			i=0;
			while (mas[i]!='=' && mas[i]!='\n' && mas[i]!=' '&& mas[i]!='\t')
			{
				units[Nunits-1].records[units[Nunits-1].Nrecords].ParamName[i]=mas[i];
				i++;
			}
			units[Nunits-1].records[units[Nunits-1].Nrecords].ParamName[i]='\0';
		
			if (mas[i]=='\n')
				continue;
			
			//Разбор =
			while (mas[i]!='=' && mas[i]!='\n')
				i++;

			if (mas[i]=='\n')
				continue;

			i++;

			//Разбор параметра
			while (mas[i]==' ' && mas[i]!='\n' && mas[i]=='\t' )
				i++;

			k=0;
			while(mas[i]!='\n' && mas[i]!=StopSymbol )
			{
				units[Nunits-1].records[units[Nunits-1].Nrecords].Value[k]=mas[i];
				i++;
				k++;
			}
			units[Nunits-1].records[units[Nunits-1].Nrecords].Value[k]='\0';
	

			if (mas[i]==StopSymbol)
			{
				i++;
				k=0;
				while(mas[i]!='\n' )
				{
					units[Nunits-1].records[units[Nunits-1].Nrecords].Commentary[k]=mas[i];
					i++;
					k++;
				}
				units[Nunits-1].records[units[Nunits-1].Nrecords].Commentary[k]='\0';
			}
			units[Nunits-1].Nrecords++;
			if (units[Nunits-1].Nrecords>=K)
				units[Nunits-1].Nrecords--;

		}
	}	
	fclose(file);
	return true;
}


void  CParsing::PrintFileInfo()
{
	for(int i=0;i<Nunits;i++)
	{
		printf("UNIT %d [%s]\n",i+1,units[i].Name);
		for(int j=0;j<units[i].Nrecords;j++)
			printf("\trecord %d - %s=%s ; %s\n",j+1,units[i].records[j].ParamName,units[i].records[j].Value,units[i].records[j].Commentary);

	}

}


char* CParsing::GetValue(char* UnitName,char* ParamName)
{int i,j;
	for (i=0;i<Nunits;i++)
	{
		if (strcmp(UnitName,units[i].Name)!=0 && UnitName!=NULL)
			continue;
		for(j=0;j<units[i].Nrecords;j++)
			if (strcmp(ParamName,units[i].records[j].ParamName)==0 )
				return units[i].records[j].Value;
	}
return NULL;
}

bool CParsing::SetValue(char* UnitName,char* ParamName,char *ValueStr) //установка значения
{
int i,j;
	for (i=0;i<Nunits;i++)
	{
		if (strcmp(UnitName,units[i].Name)!=0 && UnitName!=NULL)
			continue;
		for(j=0;j<units[i].Nrecords;j++)
			if (strcmp(ParamName,units[i].records[j].ParamName)==0 )
			{
				strcpy(units[i].records[j].Value,ValueStr);
				return true;
			}
	}
return false;
}


bool CParsing::SaveFile(char *name)
{
	file=fopen(name,"w");
	if (file==NULL)
		return false;

	for(int i=0;i<Nunits;i++)
	{
		fprintf(file,"\n[%s]\n",units[i].Name);
		for(int j=0;j<units[i].Nrecords;j++)
			fprintf(file,"%s = %s; %s\n",units[i].records[j].ParamName,units[i].records[j].Value,units[i].records[j].Commentary);
	}

	return true;

}