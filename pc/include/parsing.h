﻿//Класс CParsing - предназначен для чтения и редактирования INI файлов
//Структура
//	INI-файл:
//			---блок1
//
//			---блок2
//
//			---...
//
//			---блокN:
//						--- [заголовок]
//						--- [запись 1]
//						--- [запись 2]
//						---  ...
//						--- [запись K]
//					
// формат записи:  <параметр> = <значение> ; <комментарий>
//Отступы в начале строки не допускаются!!!
//Количество пустых и закомментированных строк не ограничивается.
//
//
//

#pragma once
#include <stdio.h>
#define N 25  //максимальное количество блоков
#define K 20	//максимальное количество записей в блоке

class Record //Класс для описании записи
{public:
	Record();

	char ParamName[50]; //параметр
	char Commentary[150];//комментарий
	char Value[100]; //значение параметра
};

typedef struct Unit //Структура для описания блока
{
	char Name[100]; //имя блока
	Record records[K]; //массив указателей на записи
	short Nrecords; //количество записей

}Unit;

class CParsing
{
public:
	CParsing(void);
	~CParsing(void);
	bool OpenFile(char *name); //открытие файла и парсинг
	void PrintFileInfo(); //вывод разобранных данных на экран
	char* GetValue(char* UnitName,char* ParamName); //получение значения по имени параметра в заданном блоке ,если UnitName=NULL то в любом блоке
	//возвращает NULL если значение не найдено

	bool SaveFile(char *name);
	bool SetValue(char* UnitName,char* ParamName,char *ValueStr); //установка значения
	//возвращает false если значение не найдено
	

//protected:
	FILE* file;
	Unit units[N];
	short Nunits;
};
