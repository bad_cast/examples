﻿//  PMK.cpp : Defines the entry point for the DLL application.
//  Pc-Mmnet-Kbus (PMK)

#include "stdafx.h"
#include "PMK.h"

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
      break;
		case DLL_THREAD_ATTACH:
      break;
		case DLL_THREAD_DETACH:
      break;
		case DLL_PROCESS_DETACH:
      m_sock.Close();
      WSACleanup();
			break;
    }
    return TRUE;
}

PMK_API int PMKConnect(char *server)
{ 
  wVersionRequested = MAKEWORD( 2, 2 );
  WSAStartup( wVersionRequested, &wsaData );
  if(!m_sock.Connect(server))
    return (FALSE);
  return PMKGetMode(&m_mode);
}

PMK_API void PMKDisconnect()
{ 
  tMMnetPackDisc pack;
  SendAndWait(&pack);
  m_sock.Close(); 
  WSACleanup();
}

PMK_API int PMKIsConnected()
{
  //  сначала смотрим, не пришло ли сообщение о дисконнекте
  if(m_sock.FindServPack(MM_CMD_DISCONNECT, NULL, MM_PACK_DELETE) != -1)
  {
    PMKDisconnect();
    return FALSE;
  }
  
  //  отсылаем сообщение
  tMMnetPackPing pack;
  return SendAndWait(&pack);
}


PMK_API int PMKSetMode(unsigned char mode)
{
  //  очищаем очередь сервисных сообщений
  m_sock.ClearServQueue();
  //  отправляем сообщение
  tMMnetPackSetMode pack(mode);
  int n = SendAndWait(&pack);
  if(n)
    m_mode = mode;
  return (n);
}

PMK_API int PMKBusSend(u8 addr, 
                      u8 cmd,
                      u16 offset, 
                      u8 len,
                      void *data,
                      u8 *inlen,
                      void *indata,
                      u8 maxlen)
{
  if(len > KB_MAX_DATA_SIZE_RAM)
    return FALSE;
  if(m_mode)
    return FALSE;
  if(!inlen)
    return FALSE;
  //  заполняем кабасный пакет
  tKBusPackRAM kbpack(addr, cmd, offset, len, data);
  //  заполняем MMnet-пакет
  tMMnetPackKBus mmpack(&kbpack);
  //  отправляем сообщение
  m_sock.Send((unsigned char*) &mmpack, MM_PACK_SIZE(mmpack.len));
  //  принимаем ответ
  tMMnetPack inpack;
  int cnt = m_timeout / DIVIDER;
  while(cnt--)
  {
    if(m_sock.FindServPack(MM_CMD_KBUS_TRANSMIT, &inpack, MM_PACK_DELETE) != -1)
    {
      *inlen = inpack.len;
      memcpy(indata, inpack.data, MIN(maxlen, inpack.len));
      return (TRUE);
    }
    ::Sleep(DIVIDER);
  }
  *inlen = 0;
  return (FALSE);
}

//  получение первого K-Bus-пакета в очереди
/*PMK_API int PMKBusGetPackFirst(tKBusPackGen *pack)
{ 
  if(!m_sock.GetKBusBufferFirst(*pack))
    return (FALSE);
  return (FALSE);
}*/

PMK_API int PMKBusGetBufferFirst(u8* buffer, u8 maxlen, u32 *ts)
{
  return m_sock.GetKBusBufferFirst(buffer, maxlen, ts);
}

PMK_API int PMKGetKBusQueueSize()
{ return m_sock.GetKBusQueueSize(); }

PMK_API void PMKClearKBusQueue()
{ m_sock.ClearKBusQueue(); }

PMK_API int PMKGetLastError(char* err_text)
{
  if(!err_text)
    return m_sock.GetLastError();
  switch(m_sock.GetLastError())
  {
  case MM_ERR_OK:
    strcpy(err_text, "OK");
    break;
  case MM_ERR_SOCK_ERROR:
    strcpy(err_text, "Socket error");
    break;
  case MM_ERR_SOCK_SERVER:
    strcpy(err_text, "Server error");
    break;
  case MM_ERR_SOCK_CLOSED:
    strcpy(err_text, "Socket closed");
    break;
  case MM_ERR_THREAD:
    strcpy(err_text, "Thread error");
    break;
  }
  return m_sock.GetLastError();
}

PMK_API void PMKSetTimeout(unsigned int timeout)
{ m_timeout = timeout; }

//  ожидание ответа
int WaitForConfirm(unsigned char cmd)
{
  int cnt = m_timeout / DIVIDER;
  //  ждем ответного сообщения с подтверждением
  while(cnt--)
  { //  подтверждение пришло
    if(m_sock.FindServPack(cmd, NULL, MM_PACK_DELETE) != -1)
      return (TRUE);
    //  не пришло, спим
    ::Sleep(DIVIDER);
  }
  return (FALSE);
}

int SendAndWait(tMMnetPack* pack)
{
  //  посылаем данные в сокет
  m_sock.Send((unsigned char*) pack, MM_PACK_SIZE(pack->len));
  //  ожидаем ответа
  return WaitForConfirm(pack->cmd);
}

//  запрос режима
PMK_API int PMKGetMode(u8 *mode)
{
  if(!mode)
    return FALSE;

  tMMnetPackGetMode pack;
  m_sock.Send((unsigned char*)&pack, MM_PACK_SIZE(pack.len));

  int cnt = m_timeout / DIVIDER;
  //  ждем ответного сообщения с подтверждением
  while(cnt--)
  { //  подтверждение пришло
    if(m_sock.FindServPack(pack.cmd, &pack, MM_PACK_DELETE) != -1)
    {
      *mode = pack.data[0];
      return TRUE;
    }
    //  не пришло, спим
    ::Sleep(DIVIDER);
  }
  return FALSE;
}

//  сброс модуля
PMK_API void PMKReset()
{
  tMMnetPackReset pack;
  m_sock.Send((unsigned char*) &pack, MM_PACK_SIZE(pack.len));
}

