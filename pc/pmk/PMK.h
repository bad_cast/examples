﻿// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the PMK_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// PMK_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.

#include "net/mmnet_def.h"
#include "net/kbus_def.h"
#include "net/ClientSocket.h"

#ifdef PMK_EXPORTS
#define PMK_API __declspec(dllexport)
#else
#define PMK_API __declspec(dllimport)
#endif

//  экспортируемые функции
//  connection
PMK_API int   PMKConnect(char* server);
PMK_API void  PMKDisconnect();
PMK_API int   PMKIsConnected();

PMK_API int   PMKSetMode(unsigned char mode);         //  установка режима работы сервера

PMK_API int   PMKBusSend(  u8 addr,   //  посылка пакета в K-BUS (блок.). адрес
                        u8 cmd,        //  команда (чтение или запись в ОЗУ)
                        u16 offset,     //  смещение в памяти ОЗУ
                        u8 len,        //  размер буфера
                        void *buffer,             //  данные для записи в K-Bus / чтения из K-Bus
                        u8 *inlen,
                        void *indata,
                        u8 maxlen);            

//  для режима сниффера
PMK_API int   PMKGetKBusQueueSize();                        //  возвращает количество пакетов в очереди
PMK_API int   PMKBusGetBufferFirst(u8* buffer, u8 maxlen, u32 *ts); //  то же, что и предыдущая функция, но запихивает K-Bus-пакет в buffer
PMK_API void  PMKClearKBusQueue();                          //  очищает очередь входящих сообщений

//PMK_API int   PMKGetServQueueSize();                      //  возвращает количество пакетов в очереди
//PMK_API void  PMKClearServQueue();                        //  очищает очередь входящих сообщений

PMK_API int   PMKGetLastError(char* err_text);
PMK_API void  PMKSetTimeout(u32 timeout);                   //  задает таймаут на ожидание ответа
PMK_API int   PMKGetMode(u8 *mode);

PMK_API void  PMKReset();                                   //  сбрасывает Nut/OS

//  неэкспортируемые функции
int SendAndWait(tMMnetPack* pack);
int WaitForConfirm(unsigned char cmd);

//  переменные и объекты
CClientSocket m_sock;
unsigned int m_timeout = MM_WAIT_TIMEOUT;
u8 m_mode = 0;
WORD wVersionRequested;
WSADATA wsaData;
