﻿#include "stdafx.h"
#include "PMKWrapper.h"

CPMKWrapper::CPMKWrapper() : hLib (NULL)
{
}

CPMKWrapper::~CPMKWrapper()
{
  UnloadLib();
}

bool CPMKWrapper::Init(char* dllpath)
{
  //  выгружаем на всякий случай
  UnloadLib();

  hLib = ::LoadLibrary(dllpath);
  if(hLib == NULL)
    return false;
  Connect = (PROC_CONN)::GetProcAddress(hLib, "PMKConnect");
  Disconnect = (PROC_DISC)::GetProcAddress(hLib, "PMKDisconnect");
  IsConnected = (PROC_PING)::GetProcAddress(hLib, "PMKIsConnected");

  SetTimeout = (PROC_TIMEOUT)::GetProcAddress(hLib, "PMKSetTimeout");
  SetMode = (PROC_SET_MODE)::GetProcAddress(hLib, "PMKSetMode");
  GetMode = (PROC_GET_MODE)::GetProcAddress(hLib, "PMKGetMode");

  KBusSend = (PROC_KBUS_SEND)::GetProcAddress(hLib, "PMKBusSend");
  KBusGetBufferFirst = (PROC_KBUS_GET_BUF_FIRST)::GetProcAddress(hLib, "PMKBusGetBufferFirst");

  Reset = (PROC_RESET)::GetProcAddress(hLib, "PMKReset");
  return true;
}

void CPMKWrapper::UnloadLib()
{
  if(hLib)
  {
    ::FreeLibrary(hLib);
    hLib = NULL;
  }
}