﻿#pragma once

#include "../common/common.h"
#include "kbus_def.h"
#include "mmnet_def.h"

typedef int   (*PROC_CONN)(char* server);
typedef void  (*PROC_DISC)(void);
typedef int   (*PROC_PING)(void);
typedef int   (*PROC_SET_MODE)(u8 mode);
typedef int   (*PROC_GET_MODE)(u8* mode);
typedef void  (*PROC_TIMEOUT)(u32 timeout);
typedef int   (*PROC_KBUS_SEND)(u8 addr, u8 cmd, u16 offset, u8 len, void* data, u8* inlen, void* indata, u8 maxlen);
typedef int   (*PROC_KBUS_GET_BUF_FIRST)(void* data, u8 maxlen, u32 *ts);
typedef void  (*PROC_RESET)(void);

class CPMKWrapper
{
public:
  CPMKWrapper();
  ~CPMKWrapper();

public:
  bool Init(char* dllpath);
//  
//  соединение
  PROC_CONN Connect;
  PROC_DISC Disconnect;
  PROC_PING IsConnected;

//  установка 
  PROC_SET_MODE SetMode;
  PROC_GET_MODE GetMode;
  PROC_KBUS_SEND KBusSend;
  PROC_KBUS_GET_BUF_FIRST KBusGetBufferFirst;
  PROC_TIMEOUT SetTimeout;
  PROC_RESET Reset;
  void UnloadLib();

private:
  HMODULE hLib;
};