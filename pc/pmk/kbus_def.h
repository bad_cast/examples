﻿#ifndef _KBUS_DEF_H_
#define _KBUS_DEF_H_

#include "../common/common.h"

//  K-BUS commands
//  unused currently
#define KB_CMD_RAM_WR_BYTE          001
#define KB_CMD_RAM_WR_DATA          002
#define KB_CMD_EEPROM_WR            003
#define KB_CMD_RAM_RD_BYTE          004
#define KB_CMD_RAM_RD_DATA          005
#define KB_CMD_EEPROM_RD            006

//  in use
#define KB_CMD_RAM_WR_COMMON        7
#define KB_CMD_RAM_RD_COMMON        8

//  unused currently
#define KB_CMD_HARD_RESET           128
#define KB_CMD_SOFT_RESET           129
#define KB_CMD_INIT                 130
#define KB_CMD_START                131
#define KB_CMD_STOP                 132
#define KB_CMD_PAUSE                133

//  прочие дефайны
//  tKBusPackGen
#define KB_MAX_DATA_SIZE            64
#define KB_MAX_DATA_SIZE_RAM        61

#define KB_DEFAULT_ADDRESS          0x00
//  tKBusPackGen
#define KB_HEADER_SIZE_GEN          5
#define KB_PACK_SIZE_F(len_frame)   (len_frame + 3)               //  размер полного пакета, от размера фрейма 
#define KB_DATA_SIZE_F(len_frame)   (len_frame - 2)
//  tKBusPackRAM
#define KB_HEADER_SIZE_RAM          8
#define KB_PACK_SIZE_RAM(len_data)  (len_data + KB_HEADER_SIZE_RAM)   //  размер полного пакета, от размера вложенных данных
#define KB_FRAME_SIZE_RAM(len_data) (len_data + 5)                 //  размер блока данных верхнего уровня

//  позиции соответствующих полей в пакете tKBusPack
#define KB_POS_ADDR                 0
#define KB_POS_LEN_FRAME            1
#define KB_POS_CMD                  2
#define KB_POS_LEN_DATA             3
#define KB_POS_OFFS_LO              4
#define KB_POS_OFFS_HI              5
#define KB_POS_DATA_RAM             6
#define KB_POS_DATA_MAIN            3
#define KB_POS_CRC_LOW_RAM(len_data)    (KB_POS_DATA_RAM + len_data)
#define KB_POS_CRC_HIGH_RAM(len_data)   (KB_POS_CRC_LOW_RAM(len_data) + 1)
#define KB_POS_CRC_LOW_GEN(len_frame)   (KB_POS_DATA_MAIN + KB_DATA_SIZE_F(len_frame))
#define KB_POS_CRC_HIGH_GEN(len_frame)  (KB_POS_CRC_LOW_GEN(len_frame) + 1)

#pragma pack(push,1)

struct tKBusDataRAM
{
  u8 len_data;
  u8 offs_lo;
  u8 offs_hi;
};

//  структура K-Bus-пакета для записи и чтения в/из ROM
struct tKBusPackRAM
{
  u8 addr;            //  адрес модуля в сети
  u8 len_frame;       //  size of data + sizeof(len) + sizeof(cmd)
  u8 cmd;             //  K-BUS-команда
  struct tKBusDataRAM ram_hdr;  //  заголовок для записи в ROM
  u8 data[KB_MAX_DATA_SIZE_RAM];    //  данные
  u8 crc_lo;         //  циклическая сумма (младший байт)
  u8 crc_hi;         //  циклическая сумма (старший байт)

tKBusPackRAM(){};

tKBusPackRAM(u8 _addr, u8 _cmd, u16 _offset=0, u8 _len=0, void* _data=NULL)
{
  addr = _addr;
  if(_cmd == KB_CMD_RAM_WR_COMMON)
    len_frame = KB_FRAME_SIZE_RAM(_len);
  else
    len_frame = 5;
  cmd = _cmd;
  ram_hdr.len_data = _len;
  ram_hdr.offs_lo = _offset & 0xFF;
  ram_hdr.offs_hi = (_offset >> 8) & 0xFF;
  memset(data, 0, sizeof(data));
  if(_len && _data != NULL)
    memcpy(data, _data, _len);
  CalcCRC();
}

//  данные с подзаголовком
/*tKBusPackGen(u8 _addr, u8 _cmd, u16 _offset, u8 _len_data, void* _data)
{
  addr = _addr;
  len_frame = KB_FRAME_SIZE(_len);
  cmd = _cmd;
  data[0] = _len_data;
  data[1] = _offset & 0xFF;
  data[2] = (_offset >> 8) & 0xFF;
  ram_hdr.len_data = _len;
  ram_hdr.offs_lo = _offset & 0xFF;
  ram_hdr.offs_hi = (_offset >> 8) & 0xFF;
  if(_len_data && data != NULL)
    memcpy(&data[3], _data, _len_data);
  CalcCRC();
}*/

//  данные без подзаголовка
/*tKBusPackRAM(u8 _addr, u8 _cmd, u8 _len_data, void* _data)
{
  addr = _addr;
  len_frame = KB_FRAME_SIZE(_len);
  cmd = _cmd;
  //  ram_hdr.len_data = _len;
  //  ram_hdr.offs_lo = _offset & 0xFF;
  //  ram_hdr.offs_hi = (_offset >> 8) & 0xFF;
  if(_len_data && data != NULL)
    memcpy(data, _data, _len_data);
  CalcCRC();
}*/

void CalcCRC(void)
{
  if(KB_DATA_SIZE_F(len_frame) > KB_MAX_DATA_SIZE)
    return;
  u16 crc = addr + len_frame + cmd + ram_hdr.len_data + ram_hdr.offs_lo + ram_hdr.offs_hi;
  for(int i=0; i<(int)(ram_hdr.len_data + sizeof(ram_hdr)); i++)
    crc += data[i];
  crc_lo = crc & 0xFF;
  crc_hi = (crc >> 8) & 0xFF;
}

bool CheckCRC()
{
  if(KB_DATA_SIZE_F(len_frame) > KB_MAX_DATA_SIZE)
    return (false);

  u16 crc = addr + len_frame + cmd + ram_hdr.len_data + ram_hdr.offs_lo + ram_hdr.offs_hi;
  for(int i=0; i<ram_hdr.len_data; i++)
    crc += data[i];

  if(((crc & 0xFF) == crc_lo) && (((crc >> 8) & 0xFF) == crc_hi))
    return (true);

  return false;
}

int GetBuffer(unsigned char* buf)
{
  int i;
  if(KB_DATA_SIZE_F(len_frame) > KB_MAX_DATA_SIZE)
    return 0;
  buf[KB_POS_ADDR] = addr;
  buf[KB_POS_LEN_FRAME] = len_frame;
  buf[KB_POS_CMD] = cmd;
  buf[KB_POS_LEN_DATA] = ram_hdr.len_data;
  buf[KB_POS_OFFS_LO] = ram_hdr.offs_lo;
  buf[KB_POS_OFFS_HI] = ram_hdr.offs_hi;
  for(i=0; i<ram_hdr.len_data; i++)
    buf[KB_POS_DATA_RAM+i] = data[i];
  buf[KB_POS_CRC_LOW_RAM(ram_hdr.len_data)] = crc_lo;
  buf[KB_POS_CRC_HIGH_RAM(ram_hdr.len_data)] = crc_hi;
  return (KB_PACK_SIZE_RAM(ram_hdr.len_data));
}

};  //  end of struct

struct tKBusPackGen
{
  u8 addr;
  u8 len_frame;       //  size of data + sizeof(len) + sizeof(cmd)
  u8 cmd;             //  K-BUS-команда
  u8 data[KB_MAX_DATA_SIZE];    //  данные
  u8 crc_lo;         //  циклическая сумма (младший байт)
  u8 crc_hi;         //  циклическая сумма (старший байт)

tKBusPackGen(u8 _addr=KB_DEFAULT_ADDRESS, u8 _len=2, u8 _cmd=0, u8 *data=NULL)
{
  addr = addr;
  len_frame = _len;
  cmd = _cmd;
  if(_len > 2)
    memcpy(data, data, KB_DATA_SIZE_F(len_frame));
}

int GetBuffer(u8 *buffer)
{
  memcpy(buffer, &addr, KB_POS_DATA_MAIN);
  if((KB_DATA_SIZE_F(len_frame) > KB_MAX_DATA_SIZE) || (len_frame < 2))
    return 0;
  memcpy(&buffer[KB_POS_DATA_MAIN], data, KB_DATA_SIZE_F(len_frame));
  buffer[KB_POS_CRC_LOW_GEN(len_frame)] = crc_lo;
  buffer[KB_POS_CRC_HIGH_GEN(len_frame)] = crc_hi;
  return KB_PACK_SIZE_F(len_frame);
}

};

#pragma pack(pop)

#endif
