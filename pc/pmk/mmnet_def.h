﻿/*  mmnet_def.h
 *  файл определений и структур для обмена между PC и MMnet
 */
#ifndef _MMNET_DEF_H_
#define _MMNET_DEF_H_

#include "../common/common.h"
#include "kbus_def.h"

//  MMnet
#define MM_PROTO_ID                 0xDEADBEEF      //  идентификатор протокола для связи с MMnet (сигнатура)
#define MM_PROTO_ID_BYTE1           (u8)(MM_PROTO_ID & 0xFF)            //  вспомогательные побайтные идентификаторы
#define MM_PROTO_ID_BYTE2           (u8)((MM_PROTO_ID >> 8) & 0xFF)     //  
#define MM_PROTO_ID_BYTE3           (u8)((MM_PROTO_ID >> 16) & 0xFF)    //  
#define MM_PROTO_ID_BYTE4           (u8)((MM_PROTO_ID >> 24) & 0xFF)    //  
#define MM_PROTO_VERSION            1               //  версия протокола

//  MMnet commands
#define MM_CMD_UNKNOWN              0x00            //  неизвестная команда
//  common
#define MM_CMD_PING                 0x01
#define MM_CMD_VERSION              0x02            //  проверка версии
#define MM_CMD_LOGIN                0x03            //  логин (unused)
#define MM_CMD_DISCONNECT           0x04            //  отсоединение
#define MM_CMD_RESET                0x08            //  сброс MMnet-модуля
//  specific
#define MM_CMD_CONFIG               0x10            //  настроить MMnet-модуль
#define MM_CMD_SET_MODE_0           0x11            //  установка режима (0 - обычный, 1 - прослушивание)
#define MM_CMD_SET_MODE_1           0x12            //  установка режима (0 - обычный, 1 - прослушивание)
#define MM_CMD_GET_MODE             0x13            //  запрос режима
//  K-Bus
#define MM_CMD_KBUS_TRANSMIT        0x20            //  передать в сеть k-bus
#define MM_CMD_LISTEN_DATA          0x21            //  данные от MMnet-модуля в режиме прослушивания
//  confirm
#define MM_CMD_CONFIRM              0x80            //  подтверждение команды (старший бит)

//  режимы функционирования MMnet
#define MM_MODE_STANDARD            0x00            //  стандартный режим
#define MM_MODE_LISTEN              0x01            //  прослушивание

//  error codes
#define MM_ERR_OK                   0
#define MM_ERR_SOCK_ERROR           -1
#define MM_ERR_SOCK_SERVER          -2
#define MM_ERR_SOCK_CLOSED          -3
#define MM_ERR_THREAD               -4


//  прочие дефайны
#define MM_WAIT_TIMEOUT            500                    //  таймаут ожидания ответа по умолчанию, мс

#define MM_PACK_NODELETE           0                      //  флаг неудаления пакета из очереди
#define MM_PACK_DELETE             1                      //  флаг удаления пакета из очереди

//  позиции соответствующих полей в пакете tMMnetPacket
#define MM_POS_ID         0
#define MM_POS_CMD        4
#define MM_POS_LEN        5
#define MM_POS_STATUS     6
#define MM_POS_TS         7
#define MM_POS_RESERVED   11
#define MM_POS_CRC        14
#define MM_POS_DATA       16

#define MM_HEADER_SIZE    MM_POS_DATA                       //  длина заголовка tMMnetPacket (все, кроме данных)
#define MM_PACK_SIZE(len) (MM_HEADER_SIZE+len)              //  полный размер пакета
#define MM_MAX_DATA_SIZE  (256-MM_HEADER_SIZE)              //  максимальный размер данных в tMMnetPacket(макс. размер пакета к-бас)
#define MM_MAX_PACK_SIZE  (MM_HEADER_SIZE+MM_MAX_DATA_SIZE) //  максимальный размер пакета


#pragma pack(push, 1)

//  обертка для посылки ее по эзернету
struct tMMnetPack
{
  u32 id;                       //  идентификатор протокола 
  u8  cmd;                      //  MMnet-команда
  u8  len;                      //  длина сообщения K-BUS
  u8  status;                   //  статус сообщения
  u32 ts;                       //  таймштамп
  u8  reserved[3];              //  резерв (под тикет, например)
  u16 crc;                      //  crc16/ccitt
  u8  data[MM_MAX_DATA_SIZE];   //  данные (k-bus-пакет или прочие)

//  конструктор
tMMnetPack()
{
  id = MM_PROTO_ID;
  cmd = MM_CMD_UNKNOWN;
  len = 0;
  status = 0;
  ts = 0;
  crc = 0xFFFF;
  memset(reserved, 0, sizeof(reserved));
  memset(data, 0, sizeof(data));
}
  
//  записывает в char-буфер MMnet-пакет
//  возвращает длину пакета MMnet
int GetBuffer(unsigned char* buffer)
{
  memcpy(buffer, (void*)&id, MM_POS_DATA);
  if(len)
    memcpy(&buffer[MM_POS_DATA], data, len);
  return MM_PACK_SIZE(len);
}

//  подсчитывает CRC16 в пакете
//  возвращает значение CRC16
u16 CalcCRC()
{
  //  сначала пихаем пакет в промежуточный буфер (за исключением CRC)
  u8 *buf = new u8[MM_PACK_SIZE(len)];
  GetBuffer(buf);

  //  теперь вычисляем CRC
  crc = 0xFFFF;
  for(u16 i=0; i<MM_PACK_SIZE(len); i++)
  {
    if(i == MM_POS_CRC || i == MM_POS_CRC+1)
		  continue;
    // the CCITT 16bit algorithm (X^16 + X^12 + X^5 + 1).
    crc  = (unsigned char)(crc >> 8) | (crc << 8);
    crc ^= buf[i];
    crc ^= (unsigned char)(crc & 0xff) >> 4;
    crc ^= (crc << 8) << 4;
    crc ^= ((crc & 0xff) << 4) << 1;
  }
  delete [] buf;
  return crc;
}

//  проверка CRC
u16 CheckCRC()
{
  //  сначала пихаем пакет в промежуточный буфер
  u8 *buf = new u8[MM_PACK_SIZE(len)];
  GetBuffer(buf);

  //  теперь вычисляем CRC
  u16 check_crc = 0xFFFF;
  for(u16 i=0; i<MM_PACK_SIZE(len); i++)
  {
    if(i == MM_POS_CRC || i == MM_POS_CRC+1)
      continue;
    // the CCITT 16bit algorithm (X^16 + X^12 + X^5 + 1).
    check_crc  = (unsigned char)(check_crc >> 8) | (check_crc << 8);
    check_crc ^= buf[i];
    check_crc ^= (unsigned char)(check_crc & 0xff) >> 4;
    check_crc ^= (check_crc << 8) << 4;
    check_crc ^= ((check_crc & 0xff) << 4) << 1;
  }
  delete [] buf;
  return (check_crc - crc); //  если все окей, то должен вернуться ноль
}

//  развернуть из мм-пакета к-бас-пакет
void GetKBusPackRAM(tKBusPackRAM* pack)
{
  //  скопируем все до данных, чтобы узнать длину пакета
  memcpy(pack, data, KB_POS_DATA_RAM);
  //  теперь копируем данные
  memcpy(pack->data, &data[KB_POS_DATA_RAM], pack->ram_hdr.len_data);
  //  теперь crc
  pack->crc_lo = data[KB_POS_CRC_LOW_RAM(pack->ram_hdr.len_data)];
  pack->crc_hi = data[KB_POS_CRC_HIGH_RAM(pack->ram_hdr.len_data)];
}

void GetKBusPackGen(tKBusPackGen *pack)
{
  //  скопируем все до данных, чтобы узнать длину пакета
//  memcpy(pack, data, KB_POS_DATA_GEN);
  //  теперь копируем данные
//  memcpy(pack->data, &data[KB_POS_DATA_GEN], KB_DATA_SIZE_F(pack->len_frame));
  //  теперь crc
//  pack->crc_lo = data[KB_POS_CRC_LOW_GEN(pack->len_frame)];
//  pack->crc_hi = data[KB_POS_CRC_HIGH_GEN(pack->len_frame)];
  memcpy(pack, data, sizeof(tKBusPackGen));
}

};  //  end of struct tMMnetPack

//  прочие структуры 
//  структура для соединения
struct tMMnetPackVersion : tMMnetPack
{
  tMMnetPackVersion()
  {
    cmd = MM_CMD_VERSION;
    len = 1;
    data[0] = MM_PROTO_VERSION;
    crc = CalcCRC();
  }
};

//  структура для дисконнекта
struct tMMnetPackLogin : tMMnetPack
{
  tMMnetPackLogin()
  {
    cmd = MM_CMD_LOGIN;
    //  TODO::::
    crc = CalcCRC();
  }
};

//  структура для установки конфигурации
struct tMMnetPackConfig : tMMnetPack
{
  tMMnetPackConfig()
  {
    cmd = MM_CMD_CONFIG;
    //  TODO:::
    crc = CalcCRC();
  }
};

//  структура для установки режима
struct tMMnetPackSetMode : tMMnetPack
{
  tMMnetPackSetMode(u8 mode)
  {
    if(!mode)
      cmd = MM_CMD_SET_MODE_0;
    else
      cmd = MM_CMD_SET_MODE_1;
    crc = CalcCRC();
  }
};

struct tMMnetPackGetMode : tMMnetPack
{
  tMMnetPackGetMode()
  {
    cmd = MM_CMD_GET_MODE;
    crc = CalcCRC();
  }
};


//  структура для посылки пакета в K-Bus
struct tMMnetPackKBus : tMMnetPack
{
  tMMnetPackKBus(tKBusPackRAM *pack)
  {
    cmd = MM_CMD_KBUS_TRANSMIT;
    len = KB_PACK_SIZE_F(pack->len_frame);
    memcpy(data, pack, KB_POS_DATA_RAM);
    memcpy(&data[KB_POS_DATA_RAM], pack->data, pack->ram_hdr.len_data);
    data[KB_POS_CRC_LOW_GEN(pack->len_frame)] = pack->crc_lo;
    data[KB_POS_CRC_HIGH_GEN(pack->len_frame)] = pack->crc_hi;
    int n = KB_POS_CRC_LOW_GEN(pack->len_frame);
    n = KB_POS_CRC_HIGH_GEN(pack->len_frame);
//    memcpy(data, pack, sizeof(*pack));
    CalcCRC();
  }
};

//  структура для отсылки пинга
struct tMMnetPackPing : tMMnetPack
{
  tMMnetPackPing()
  {
    cmd = MM_CMD_PING;
    len = 0;
    CalcCRC();
  }
};

//  структура для отсылки дисконнекта
struct tMMnetPackDisc : tMMnetPack
{
  tMMnetPackDisc()
  {
    cmd = MM_CMD_DISCONNECT;
    CalcCRC();
  }
};

struct tMMnetPackReset : tMMnetPack
{
  tMMnetPackReset()
  {
    cmd = MM_CMD_RESET;
    CalcCRC();
  }
};

#pragma pack(pop)

#endif
