#!/Python27/python
# coding: utf8

import sys
import os
import re
import socket
import time
from datetime import datetime
from datetime import timedelta

""" Please download sql connector from mysql.com """
import mysql.connector
from mysql.connector import errorcode

db_config = {
    'user': 'root',
    'password': 'pass123',
    'host': '10.10.10.10',
    'database': 'st',
    'port': 3306,
    'charset': 'utf8',
    'collation': 'utf8_general_ci',
    'raise_on_warnings': True,
    'use_unicode': True,
}

def db_connect():
    """ connect to mysql and return a connector """
    try:
        cnx = mysql.connector.connect(**db_config)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
        cnx  = None

    return cnx

def db_disconnect(cnx):
    """ Just disconnect from db """
    cnx.close()

def db_query(cnx, job):
    """ Insert data into tables"""
    cursor = cnx.cursor()
    query = ("INSERT INTO jobs"
            "(job_path,"
            "job_storage_path,"
            "subjob_dir,"
            "server_name,"
            "server_ip,"
            "component,"
            "component_version,"
            "component_build,"
            "layout_tested,"
            "tup,"
            "time,"
            "total,"
            "pass,"
            "fail,"
            "not_run,"
            "status,"
            "comment)"

            "VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)")

    data = (job['job_path'],
            job['job_storage_path'],
            job['subjob_dir'],
            job['server_name'],
            job['server_ip'],
            job['component'],
            job['component_version'],
            job['component_build'],
            job['layout_tested'],
            job['tup'],
            datetime.now(),
            job['total'],
            job['pass'],
            job['fail'],
            job['not_run'],
            job['status'],
            job['comment'])

    try:
        cursor.execute(query, data)
    except mysql.connector.Error as err:
        print query
        print err
    job_id = cursor.lastrowid

    for key, value in job['testsuites'].iteritems():
        testsuite = job['testsuites'][key]
        testsuite['job_id'] = job_id
        query = ("INSERT INTO testsuites"
                "(job_id,"
                "name,"
                "status,"
                "total,"
                "pass,"
                "fail,"
                "not_run,"
                "comment)"

                "VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')".format(
                testsuite['job_id'],
                testsuite['name'],
                testsuite['status'],
                testsuite['total'],
                testsuite['pass'],
                testsuite['fail'],
                testsuite['not_run'],
                testsuite['comment']))
        try:
            cursor.execute(query)
        except mysql.connector.Error as err:
            print query
            print err
        ts_id = cursor.lastrowid

        for key, value in testsuite['testcases'].iteritems():
            testcase = testsuite['testcases'][key]
            testcase['ts_id'] = ts_id
            if testcase['doc'] != '':
                doc_path = os.path.join(job['job_path'], job['subjob_dir'],
                        testsuite['name'], testcase['dir'], testcase['doc'])
                try:
                    f = open(doc_path, "r")
                    testcase['doc_contents'] = f.read().decode('cp1251').encode('utf8')
                    f.close()
                except:
                    print 'Error opening doc-file.'
                    testcase['doc_contents'] = ''
            else:
                testcase['doc_contents'] = ''
            query = ("INSERT INTO testcases"
                    "(ts_id,"
                    "name,"
                    "dir,"
                    "status,"
                    "name_doc,"
                    "name_stderr,"
                    "name_stdout,"
                    "time_start,"
                    "time_end,"
                    "time_exec,"
                    "total,"
                    "pass,"
                    "fail,"
                    "not_run,"
                    "info,"
                    "doc_contents,"
                    "comment)"

                    "VALUES (%(ts_id)s,"
                    "%(name)s,"
                    "%(dir)s,"
                    "%(status)s,"
                    "%(doc)s,"
                    "%(stderr)s,"
                    "%(stdout)s,"
                    "%(time_start)s,"
                    "%(time_end)s,"
                    "%(time_exec)s,"
                    "%(total)s,"
                    "%(pass)s,"
                    "%(fail)s,"
                    "%(not_run)s,"
                    "%(info)s,"
                    "%(doc_contents)s,"
                    "%(comment)s)")
            try:
                cursor.execute(query, testcase)
            except mysql.connector.Error as err:
                print query
                print err

    cnx.commit()
    cursor.close()
    return

def find_files(subjob_path, files):
    """ Find all necessary files

    ts_dirs - testsuites folders
    tc_dirs - testcases folders of each testsuite
    tc_files - files of each testcase

    files - a dictionary, has following structure:
        {testsuite_dir:
            {testcase_dir:
                {'doc': filename1,
                 'stderr': filename2,
                 'stdout': filename3 } ...} ...}
    """

    ts_dirs = {}
    tc_dirs = {}
    # get all testsuites dirs
    ts_dirs = {d for d in os.listdir(subjob_path) if \
                  os.path.isdir(os.path.join(subjob_path, d))}
    # get all tescases dirs
    for ts_dir in ts_dirs:
        ts_path = os.path.join(subjob_path, ts_dir)
        tc_dirs[ts_dir] = [d for d in os.listdir(ts_path) if \
            os.path.isdir(os.path.join(ts_path, d))]
    # get all testcases files
    for ts_dir, dirs in tc_dirs.iteritems():
        tc_dict = {}
        for tc_dir in dirs:
            tc_files = {}
            tc_path = os.path.join(subjob_path, ts_dir, tc_dir)
            all_tc_files = [f for f in os.listdir(tc_path) if \
                os.path.isfile(os.path.join(tc_path, f))]
            for tc_file in all_tc_files:
                if tc_file.lower().endswith('.log') and \
                        tc_file.lower().find("stderr") != -1:
                    tc_files['stderr'] = tc_file
                    continue
                if tc_file.lower().endswith('.log') and \
                        tc_file.lower().find("stdout") != -1:
                    tc_files['stdout'] = tc_file
                    continue
                if tc_file.lower().endswith('.doc'):
                    tc_files['doc'] = tc_file
                    continue
                if tc_file.lower().endswith('.gif'):
                    tc_files['gif'] = tc_file
                    continue
            tc_dict[tc_dir] = tc_files
        files[ts_dir] = tc_dict
    return

def parse_doc(testcase, tc_path):
    """ Parse doc file for testcase

    testcase - dict that has almost the same structure, as corresponding
    table in sql

    """
    actions = []        # action list
    start_dt = datetime(2100,12,31)
    end_dt = datetime(1900,1,1)
    testcase['status'] = 'not_run'
    testcase['comment'] = ''

    if testcase['doc'] != '':
        ## open doc file
        doc_path = os.path.join(tc_path, testcase['doc'])
        f = open(doc_path, "r")
        ## first line - tc name
        testcase['name'] = f.readline().rstrip('\n')[5:]
        ## second line - tc parameters TODO?
        tc_parameters = f.readline()
        ## tc status is 'passed' by default, if doc exists
        testcase['status'] = 'pass'

        for line in f:
            line = line.rstrip('\n')
            if line.lower().find('***fail***') != -1:
                testcase['status'] = 'fail'
                action_status = 'fail'
            status = line.split('\t')
            if len(status) == 3:
                ## second column - action status
                if status[1].lower() == 'pass':
                    action_status = 'pass'
                    if status[0] != testcase['name']:    ## exclude last line in file
                        testcase['pass'] += 1
                ## if action fails, then testcase fails too
                elif status[1].lower() == 'fail':
                    testcase['status'] = 'fail'
                    action_status = 'fail'
                    testcase['fail'] += 1
                elif status[1].lower() == 'info':
                    action_status = 'info'
                    testcase['info'] += 1
                else:
                    action_status = 'unknown'
                if action_status != 'unknown':
                    ## datetime convert
                    try:
                        dt = datetime.strptime(status[2], '%H:%M:%S %m.%d.%Y')
                    except:
                        continue
                    if start_dt > dt:
                        start_dt = dt
                    if end_dt < dt:
                        end_dt = dt
                    action_dt = '{:%Y%m%d%H%M%S}'.format(dt)
                    actions.append([status[0], action_status, action_dt])
                else:
                    for sts in status:
                        if sts != '':
                            actions.append([sts])
            else:
                for sts in status:
                    if sts != '':
                        actions.append([sts])
        f.close()

    testcase['total'] = testcase['pass'] + testcase['fail'] + testcase['not_run']

    if start_dt.year != 2100:
        ## if tc start and tc end time are defined, calculate diff
        testcase['time_start'] = '{:%Y%m%d%H%M%S}'.format(start_dt)
        testcase['time_end'] = '{:%Y%m%d%H%M%S}'.format(end_dt)
        testcase['time_exec'] = int((end_dt - start_dt).total_seconds())
    else:
        if testcase['stderr'] != '' :
            stderr_path = os.path.join(tc_path, testcase['stderr'])
            dt = datetime.fromtimestamp(os.path.getmtime(stderr_path))
            testcase['time_start'] = '{:%Y%m%d%H%M%S}'.format(dt)
            testcase['time_end'] = testcase['time_start']
        else:
            testcase['time_start'] = '{:%Y%m%d%H%M%S}'.format(start_dt)
            testcase['time_end'] = '{:%Y%m%d%H%M%S}'.format(end_dt)

##    testcase['actions'] = actions
    return

def get_parameter(s, name):
    """ Get parameter from command line """
    for arg in s:
        key_value = arg.split('=')
        if len(key_value) >= 2 and key_value[0] == name:
            return key_value[1]
    return None

def main():
    """ Main  """
    ## get parameters from command line arguments
    job = {}
    subjob_path = get_parameter(sys.argv, '--job_path')
    if subjob_path is not None:
        subjob_path = os.path.normpath(subjob_path)
        job['job_path'] = os.path.split(subjob_path)[0]
        job['subjob_dir'] = os.path.split(subjob_path)[1]
    job['job_storage_path'] = get_parameter(sys.argv, '--job_storage_path')
    if job['job_storage_path'] is not None and job['job_storage_path'] != '':
        job['job_storage_path'] = os.path.normpath(job['job_storage_path'])
    job['server_name'] = socket.gethostname()
    job['server_ip'] = socket.gethostbyname(socket.gethostname())
    job['component'] = get_parameter(sys.argv, '--component')
    job['component_version'] = get_parameter(sys.argv, '--component_version')
    job['component_build'] = get_parameter(sys.argv, '--component_build')
    job['layout_tested'] = get_parameter(sys.argv, '--layout_tested')
    job['tup'] = get_parameter(sys.argv, '--tup')
    if job['tup'] is not None:
        job['tup'] = os.path.normpath(job['tup'])
    job['time'] = '{:%Y%m%d%H%M%S}'.format(datetime.now())
    job['total'] = 0
    job['pass'] = 0
    job['fail'] = 0
    job['not_run'] = 0
    job['status'] = 0
    job['comment'] = ''
    job['testsuites'] = {}
    for key, value in job.iteritems():
        if job[key] is None:
            print 'More parameters needed.\n' \
                    'Syntax: sting_db.py --job_path= ' \
                    '--component= --component_version= --component_build=' \
                    '--layout_tested= --tup= --job_storage_path='
            return
    if 'subjob_dir' not in job or 'job_path' not in job:
        print 'Dirs for job_path or subjob_dir are not set or found.'
        return

    ## iterate through subdirs, find doc and log files
    files = {}
    find_files(os.path.join(job['job_path'], job['subjob_dir']), files)
    job_path = job['job_path']
    subjob_path = os.path.join(job_path, job['subjob_dir'])

    ## testsuites, key is dir of testuite
    for ts_dir, tc_dirs in files.iteritems():
        job['testsuites'][ts_dir] = {'name': ts_dir,
                                        'status': 'not_run',
                                        'total': 0,
                                        'pass': 0,
                                        'fail': 0,
                                        'not_run': 0,
                                        'comment': '',
                                        'testcases': {}}
        testsuite = job['testsuites'][ts_dir]
        for tc_dir, tc_files in tc_dirs.iteritems():
            tc_path = os.path.join(subjob_path, ts_dir, tc_dir)
            if 'doc' not in tc_files.keys():
                testsuite['not_run'] += 1
                tc_doc = ''
            else:
                tc_doc = tc_files['doc']
            if 'stderr' not in tc_files.keys():
                tc_stderr = ''
            else:
                tc_stderr = tc_files['stderr']
            if 'stdout' not in tc_files.keys():
                tc_stdout = ''
            else:
                tc_stdout = tc_files['stdout']

            testsuite['testcases'][tc_dir] = {
                            'name': tc_dir,
                            'dir': tc_dir,
                            'status': 'not_run',
                            'doc': tc_doc,
                            'stderr': tc_stderr,
                            'stdout': tc_stdout,
                            'time_start': '19000101000000',
                            'time_end': '21001231235959',
                            'time_exec': 0,
                            'total': 0,
                            'pass': 0,
                            'fail': 0,
                            'not_run': 0,
                            'info': 0,
                            'comment':'' }

            testcase = testsuite['testcases'][tc_dir]

            ## parse doc file
            parse_doc(testcase, tc_path)
            if testcase['status'] == 'fail':
                testsuite['status'] = 'fail'
                testsuite['fail'] += 1
            if testcase['status'] == 'pass':
                testsuite['pass'] += 1
                if testsuite['status'] != 'fail':
                    testsuite['status'] = 'pass'
            testsuite['total'] += 1

        job['total'] += testsuite['total']
        job['pass'] += testsuite['pass']
        job['fail'] += testsuite['fail']
        job['not_run'] += testsuite['not_run']

    if job['fail'] > 0:
        job['status'] = 'fail'
    elif job['pass'] == job['total']:
        job['status'] = 'pass'
    elif job['not_run'] == job['total']:
        job['status'] = 'not_run'
    else:
        job['status'] = 'fail'
    if job['total'] == 0:
        job['status'] = 'not_run'

    ## db operations
    cnx = db_connect()
    if cnx is None:
        print "Can't connect to database.\n"
        return -2
    db_query(cnx, job)
    db_disconnect(cnx)
    return 0

if __name__ == "__main__":
    main()
