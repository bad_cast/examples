#!/usr/bin/env python
#coding: utf8

import xml.dom.minidom
import os

FILE_INCLUDE_NAME = "mc9s08sd8"

suff = ["Vreset","Vswi","Virq","Vlvw","Vovw","Vmcpwmf","Vmcpwm","Vpwt0rdy",
            "Vpwt0ovf","Vpwt1rdy","Vpwt1ovf","Vadc0","Vadc1","GDU","reserve1",
            "reserve2","Vtpmch0","Vtpmch1","Vtpmovf","Vacmp","Vgducmp0",
            "Vgducmp1","Vgducmp2","Vscierr","Vscirx","Vscitx","Vpdb","Vics",
            "Vmtim","Vkbi","Viic","Vnvm"]
vect = [0xfffe,0xfffc,0xfffa,0xfff8,0xfff6,0xfff4,0xfff2,0xfff0,0xffee,
            0xffec,0xffea,0xffe8,0xffe6,0xffe4,0xffe2,0xffe0,0xffde,0xffdc,0xffda,
            0xffd8,0xffd6,0xffd4,0xffd2,0xffd0,0xffce,0xffcc,0xffca,0xffc8,0xffc6,
            0xffc4,0xffc2,0xffc0]

class FileMem:
    """
        representation of the mem file
    """
    def __init__(self, f_name):
        self.name = f_name
        self.modules = []

    def add_modules(self, modules):
        self.modules = modules

    def __sort(self):
        for module in self.modules:
            module.sort_asc()
        self.modules = sorted(self.modules, key=lambda x: x.regs[0].adr)

    def __get_str(self):
        return "good!\n"

    def create_file(self):
        self.__sort()
        f = open(self.name, "a")
        str = self.__get_str()
        f.write(str)
        f.close()


class Register:
    """
        representation of an any register
    """
    def __init__(self, reg_name, reg_adr, reg_fullname, reg_access):
        self.name = reg_name
        self.adr = reg_adr
        self.fullname = reg_fullname
        self.access = reg_access


ModulesList = [] # This is a global list of modules

def insert_vector_number_h():
    """
        creating of the vector number for .h
    """
    out_str = "/**************** interrupt vector numbers ****************/\n"
    general_part = "#define VectorNumber_"
    for i in range(32):
        out_str += "%-30s %dU\n" % (general_part+suff[i], i)
    return out_str+"\n"


def insert_vector_table_h():
    """
        creating of the vector table for .h
    """
    out_str = "/**************** interrupt vector table ****************/\n"
    for i in range(32):
        out_str += "%-30s 0x%04XU\n" % ("#define "+suff[i], vect[i])
    return out_str+"\n"


def insert_vector_table_inc():
    """
        creating of the vector table for .inc
    """
    out_str = ";**************** interrupt vector table ****************\n"
    for i in range(32):
        out_str += "%-20s %-10s $%08X\n" % (suff[i]+":", "equ" , vect[i])
    return out_str+"\n"


def insert_memory_map_inc():
    """
        creating of the vector table for .inc
    """

    out_str = ";*************Memory Map************************\n"
    mm = {"Dreg_b":0,"Dreg_e":0x7f,"RAMStart":0x80,"RAMEnd":0x17f,"Hreg_b":0x1800,"Hreg_e":0x18ff,"ROMStart":0xe000,"ROMEnd":0xffff}

    for key, val in mm.items():
        out_str += "%-20s %-10s $%08X\n" % (key+":", "equ", val)
    return out_str+"\n"


def insert_prefix_inc():
    """
        creating of the start string fro the .inc
    """
    out_string = """; ###################################################################
;         This header implements the mapping of I/O devices.
;
;     Copyright : 1997 - 2014 Freescale Semiconductor, Inc. All Rights Reserved.
;
;     http      : www.freescale.com
;     mail      : support@freescale.com
; ###################################################################
"""
    return out_string+"\n"


def insert_prefix_c():
    """
        creating of the start strings for the file_name.c
    """
    out_string = """/* DataSheet : MC9S08SD8RM Rev. 0 5/2014 */
"""
    return out_string+"\n"


def insert_prefix_h(file_name):
    """
        creating of the start strings for the file_name.h
    """

    out_string = """/*
** ###################################################################
**     Filename  : mc9s08sd8.h
**     Processor : MC9S08SD8
**     DataSheet : MC9S08SD8RM Rev. 0 5/2014
**     Compiler  : CodeWarrior compiler
**     Date/Time : 4.05.2014, 12:43
**     Abstract  :
**         This header implements the mapping of I/O devices.
**
**     Copyright : 1997 - 2014 Freescale Semiconductor, Inc. All Rights Reserved.
**
**     http      : www.freescale.com
**     mail      : support@freescale.com
** ###################################################################
*/

"""

    out_string += "#ifndef _%s_H\n#define _%s_H\n\n" % (file_name.upper(), file_name.upper())
    out_string +="""
/*lint -save  -e950 -esym(960,18.4) -e46 -esym(961,19.7) Disable MISRA rule (1.1,18.4,6.4,19.7) checking. */
/* Types definition */
typedef unsigned char byte;
typedef unsigned int word;
typedef unsigned long dword;
typedef unsigned long dlong[2];

/* Watchdog reset macro */
#ifndef __RESET_WATCHDOG
#ifdef _lint
  #define __RESET_WATCHDOG()  /* empty */
#else
  #define __RESET_WATCHDOG() (void)(SIM_SRS = 0x55U, SIM_SRS = 0xAAU)
#endif
#endif /* __RESET_WATCHDOG */

#define REG_BASE 0x0000                /* Base address for the I/O register block */


#pragma MESSAGE DISABLE C1106 /* WARNING C1106: Non-standard bitfield type */
"""
    return out_string+"\n"


def take_txt_value(node):
    """
    to take nodeValue from the first TEXT_NODE child
    """
    if node.nodeType == node.TEXT_NODE:
        if node.nodeValue.strip():
            return node.nodeValue.strip()
        else:
            return ""
    else:
        for el in node.childNodes:
            txt = take_txt_value(el)
            if txt != "":
                return txt
    return "DDDD"


def get_reg_fields(bitFields):
    """
        finding the information about register fields
    """
    l_fields=[]

    for f in bitFields:
        fields = {"name":'', "width":'', "offset":''};
        fields["name"] = take_txt_value(f.getElementsByTagName('bitFieldName')[0])
        if "[" in fields["name"]:
            fields["name"] = fields["name"][0:fields["name"].find("[")]
            if fields["name"][0].isdigit(): fields["name"] = "_"+fields["name"]

        fields["width"] = f.getElementsByTagName('bitWidth')[0].firstChild.nodeValue
        fields["offset"] = f.getElementsByTagName('bitOffset')[0].firstChild.nodeValue
        l_fields.append(fields)

    l_fields = sorted(l_fields, key=lambda x: x["offset"]) #sorting according to offset

    return l_fields


def get_reg_info(onereg):
    """
        finding the information about a register
    """
    reg_info = [] # 0: abs adress, 1: reg name, 2: reg size, 3: reg fields [{'name', 'width', 'offset'},...], 4: reg fullName, 5: reg access

    reg_info.append(int(onereg.getAttribute("registerAbsoluteAddrAsHex"), 16))  #absolute address
    reg_info.append(take_txt_value(onereg.getElementsByTagName('registerName')[0])) #register name
    reg_info.append(onereg.getElementsByTagName('registerSize')[0].firstChild.nodeValue) #register size
    reg_info.append(get_reg_fields(onereg.getElementsByTagName('bitField'))) #register fields
    reg_info.append(take_txt_value(onereg.getElementsByTagName('registerNameFull')[0]).replace('\n','')) #register full name
    reg_info.append(take_txt_value(onereg.getElementsByTagName('registerAccess')[0])) # take register access

    print "\n------------------------------register %s-----------------------------" % reg_info[1]

    print "register %s: information getting........finished" % reg_info[1]

    return reg_info


def get_str_info_h(reg_info, module_name):
    """
        generating the information for the .h file
    """
    b_size="byte"
    b_name="Byte"

    #adding the register description
    out_str = '/*** %s %s; 0x%08X ***/\n' % (module_name+reg_info[1], reg_info[4], reg_info[0])

    #starting the union
    out_str += "typedef union {"+"\n"

    #creating the size of the register
    if reg_info[2] == '8':
        pass
    elif reg_info[2] == '16':
        b_size = "word"
        b_name = "Word"
    elif reg_info[2] == '32':
        b_size = "long"
        b_name = "Long"
    else:
        b_size = "unknwn"
        b_name = "Unknwn"

    out_str += "  "+b_size+" "+b_name+";"+"\n"

    if(reg_info[3][0]["width"] != reg_info[2]):
        bit_fields_visible = True
    else:
        bit_fields_visible = False

    #creating of the bit struct if the count of the bit fields are more than base size
    if(bit_fields_visible):
        out_str += "  struct {"+"\n"
        for field in reg_info[3]:
            s = '    %-5s %-10s : %s;'%(b_size, field["name"] if field["name"] != "Reserved" else "", field["width"])
            out_str += s+"\n"
        out_str += "  } Bits;"+"\n"

    #closing of the union
    union_name_str = module_name+reg_info[1]+"STR"
    out_str += "} "+union_name_str+";"+"\n"

    #creating the var line
    var_name_str = "_"+module_name+reg_info[1]
    out_str += 'extern volatile %s %s @0x%08X;\n' % (union_name_str, var_name_str, reg_info[0])
    out_str += '#define %-25s %s\n' % (module_name+reg_info[1], var_name_str+"."+b_name)

    if bit_fields_visible:
        #cretaing defines for fields
        for field in reg_info[3]:
            if (field["name"] != "Reserved"):
                s = '#define %-25s %s' % (module_name+reg_info[1]+"_"+field["name"], var_name_str+".Bits"+"."+field["name"])
                out_str += s+"\n"

        #creating defines for masks
        out_str += "\n"
        for field in reg_info[3]:
            if (field["name"] != "Reserved"):
                mask = int(reg_info[2])-int(field["width"])
                mask = 0xff>>mask   #this decision is only for byte
                mask = mask << int(field["offset"])
                out_str += '#define %-25s %s\n' % (module_name+reg_info[1]+"_"+field["name"]+"_MASK", str(mask)+"U")
                out_str += '#define %-25s %s\n' % (module_name+reg_info[1]+"_"+field["name"]+"_BITNUM", field["offset"])

    print "register %s: string construction for the *.h........finished" % reg_info[1]

    return out_str+"\n\n"


def get_str_info_c(reg_info, module_name, f_name):
    """
    generating the information for the .c file
    """

    if (hasattr(get_str_info_c, 'singl')==False):
        out_str = "#include <%s>\n\n\n" % (f_name)
        get_str_info_c.singl = True
        out_str += "volatile %-25s %-20s %s\n" % (module_name+reg_info[1]+"STR", "_"+module_name+reg_info[1]+";", "/*"+reg_info[4]+"*/")
        print "register %s: string construction for the *.c........finished" % reg_info[1]
        return out_str
    else:
        print "register %s: string construction for the *.c........finished" % reg_info[1]
        return "volatile %-25s %-20s %s\n" % (module_name+reg_info[1]+"STR", "_"+module_name+reg_info[1]+";", "/*"+reg_info[4]+"*/")


def get_str_info_inc(reg_info, module_name):
    """
    generating the information for the .inc file
    """
    reg_name_str = module_name+reg_info[1]

    #adding the register description
    out_str = ';*** %s %s; 0x%08X ***\n' % (reg_name_str, reg_info[4], reg_info[0])
    out_str += "%-30s %-10s $%08X\n" % (reg_name_str+":", "equ", reg_info[0])

    #adding of the bit field information if the count of the fields is more than one
    if(reg_info[3][0]["width"] != reg_info[2]):
        out_str += "; bit numbers for usage in BCLR, BSET, BRCLR and BRSET\n"
        for field in reg_info[3]:
            if (field["name"] != "Reserved"):
                if (field["width"]=="1"): # field consists from one bit
                    out_str += "%-30s %-10s %s\n" % (reg_name_str+"_"+field["name"]+":", "equ", field["offset"])
                else:
                    for delta in range(int(field["width"])): # field consists from more than one bit
                        out_str += "%-30s %-10s %d\n" % (reg_name_str+"_"+field["name"]+str(delta)+":", "equ", int(field["offset"])+delta)

        out_str += "; bit position masks\n"

        null = "00000000"

        for field in reg_info[3]:
            if (field["name"] != "Reserved"):
                if (field["width"]=="1"):
                    bt = bin(1<<int(field["offset"]))[2:]
                    mask = null[0:-len(bt)]+bt
                    out_str += "%-30s %-10s %%%s\n" % ("m"+reg_name_str+"_"+field["name"]+":", "equ", mask)
                else:
                    for delta in range(int(field["width"])): # field consists from more than one bit
                        bt = bin(1<<(int(field["offset"])+delta))[2:]
                        mask = null[0:-len(bt)]+bt
                        out_str += "%-30s %-10s %%%s\n" % ("m"+reg_name_str+"_"+field["name"]+str(delta)+":", "equ", mask)

    print "register %s: string construction for the *.inc........finished" % reg_info[1]
    return out_str+"\n\n"


def out_file(str, f_name):
    """
        put out the information to the f_name file
    """
    f = open(f_name, "a")
    f.write(str)
    f.close()
    print "put out to the %s........finished" % f_name


def parse_file(f_name):
    """
    generating the .h, .c, .inc code for the f_name xml file
    """
    dom = xml.dom.minidom.parse(f_name)
    dom.normalize()

    module_name = take_txt_value(dom.getElementsByTagName('componentName')[0])
    registers = dom.getElementsByTagName('register')

    ModulesList.append(Module(module_name))

    # output data about registers
    for onereg in registers:
        reg_info = get_reg_info(onereg) #get register information
        ModulesList[len(ModulesList)-1].add_reg(Register(reg_info[1], reg_info[0], reg_info[4], reg_info[5]))

        temp_module_name = module_name+"_"
        if (module_name in reg_info[1]):
            temp_module_name = ""

        str_info = get_str_info_h(reg_info, temp_module_name) #get string information for the .h
        out_file(str_info, FILE_INCLUDE_NAME+".h") #put out the information

        str_info = get_str_info_c(reg_info, temp_module_name, FILE_INCLUDE_NAME+".h") #get string information for the .c
        out_file(str_info, FILE_INCLUDE_NAME+".c") #put out the information

        str_info = get_str_info_inc(reg_info, temp_module_name) #get string information for the .inc
        out_file(str_info, FILE_INCLUDE_NAME+".inc") #put out the information

#----------------------------------------------------main-----------------------------------------------------


def main():
    """
    the main function
    """
    # getting the list of files
    files = os.listdir("./")
    files = filter(lambda x: x.endswith('.xml'), files);

    # output normal data
    out_file(insert_prefix_h(FILE_INCLUDE_NAME), FILE_INCLUDE_NAME+".h") #general information for the .h
    out_file(insert_vector_number_h(), FILE_INCLUDE_NAME+".h")  #vector number strings for the .h
    out_file(insert_vector_table_h(), FILE_INCLUDE_NAME+".h")   #vector table address for the .h

    out_file(insert_prefix_c(), FILE_INCLUDE_NAME+".c") #general information for the .c

    out_file(insert_prefix_inc(), FILE_INCLUDE_NAME+".inc") #general information for the .inc
    out_file(insert_vector_table_inc(), FILE_INCLUDE_NAME+".inc")   #vector table adress for the .inc
    out_file(insert_memory_map_inc(), FILE_INCLUDE_NAME+".inc") #memory map address for the .inc

    # output register information
    for file_name in files:
        print "--------------------------parsing the %s-----------------------------\n" % file_name
        parse_file(file_name)
        print "--------------------------%s finished--------------------------------\n\n" % file_name

    out_file("#endif\n", FILE_INCLUDE_NAME+".h")

    fmem = FileMem(FILE_INCLUDE_NAME+".mem")
    fmem.add_modules(ModulesList)
    fmem.create_file()

    print "------------------------generating is over------------------------------\n"


if __name__ == '__main__':

    if os.path.exists(FILE_INCLUDE_NAME+".h"):
        os.remove(FILE_INCLUDE_NAME+".h")
        print "old %s was deleted\n" % (FILE_INCLUDE_NAME+".h")

    if os.path.exists(FILE_INCLUDE_NAME+".c"):
        os.remove(FILE_INCLUDE_NAME+".c")
        print "old %s was deleted\n" % (FILE_INCLUDE_NAME+".c")

    if os.path.exists(FILE_INCLUDE_NAME+".inc"):
        os.remove(FILE_INCLUDE_NAME.upper()+".inc")
        print "old %s was deleted\n" % (FILE_INCLUDE_NAME.upper()+".inc")

    main()


