#!/usr/bin/env python
#coding: utf8

# Registers files generator for S08 architecture
# ver. 0.21:
# - added range support for Values fields
# ver. 0.2:
# - added command line support
# - DSC and S08 support
# ver. 0.11:
# - fixed descriptions for registers and bitfields (all tags are cut from description)
# ver. 0.1:
# - initial release, made for SD8 (hardcoded)

RG_VERSION = "0.21"
REGISTERS_VERSION = "0.1"
PUT_VERSION = 1


import elementtree.ElementTree as ET
from elementtree.ElementTree import Element, SubElement, parse
import binascii
import os
import shutil
import re
import string
import sys
import xml.sax.saxutils as saxutils

### Format xml
def indent(elem, level=0):
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i

### Make Register Layout ###
def make_registers(filename, registers_dir, details_dir, architecture):
    # parse file
    print "Processing " + filename + "..."
    tree = parse(filename)

    # build a tree
    register_collection = Element("register-collection")
#    if PUT_VERSION != 0:
#        version = SubElement(register_collection, "version")
#        version.text = REGISTERS_VERSION
    collection_name = SubElement(register_collection, "name")
    register_sets = SubElement(register_collection, "register-sets")
    register_set_descriptor = SubElement(register_sets, "register-set-descriptor")
    register_set_name = SubElement(register_set_descriptor, "name")
    registers = SubElement(register_set_descriptor, "registers")

    if tree.findtext("componentName/ph"):
        component_name = tree.findtext("componentName/ph")
    else:
        component_name = tree.findtext("componentName")

    collection_name.text = component_name + "_Layout"
    if tree.findtext("componentBriefDescription/ph"):
        register_set_name.text = tree.findtext("componentBriefDescription/ph") + " (" + component_name + ")"
    else:
        register_set_name.text = tree.findtext("componentBriefDescription") + " (" + component_name + ")"

    # fill registers description into the register_collection
    for reg in tree.getiterator("register"):
        # create register structure
        register = SubElement(registers, "register")

        #name
        name = SubElement(register, "name")
        if reg.findtext("registerName/ph"):
            name.text = reg.findtext("registerName/ph")
        else:
            name.text = reg.findtext("registerName")
        name.text = name.text.replace("\n","")
        name.text = name.text.replace("\r","")
        name.text = name.text.replace(" ","")
        if name.text.find(component_name + "_") == -1:
            name.text = component_name + "_" + name.text

        # access
        access = SubElement(register, "access")
        access.text = reg.findtext("registerBody/registerProperties/registerPropset/registerAccess")
        if access.text == "WORZ" or access.text == "WO":
            access.text = "W"
        if access.text == "W1C" or access.text == "R/W":
            access.text = "RW"
        if access.text == "RO" or access.text == "ROZ":
            access.text = "R"

        # register details filename
        reg_det_filename = SubElement(register, "register-details-file-name")
        reg_det_filename.text = component_name + "/" + name.text + ".xml"
        # offset
        offset = SubElement(register, "offset")
        if architecture == "DSC":
            offset.text = "0x" + reg.get("normalizedAddrOffsetAsHex")
            if offset.text == "0x0":
                offset.text = "0"
        else:
            offset.text = "0x" + reg.get("registerAbsoluteAddrAsHex")
        # size
        size = SubElement(register, "size")
        size.text = str(int(reg.findtext("registerBody/registerProperties/registerPropset/registerSize")) / 8)
        # base
        if architecture == "DSC":
            base = SubElement(register, "base")
            direct_base = SubElement(base, "direct-base")
            base_address = SubElement(direct_base, "base-address")
            base_address.text = "0x" + reg.get("registerBaseAddrAsHex")
            base_memory_space = SubElement(direct_base, "base-memory-space")
            if int(size.text) == 1:
                base_memory_space.text = "10"
            else:
                base_memory_space.text = "1"

        # make details
        make_details(component_name, reg, details_dir)

    # indent contents
    indent(register_collection)
    handle = ET.tostring(register_collection, encoding="utf-8")
    handle = html_escape(handle)

    # out to xml
    filename = os.path.join(registers_dir, component_name + "_Layout.xml")

    out_xml = open(filename, "w")
    out_xml.write('<?xml version="1.0" encoding="UTF-8" standalone="no" ?>\n')
    out_xml.write('<!DOCTYPE register-collection-file>\n')
    out_xml.write('<register-collection-file xmlns="http://www.freescale.com/schema/ddd/1.0/regdata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.freescale.com/schema/ddd/1.0/regdata RegisterData.xsd">\n\n')
    out_xml.writelines(handle)
    out_xml.write('\n</register-collection-file>\n')
    out_xml.close()

### make Default Layout ###
def make_layout(derivative, registers_dir, postfix_dl):
    # make a tree
    filename = os.path.join(registers_dir, "DefaultLayout_cpu" + postfix_dl + ".xml")
    register_collection = Element("register-collection")
#    if PUT_VERSION != 0:
#        version = SubElement(register_collection, "version")
#        version.text = REGISTERS_VERSION
    name = SubElement(register_collection, "name")
    register_sets = SubElement(register_collection, "register-sets")

    # fill text
    name.text = "DefaultLayout_cpu" + postfix_dl

    # fill register-file-name
    files = os.listdir(registers_dir)
    for file in files:
        register_file_name = SubElement(register_sets, "register-file-name")
        register_file_name.text = "Registers" + "/" + derivative + "/" + file

    # indent contents
    indent(register_collection)
    handle = ET.tostring(register_collection, encoding="utf-8")

    # out to xml
    out_xml = open(filename, "w")
    out_xml.write('<?xml version="1.0" encoding="UTF-8" standalone="no" ?>\n')
    out_xml.write('<!DOCTYPE register-collection-file>\n')
    out_xml.write('<register-collection-file xmlns="http://www.freescale.com/schema/ddd/1.0/regdata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.freescale.com/schema/ddd/1.0/regdata RegisterData.xsd">\n\n')
    out_xml.writelines(handle)
    out_xml.write('\n</register-collection-file>\n')
    out_xml.close()


### make RegisterDetails for register
def make_details(module_name, reg, details_dir):
    # make a tree
    register_details = Element("register-details")
    if PUT_VERSION != 0:
        version = SubElement(register_details, "version")
    reg_name = SubElement(register_details, "name")
    bitrange = SubElement(register_details, "bitrange")

    # default_value
    # if value="X" (dafaque that means?)
    if reg.find("registerBody/registerProperties/registerPropset/registerResetValue") != None:
       if reg.find("registerBody/registerProperties/registerPropset/registerResetValue").text != None:
            if reg.find("registerBody/registerProperties/registerPropset/registerResetValue").text.upper() != "X":
                reset_value = SubElement(register_details, "reset-value")
                reset_value.text = reg.findtext("registerBody/registerProperties/registerPropset/registerResetValue")
                reset_value.text = reset_value.text.replace("_", "")
                try:
                    value = int(reset_value.text, 0)
                    if value == 0:
                        reset_value.text = "%d" %value
                    else:
                        #reset_value.text = "0x%02X" %value
                        reset_value.text = "0x%X" %value
                except:
                    reset_value.text = ""


    description = SubElement(register_details, "description")
    bitfields = SubElement(register_details, "bitfields")

    # fill with text
    if PUT_VERSION != 0:
        version.text = REGISTERS_VERSION
    if reg.findtext("registerName/ph"):
        reg_name.text = reg.findtext("registerName/ph")
    else:
        reg_name.text = reg.findtext("registerName")

    if reg_name.text.find(module_name + "_") == -1:
        reg_name.text = module_name + "_" + reg_name.text
    reg_name.text = reg_name.text.replace("\n","")
    reg_name.text = reg_name.text.replace("\r","")
    reg_name.text = reg_name.text.replace(" ","")

    if reg.find("registerNameMore/registerNameFull") != None and reg.find("registerNameMore/registerNameFull").text != None:
        description.text = ET.tostring(reg.find("registerNameMore/registerNameFull"))
    elif reg.find("registerNameMore/registerBriefDescription") != None and reg.find("registerNameMore/registerBriefDescription").text != None:
        description.text = ET.tostring(reg.find("registerNameMore/registerBriefDescription"))
    else:
        description.text = ""
    description.text = remove_tags(description.text)


    min_bit = 7
    max_bit = 0

    data_bf = []

    # fill bitfileds
    for field in reg.findall("bitField"):
        if field.findtext("bitFieldName") == -1:
            continue

        # make a subtree
        bitfield = SubElement(bitfields, "bitfield")
        b_name = SubElement(bitfield, "name")
        b_bitrange = SubElement(bitfield, "bitrange")
        b_format = SubElement(bitfield, "format")
        b_access = SubElement(bitfield, "access")
        if field.find("bitFieldBriefDescription") != None:
            if field.find("bitFieldBriefDescription").text != None:
                b_description = SubElement(bitfield, "description")
                b_description.text = ET.tostring(field.find("bitFieldBriefDescription"))
                b_description.text = remove_tags(b_description.text)
            elif field.find("bitFieldBody/bitFieldDescription") != None:
                if field.find("bitFieldBody/bitFieldDescription").text != None:
                    b_description = SubElement(bitfield, "description")
                    b_description.text = ET.tostring(field.find("bitFieldBody/bitFieldDescription"))
                    b_description.text = remove_tags(b_description.text)
        b_values = SubElement(bitfield, "values")

        # fill with text
        if field.findtext("bitFieldName/ph"):
            if field.findtext("bitFieldName"):
                b_name.text = field.findtext("bitFieldName") + field.findtext("bitFieldName/ph")
            else:
                b_name.text = field.findtext("bitFieldName/ph")
        else:
            b_name.text = field.findtext("bitFieldName")
        if b_name.text.upper().find("RESERVED") != -1:
            b_name.text = ""
        b_name.text = remove_regions(b_name.text)

        if field.findtext("bitFieldBody/bitFieldProperties/bitFieldPropset/bitFieldAccess") == "RW":
            b_access.text = "readwrite"
        elif field.findtext("bitFieldBody/bitFieldProperties/bitFieldPropset/bitFieldAccess") == "W1C":
            b_access.text = "readwrite"
        elif field.findtext("bitFieldBody/bitFieldProperties/bitFieldPropset/bitFieldAccess") == "W":
            b_access.text = "write"
        elif field.findtext("bitFieldBody/bitFieldProperties/bitFieldPropset/bitFieldAccess") == "WO":
            b_access.text = "write"
        elif field.findtext("bitFieldBody/bitFieldProperties/bitFieldPropset/bitFieldAccess") == "WORZ":
            b_access.text = "write"
        elif field.findtext("bitFieldBody/bitFieldProperties/bitFieldPropset/bitFieldAccess") == "R":
            b_access.text = "read"
        else:
            b_access.text = "read"

        b_format.text = "binary" # TODO
        b_offset = int(field.findtext("bitFieldBody/bitFieldProperties/bitFieldPropset/bitOffset"))
        b_width =  int(field.findtext("bitFieldBody/bitFieldProperties/bitFieldPropset/bitWidth"))
        if b_width == 1:
            b_bitrange.text = str(b_offset)
            data_bf.append(("{:0>2d}".format(b_offset), bitfield))
        else:
            #b_bitrange.text = "{:0>2d}:{:0>2d}".format(b_offset + b_width - 1, b_offset)
            b_bitrange.text = "{0}:{1}".format(b_offset + b_width - 1, b_offset)
            data_bf.append(("{:0>2d}:{:0>2d}".format(b_offset + b_width - 1, b_offset), bitfield))

        # calc maximum global bitrange
        min_bit = min(min_bit, b_offset)
        max_bit = max(max_bit, b_offset + b_width - 1)
        bitrange.text = "{0}:{1}".format(max_bit, min_bit)

        # fill bitfields values
        for valueGroup in field.findall("bitFieldBody/bitFieldValues/bitFieldValueGroup"):
            # skip not existing
            if valueGroup.find("bitFieldValueName") == None:
                if valueGroup.find("bitFieldValueDescription") == None:
                    continue
            if valueGroup.find("bitFieldValue") == None:
                continue

            # skip empty
            if valueGroup.findtext("bitFieldValueDescription") == "":
                continue

            # skip reserved, else, apos, etc
            if valueGroup.findtext("bitFieldValue").upper() == "RESERVED":
                continue
            if valueGroup.find("bitFieldValue").text.upper() == "ELSE":
                continue
            #if valueGroup.find("bitFieldValue").text.find("'") != -1:
            #    continue

            # если range
            s = valueGroup.findtext("bitFieldValue")
            if valueGroup.findtext("bitFieldValue").find("-") != -1:
                # вычленяем границы range
                ranges = valueGroup.find("bitFieldValue").text.split("-")
                range1 = int(ranges[0], 2)
                range2 = int(ranges[1], 2)
                # заполняем values
                while range1 <= range2:
                    # создаем value и description
                    g_value = SubElement(b_values, "value")
                    v_value = SubElement(g_value, "value")
                    v_description = SubElement(g_value, "description")
                    # заполняем value и description
                    v_value.text = "0b" + format(range1, 'b').zfill(b_width)
                    if valueGroup.find("bitFieldValueName") != None:
                        v_description.text = ET.tostring(valueGroup.find("bitFieldValueName"))
                    else:
                        v_description.text = ET.tostring(valueGroup.find("bitFieldValueDescription"))
                    v_description.text = remove_tags(v_description.text)
                    range1 = range1 + 1
            # если не range
            else:
                # make a subtree
                g_value = SubElement(b_values, "value")
                v_value = SubElement(g_value, "value")
                v_description = SubElement(g_value, "description")

                # fill with text
                v_value.text = valueGroup.findtext("bitFieldValue")

                if v_value.text.find("'h") != -1:
                    v_value.text = remove_xaposh(v_value.text)
                    v_value.text = "0b{0:0>4b}".format(int(v_value.text, 16))
                else:
                    v_value.text = "0b" + v_value.text
                    v_value.text = v_value.text.replace("x", "-")
                #v_value.text = "0b{0}".format(valueGroup.findtext("bitFieldValue"))

                if valueGroup.find("bitFieldValueName") != None:
                    v_description.text = ET.tostring(valueGroup.find("bitFieldValueName"))
                else:
                    v_description.text = ET.tostring(valueGroup.find("bitFieldValueDescription"))
                v_description.text = remove_tags(v_description.text)

        if b_values != None and len(b_values.getchildren()) == 0:
            bitfield.remove(b_values)

    data_bf.sort()

    # remove leading zero
    data_tmp = []
    for key, elem in data_bf:
        t = key.split(":")
        for index, e in enumerate(t):
            t[index] = "{0}".format(int(e))
        if len(t) == 2:
            data_tmp.append(("{0}:{1}".format(t[0], t[1]), elem))
        else:
            data_tmp.append(("{0}".format(t[0]), elem))

    # insert the last item from each tuple
    bitfields[:] = [item[-1] for item in data_tmp]


    # create dir if needed
    if not os.path.exists(os.path.join(details_dir, module_name)):
        os.makedirs(os.path.join(details_dir, module_name))

    # indent
    indent(register_details)
    handle = ET.tostring(register_details, encoding="utf-8")
    handle = html_escape(handle)

    # out to xml
    out_xml = open(os.path.join(details_dir, module_name, reg_name.text + ".xml"), "w")

    out_xml.write('<?xml version="1.0" encoding="UTF-8" standalone="no" ?>\n')
    out_xml.write('<!DOCTYPE register-details-file>\n')
    out_xml.write('<register-details-file xmlns="http://www.freescale.com/schema/ddd/1.0/detail" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.freescale.com/schema/ddd/1.0/detail RegisterDetails.xsd">\n\n')
    out_xml.writelines(handle)
    out_xml.write('\n</register-details-file>\n')
    out_xml.close()


### remove tags ###
def remove_tags(text):
    p = re.compile(r'<.*?>')
    if not isinstance(text, basestring):
        return
    result = p.sub('', text)
    result = string.strip(result, "\n ")
    return re.sub(r'\s+', ' ', result)

### remove regions [xx:xx] ###
def remove_regions(text):
    p = re.compile(r'\[.*?\]')
    if not isinstance(text, basestring):
        return
    result = p.sub('', text)
    result = string.strip(result, "\n ")
    return re.sub(r'\s+', ' ', result)

# remove X'h in value
def remove_xaposh(text):
    p = re.compile(r'[1-9]\'[h]')
    if not isinstance(text, basestring):
        return
    result = p.sub('', text)
    result = string.strip(result, "\n ")
    return re.sub(r'\s+', ' ', result)



### replace escape characters ###
def html_escape(text):
    text = text.replace("'", '&apos;')
    text = text.replace('"', '&quot;')
    text = text.replace('&amp;#8217;', '&apos;')
    text = text.replace('&amp;#8212;', '-')
    text = text.replace('&amp;#64257;', 'fi')
    text = text.replace('&amp;#8804;', '&lt;=')
    text = text.replace('&amp;lt;', '&lt;')
    text = text.replace('&amp;gt;', '&gt;')
    text = text.replace('&amp;#8220;', '&quot;')
    text = text.replace('&amp;#8221;', '&quot;')
    text = text.replace('&amp;#177;', '+/-')
    text = text.replace('&amp;#160;', '')
    return text


### main() ###
def main():
    print "Register Generator from RDP drop, ver." + RG_VERSION
    if len(sys.argv) <> 5:
        print "Usage: RegisterGenerator <components_folder> <derivative> <default_layout_postfix> <architecture>"
        print "<components_folder> - folder 'components' from RDP drop"
        print "<derivative> - derivative name"
        print "<default_layout_postfix> - postfix for default layout file (e.g. HCS08, DSC, MK11DA5, etc)"
        print "<architecture> - DSC or S08 (others not yet tested)"
        return

    print "Generating Registers..."
    components_dir = sys.argv[1]
    derivative = sys.argv[2]
    postfix_dl = sys.argv[3]
    architecture = sys.argv[4]
    registers_dir = os.path.join(derivative, "Registers", derivative)
    details_dir = os.path.join(derivative, "RegisterDetails", derivative)

    # make dirs
    if os.path.exists(derivative):
        shutil.rmtree(derivative)
    if not os.path.exists(registers_dir):
        os.makedirs(registers_dir)
    if not os.path.exists(details_dir):
        os.makedirs(details_dir)

    files = os.listdir(components_dir)
    for file in files:
        make_registers(os.path.join(components_dir, file), registers_dir, details_dir, architecture)
    make_layout(derivative, registers_dir, postfix_dl)

    print "Generating done."

main()